package com.dgharami.RiverDaleTaxi;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.dgharami.RiverDaleTaxi.locationwork.GPSTracker;
import com.dgharami.RiverDaleTaxi.logger.Logger;
import com.dgharami.RiverDaleTaxi.manager.DeviceManager;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.manager.SessionManager;
import com.dgharami.RiverDaleTaxi.models.deviceid.DeviceId;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.others.ConnectionDetector;
import com.dgharami.RiverDaleTaxi.parsing.CustomerModule;
//import com.digits.sdk.android.Digits;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

//import com.twitter.sdk.android.core.TwitterAuthConfig;
//import com.twitter.sdk.android.core.TwitterCore;
//import io.fabric.sdk.android.Fabric;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class SplashActivity extends AppCompatActivity implements
        ApiFetcher {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    /*private static final String TWITTER_KEY = "jan5GBSe8Jl5yCJERTZq6vj7u";
    private static final String TWITTER_SECRET = "q2yKPLLRoU2bwzBP2QrD7gfC9aUr4Vmv6ghh3segybC55R6cCa";
    */

    private static final String TWITTER_KEY = "xEosUtNZhuv0xzjNy2RSWQexk";
    private static final String TWITTER_SECRET = "XYqlntgSOwWOShTO6Y6z8kyckPoVltfQmXLrEuLIfEeuOGsTAG";

    //ViewFlipper viewFlipper;
    CoordinatorLayout cl_splash;

    public static Activity splashActivity;
    int gallery_grid_Images[] = {R.drawable.splash1, R.drawable.splash2, R.drawable.splash3};

    String ACCESS_FINE_LOCATION = "android.permission.ACCESS_FINE_LOCATION";
    String CALL_PHONE = "android.permission.CALL_PHONE";
    String READ_EXTERNAL_STORAGE = "android.permission.READ_EXTERNAL_STORAGE";
    ProgressDialog pd;

    GPSTracker gpsTracker;
    Geocoder geocoder;
    List<Address> addressList;
    double currentLat, currentLong;

    DeviceManager deviceManager;
    CustomerModule customerModule;
    SessionManager sessionManager;
    LanguageManager languageManager;
    ConnectionDetector connectionDetector;
    Boolean isInternetPresent = false;
    int version_code;

    String version_name = "";

    String city;

    String language_id = "";

    protected static final int REQUEST_CHECK_SETTINGS = 0x1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
//        Fabric.with(this, new TwitterCore(authConfig), new Digits.Builder().build());
        setContentView(R.layout.activity_splash);
        splashActivity = this;

        deviceManager = new DeviceManager(this);
        sessionManager = new SessionManager(this);
        customerModule = new CustomerModule(this);
        languageManager = new LanguageManager(this);

        String lang = Locale.getDefault().getDisplayLanguage();
        Logger.e("language      " + lang);
        if (lang.equals("français")) {
            language_id = "2";
            Logger.e("language_id       " + "" + lang);
            languageManager.createLanguageSession(language_id);
        } else if (lang.equals("English")){
            language_id = "1";
            Logger.e("language_id       " + "" + language_id);
            languageManager.createLanguageSession(language_id);
        }

        pd = new ProgressDialog(this,R.style.MyAlertDialogStyle);
        pd.setMessage("Loading...");
//        pd.setCanceledOnTouchOutside(false);
//        pd.setCancelable(false);

//        viewFlipper = (ViewFlipper) findViewById(R.id.flipper);
        cl_splash = (CoordinatorLayout) findViewById(R.id.cl_splash);

        if ((ContextCompat.checkSelfPermission(SplashActivity.this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(SplashActivity.this, CALL_PHONE) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(SplashActivity.this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{"android.permission.ACCESS_FINE_LOCATION", "android.permission.CALL_PHONE", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.RECEIVE_SMS"}, 1);
        } else if ((ContextCompat.checkSelfPermission(SplashActivity.this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(SplashActivity.this, CALL_PHONE) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{"android.permission.ACCESS_FINE_LOCATION", "android.permission.CALL_PHONE", "android.permission.RECEIVE_SMS"}, 1);
        } else if ((ContextCompat.checkSelfPermission(SplashActivity.this, CALL_PHONE) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(SplashActivity.this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{"android.permission.CALL_PHONE", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.RECEIVE_SMS"}, 1);
        } else if ((ContextCompat.checkSelfPermission(SplashActivity.this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(SplashActivity.this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{"android.permission.ACCESS_FINE_LOCATION", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.RECEIVE_SMS"}, 1);
        } else if ((ContextCompat.checkSelfPermission(SplashActivity.this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{"android.permission.ACCESS_FINE_LOCATION", "android.permission.RECEIVE_SMS"}, 1);
        } else if ((ContextCompat.checkSelfPermission(SplashActivity.this, CALL_PHONE) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{"android.permission.CALL_PHONE", "android.permission.RECEIVE_SMS"}, 1);
        } else if ((ContextCompat.checkSelfPermission(SplashActivity.this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{"android.permission.READ_EXTERNAL_STORAGE", "android.permission.RECEIVE_SMS"}, 1);
        } else {
            checkNetworkStatus();
        }

//        for (int i = 0; i < gallery_grid_Images.length; i++) {
//            ImageView image = new ImageView(getApplicationContext());
//            image.setBackgroundResource(gallery_grid_Images[i]);
//            viewFlipper.addView(image);
//        }
//        viewFlipper.startFlipping();
    }

    public void checkNetworkStatus() {
        connectionDetector = new ConnectionDetector(this);
        isInternetPresent = connectionDetector.isConnectingToInternet();
        if (isInternetPresent) {
            GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(10000);
            locationRequest.setFastestInterval(10000 / 2);

            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            Logger.e("SUCCESS       " + "All location settings are satisfied.");

                            PackageManager manager = SplashActivity.this.getPackageManager();
                            PackageInfo info = null;
                            try {
                                info = manager.getPackageInfo(SplashActivity.this.getPackageName(), 0);
                            } catch (PackageManager.NameNotFoundException e) {
                                e.printStackTrace();
                            }
                            version_name = info.versionName;
                            version_code = info.versionCode;

                            customerModule.forceUpdateApi(version_code + "",language_id);

                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            Logger.e("RESOLUTION_REQUIRED       " + "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");
                            try {
                                // Show the dialog by calling startResolutionForResult(), and check the result in onActivityResult().
                                status.startResolutionForResult(SplashActivity.this, REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                                Logger.e("Exception         " + "PendingIntent unable to execute request.");
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            Logger.e("SETTINGS_CHANGE_UNAVAILABLE       " + "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                            break;
                    }
                }
            });


        } else {
            Snackbar snackbar = Snackbar
                    .make(cl_splash, "No Internet Connection", Snackbar.LENGTH_LONG)
                    .setDuration(Snackbar.LENGTH_INDEFINITE)
                    .setActionTextColor(Color.RED)
                    .setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            checkNetworkStatus();
                        }
                    });
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == 1) {
            Logger.e("request code " + "k ander a gya");
            int len = permissions.length;
            Logger.e("Length of permission      " + len + "");
            if (len == 4) {
                checkNetworkStatus();
            } else {
                Logger.e("else " + "Chal gya");
            }
        }
    }

    @Override
    public void onAPIRunningState(int a) {

        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            pd.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            pd.dismiss();
        }
    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("Force Update")) {

            DeviceId deviceId;
            deviceId = gson.fromJson(response, DeviceId.class);

            if (deviceId.getResult().toString().equals("1")) {

                if (sessionManager.isLoggedIn()) {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                } else if (!(sessionManager.isLoggedIn())) {
                    startActivity(new Intent(SplashActivity.this, LoginRegisterActivity.class).putExtra("cityLocation", "Dummy City"));
                    finish();
                }
                Logger.e("success" + deviceId.getMsg());
            } else if (deviceId.getResult().toString().equals("418")) {
                Logger.e("message      " + deviceId.getMsg());
                dialogForAppUpdate();
            } else {
                Logger.e("message      " + deviceId.getMsg());
            }
        }
    }

    public class GetLocationAsync extends AsyncTask<String, Void, String> {

        double x, y;
        StringBuilder str;

        public GetLocationAsync(double latitude, double longitude) {
            x = latitude;
            y = longitude;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {
            str = new StringBuilder();
            try {
                geocoder = new Geocoder(SplashActivity.this, Locale.ENGLISH);
                addressList = geocoder.getFromLocation(x, y, 1);
                if (geocoder.isPresent()) {
                    Address returnAddress = addressList.get(0);
                    String addressLine = returnAddress.getAddressLine(0);
                    String addressLine1 = returnAddress.getAddressLine(1);
                    String addressLine2 = returnAddress.getAddressLine(2);
                    String locality = returnAddress.getLocality();

                    returnAddress.getCountryCode();
                    returnAddress.getLocale();
                    returnAddress.getPostalCode();
                    returnAddress.getPhone();
                    returnAddress.getSubLocality();
                    str.append(addressLine + ", " + addressLine1 + ", " + addressLine2 + "__" + locality);
                } else {
                    Logger.e("Geocoder is not " + "Present");
                }
            } catch (Exception e) {
//                Logger.e("Exception from getting Location in Do-In Background       " + e.toString());
            }
            return str.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                if (result.equals("")) {
                    new GetLocationAsync(currentLat, currentLong).execute();
                } else {
                    pd.dismiss();
                    String[] parts = result.split("__");
                    String address = parts[0];
                    city = parts[1];
                    Logger.e("city      " + city);
                    if (sessionManager.isLoggedIn()) {
                        startActivity(new Intent(SplashActivity.this, MainActivity.class).putExtra("cityLocation", city));
                        finish();
                    } else if (!(sessionManager.isLoggedIn())) {
                        startActivity(new Intent(SplashActivity.this, LoginRegisterActivity.class).putExtra("cityLocation", city));
                        finish();
                    }
                }
            } catch (Exception e) {
                Logger.e("Exception on On Post Method       " + e.toString());
            }
        }
    }

    public void getCurrentLocation() {

        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addressList = geocoder.getFromLocation(currentLat, currentLong, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            Logger.e("Exception in " + "get Current Location Method");
        }

        String address = addressList.get(0).getAddressLine(0) + " | " + addressList.get(0).getAddressLine(1) + " | " + addressList.get(0).getAddressLine(2); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addressList.get(0).getLocality();
        String state = addressList.get(0).getAdminArea();
        String country = addressList.get(0).getCountryName();
        String postalCode = addressList.get(0).getPostalCode();
        String knownName = addressList.get(0).getFeatureName();
        String countryCode = addressList.get(0).getCountryCode();

        Logger.e("address       " + address);
        Logger.e("city       " + city);
        Logger.e("state       " + state);
        Logger.e("country       " + country);
        Logger.e("postalCode       " + postalCode);
        Logger.e("known Name       " + knownName);
        Logger.e("Country Code       " + countryCode);

        pd.dismiss();

        if (sessionManager.isLoggedIn()) {
            startActivity(new Intent(SplashActivity.this, MainActivity.class).putExtra("cityLocation", city));
            finish();
        } else if (!(sessionManager.isLoggedIn())) {
            startActivity(new Intent(SplashActivity.this, LoginRegisterActivity.class).putExtra("cityLocation", city));
            finish();
        }
    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }

    public void dialogForAppUpdate() {

        Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        dialog.setContentView(R.layout.dialog_for_force_update);
        dialog.setCancelable(false);

        LinearLayout ll_update = (LinearLayout) dialog.findViewById(R.id.ll_update);
        ll_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String appPackageName = getPackageName();
                Logger.e("package name      " + appPackageName);
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Logger.e("RESULT_OK       " + "User agreed to make required location settings changes.");
                        PackageManager manager = SplashActivity.this.getPackageManager();
                        PackageInfo info = null;
                        try {
                            info = manager.getPackageInfo(SplashActivity.this.getPackageName(), 0);
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                        version_name = info.versionName;
                        version_code = info.versionCode;

                        customerModule.forceUpdateApi(version_code + "",language_id);
                        break;
                    case Activity.RESULT_CANCELED:
                        Logger.e("RESULT_CANCELED       " + "User chose not to make required location settings changes.");
                        finish();
                        break;
                }
                break;
        }
    }
}
