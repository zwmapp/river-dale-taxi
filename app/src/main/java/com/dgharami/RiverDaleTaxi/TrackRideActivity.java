package com.dgharami.RiverDaleTaxi;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.dgharami.RiverDaleTaxi.adapter.ReasonAdapter;
import com.dgharami.RiverDaleTaxi.logger.Logger;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.manager.SessionManager;
import com.dgharami.RiverDaleTaxi.models.ResultCheck;
import com.dgharami.RiverDaleTaxi.models.cancelreasoncustomer.CancelReasonCustomer;

import com.dgharami.RiverDaleTaxi.models.deviceid.DeviceId;
import com.dgharami.RiverDaleTaxi.models.trackridebeforearrived.TrackRideBeforeArrived;
import com.dgharami.RiverDaleTaxi.models.viewrideinfo.ViewRideInfo;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.others.Contants;
import com.dgharami.RiverDaleTaxi.others.DownloadTask;
import com.dgharami.RiverDaleTaxi.others.FirebaseDriverLatLng;
import com.dgharami.RiverDaleTaxi.parsing.CustomerModule;
import com.dgharami.RiverDaleTaxi.parsing.RideModule;
import com.dgharami.RiverDaleTaxi.urls.Apis;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.h6ah4i.android.materialshadowninepatch.MaterialShadowContainerView;
import com.sinch.android.rtc.MissingPermissionException;
import com.squareup.picasso.Picasso;

import com.sinch.android.rtc.calling.Call;


import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class TrackRideActivity extends BaseActivity implements
        ApiFetcher,
        Apis,
        OnMapReadyCallback,
        GoogleMap.OnMyLocationChangeListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        GoogleMap.OnCameraMoveListener,
        GoogleMap.OnCameraMoveStartedListener,
        GoogleMap.OnCameraIdleListener {

    LinearLayout ll_back_track_ride, ll_cancel_ride;
    Button ll_call_driver;
    TextView drivernme, drivercarno, tv_rating;
    TextView tv_cancel, tv_set, tv_location;
    ImageView driverimage;

    BroadcastReceiver broadcastReceiver;

    public static int distance;
    MapView map_view_driver_detail;
    public static GoogleMap mGoogleMap;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    ArrayList<LatLng> mMarkerPoints;

    LanguageManager languageManager;
    String driverID, driverId, driverName, driverPhone, driverImage, rating, carNumber, oldRideID, ride_id, userId, dropLat, dropLong, pickupLat, pickupLocation, pickupLong, driverLat, driverLong;
    public static int i = 0;
    public static TrackRideActivity trackRideActivity, abc;
    ProgressDialog pd;
    String language_id;


    public static String dropLocation;
    String ride_status;
    public static FrameLayout fl_gps1;

    RideModule rideModule;
    CancelReasonCustomer cancelReasonCustomer;
    String reason_id;

    // MOVE CAR ANIMATION DEFAULT VALUE previousStep = 0f;
    private Marker marker;
    float previousStep = 0f;
    ValueAnimator animationMove;
    Marker routeStart, routeEnd;
    int globalZoom = 16;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference allDriversLiveLatLng;
    DatabaseReference currentDriver;

    MaterialShadowContainerView dgHeaderArea;
    MaterialShadowContainerView dgFooterArea;
    boolean isMapInteractedWithUser = false;
    int drawActualDriverRoute = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_ride);
        getSupportActionBar().hide();
        trackRideActivity = this;

        firebaseDatabase = FirebaseDatabase.getInstance();
        allDriversLiveLatLng = firebaseDatabase.getReference("RiverDaleCabProject").child("allDrivers");
        dgHeaderArea = (MaterialShadowContainerView) findViewById(R.id.dgHeaderArea);
        dgFooterArea = (MaterialShadowContainerView) findViewById(R.id.dgFooterArea);

        abc = this;
        map_view_driver_detail = (MapView) findViewById(R.id.map_view_driver_detail);
        map_view_driver_detail.onCreate(savedInstanceState);

        drivernme = (TextView) findViewById(R.id.drivername);
        drivercarno = (TextView) findViewById(R.id.drivercarno);
        tv_cancel = (TextView) findViewById(R.id.tv_cancel);
        tv_location = (TextView) findViewById(R.id.tv_location);
        tv_set = (TextView) findViewById(R.id.tv_set);
        tv_rating = (TextView) findViewById(R.id.tv_rating);
        ll_back_track_ride = (LinearLayout) findViewById(R.id.ll_back_track_ride);
        ll_cancel_ride = (LinearLayout) findViewById(R.id.ll_cancel_ride);
        ll_call_driver = (Button) findViewById(R.id.ll_call_driver);
        ll_call_driver.setEnabled(false);
        driverimage = (ImageView) findViewById(R.id.drivercircularimage);
        fl_gps1 = (FrameLayout) findViewById(R.id.fl_gps1);

        rideModule = new RideModule(this);

        languageManager = new LanguageManager(this);
        language_id = languageManager.getLanguageDetail().get(LanguageManager.LANGUAGE_ID);


        Contants.openActivity2 = 0;

        pd = new ProgressDialog(this, R.style.MyAlertDialogStyle);
        pd.setMessage("Loading...");

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        ride_id = getIntent().getExtras().getString("ride_id");
        oldRideID = getIntent().getExtras().getString("ride_id");
        ride_status = getIntent().getExtras().getString("ride_status");
        rideModule.viewRideInfoApi(ride_id, language_id);
//        String cancelButton = super.getIntent().getExtras().getString("cancelButton");

        // SCHEDULED UPDATE OF DRIVER CAR ===
//        final Handler handler = new Handler();
//        final int interval = 10000;
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                CustomerModule customerModule = new CustomerModule(TrackRideActivity.this);
//                customerModule.trackDriverApi(ride_id, language_id);
//                handler.postDelayed(this, interval);
//            }
//        }, interval);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle b = intent.getExtras();
                ride_id = b.getString("ride_id");
                ride_status = b.getString("ride_status");
                if (ride_status.equals("5")) {
                    tv_cancel.setVisibility(View.INVISIBLE);
                    ll_cancel_ride.setOnClickListener(null);
                    dialogForArrived();
                } else if (ride_status.equals("6")) {
                    tv_location.setText(dropLocation);
                    tv_set.setText("DROP LOCATION");
                    fl_gps1.setVisibility(View.VISIBLE);
                    dialogForStart();
                } else if (ride_status.equals("7")) {
                    Intent i = new Intent();
                    i.setClassName("com.zwm.dg.riverdaletaxi", "com.dgharami.RiverDaleTaxi.InvoiceActivity");
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("ride_id", oldRideID);
                    i.putExtra("order_id", ride_id);
                    i.putExtra("ride_status", ride_status);
                    context.startActivity(i);
                } else if (ride_status.equals("9")) {
                    dialogForCancelRideByDriver();
                }
                updateRideUI();
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("broadCastNameCancel"));

        userId = new SessionManager(TrackRideActivity.this).getUserDetails().get(SessionManager.USER_ID);

        ll_call_driver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callButtonClicked();
            }
        });

        fl_gps1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                track(pickupLat, pickupLong, dropLat, dropLong);
            }
        });

        ll_cancel_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCancelRideDialog();
            }
        });
        ll_back_track_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    } // end Of onCreate

    @Override
    protected void onServiceConnected() {
        ll_call_driver.setEnabled(true);
        //Toast.makeText(trackRideActivity, "connected", Toast.LENGTH_SHORT).show();
    }

    //SINCH
    private void callButtonClicked() {
        String userName = "DRIVER-" + driverId.trim();
        if (userName.isEmpty()) {
            Toast.makeText(this, "Please enter a user to call", Toast.LENGTH_LONG).show();
            return;
        }

        try {
            Call call = getSinchServiceInterface().callUser(userName);
            if (call == null) {
                // Service failed for some reason, show a Toast and abort
                Toast.makeText(this, "Service is not started. Try stopping the service and starting it again before "
                        + "placing a call.", Toast.LENGTH_LONG).show();
                return;
            }
            String callId = call.getCallId();
            Intent callScreen = new Intent(this, CallScreenActivity.class);
            callScreen.putExtra(SinchService.CALL_ID, callId);
            startActivity(callScreen);
        } catch (MissingPermissionException e) {
            ActivityCompat.requestPermissions(this, new String[]{e.getRequiredPermission()}, 0);
        }

    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "You may now place a call", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "This application needs permission to use your microphone to function properly.", Toast
                    .LENGTH_LONG).show();
        }
    }
    //SINCH

    public void dialogForCancelRideByDriver() {

        final Dialog dialog = new Dialog(TrackRideActivity.abc, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        dialog.setContentView(R.layout.dialog_for_cancel_by_driver);
        dialog.setCancelable(false);

        LinearLayout ll_ok_cancel = (LinearLayout) dialog.findViewById(R.id.ll_ok_cancel);
        ll_ok_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
                startActivity(new Intent(TrackRideActivity.this, RidesActivity.class));

            }
        });
        dialog.show();
    }

    public void dialogForStart() {

        final Dialog dialog = new Dialog(TrackRideActivity.abc, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        dialog.setContentView(R.layout.dialog_for_start);
        dialog.setCancelable(false);

        LinearLayout ll_ok_start = (LinearLayout) dialog.findViewById(R.id.ll_ok_start);
        ll_ok_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*                mGoogleMap.clear();
                mMarkerPoints.clear();*/

                //drawMarker(new LatLng(Double.parseDouble(pickupLat), Double.parseDouble(pickupLong)));
                //drawMarker(new LatLng(Double.parseDouble(dropLat), Double.parseDouble(dropLong)));

                dialog.dismiss();

            }
        });
        dialog.show();
    }

    public void dialogForArrived() {
        final Dialog dialog = new Dialog(TrackRideActivity.abc, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        dialog.setContentView(R.layout.dialog_for_arrived);
        dialog.setCancelable(false);

        LinearLayout ll_ok_arrived = (LinearLayout) dialog.findViewById(R.id.ll_ok_arrived);
        ll_ok_arrived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void track(String pickup_Lat, String pickup_Long, String drop_Lat, String drop_Long) {

        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?" + "saddr=" + pickup_Lat + "," + pickup_Long + "&daddr=" + drop_Lat + "," + drop_Long));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        startActivity(intent);
    }

    private void drawMarker(LatLng point) {

        mMarkerPoints.add(point);
        MarkerOptions options = new MarkerOptions();
        options.position(point);
        if (mMarkerPoints.size() == 1) {
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.car_ola));
            marker = mGoogleMap.addMarker(options);
        } else if (mMarkerPoints.size() == 2) {
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.location_marker_small));
            mGoogleMap.addMarker(options);
        }


        if (mMarkerPoints.size() >= 2) {
            LatLng start = mMarkerPoints.get(0);
            LatLng destination = mMarkerPoints.get(1);
            String url = "https://maps.googleapis.com/maps/api/directions/json?" + "origin=" + start.latitude + "," + start.longitude + "&destination=" + destination.latitude + "," + destination.longitude + "&sensor=false";
            Logger.e(" Direction url        " + url);
            DownloadTask downloadTask = new DownloadTask();
            downloadTask.execute(url);
        }
    }

    @Override
    public void onAPIRunningState(int a) {

        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            pd.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            pd.dismiss();
        }
    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("View Ride Info")) {
            ViewRideInfo viewRideInfo;
            viewRideInfo = gson.fromJson(response, ViewRideInfo.class);

            if (viewRideInfo.getResult().toString().equals("1")) {
                ride_id = viewRideInfo.getDetails().getRideId();
                pickupLat = viewRideInfo.getDetails().getPickupLat();
                pickupLong = viewRideInfo.getDetails().getPickupLong();
                pickupLocation = viewRideInfo.getDetails().getPickupLocation();
                dropLocation = viewRideInfo.getDetails().getDropLocation();
                dropLat = viewRideInfo.getDetails().getDropLat();
                dropLong = viewRideInfo.getDetails().getDropLong();
                driverName = viewRideInfo.getDetails().getDriverName();
                driverId = viewRideInfo.getDetails().getCarNumber();
                driverID = viewRideInfo.getDetails().getDriverID();
                driverImage = viewRideInfo.getDetails().getDriverImage();
                driverPhone = viewRideInfo.getDetails().getDriverPhone();
                driverLat = viewRideInfo.getDetails().getDriverLat();
                driverLong = viewRideInfo.getDetails().getDriverLong();
                carNumber = viewRideInfo.getDetails().getCarNumber();
                rating = viewRideInfo.getDetails().getDriverRating();
                ride_status = viewRideInfo.getDetails().getRideStatus();
                map_view_driver_detail.getMapAsync(this);

                drivernme.setText(driverName);
                drivercarno.setText(carNumber);
                tv_rating.setText(rating);
                Picasso.with(this)
                        .load(imageDomain + driverImage)
                        .placeholder(R.drawable.dummy_pic)
                        .error(R.drawable.dummy_pic)
                        .fit()
                        .into(driverimage);
                tv_location.setText(pickupLocation);
                updateRideUI();

            } else {
                Toast.makeText(TrackRideActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
            }
        }

        if (apiName.equals("Cancel Ride")) {
            DeviceId deviceId;
            deviceId = gson.fromJson(response, DeviceId.class);
            if (deviceId.getResult().toString().equals("1")) {
//                Toast.makeText(this, "" + deviceId.getMsg(), Toast.LENGTH_SHORT).show();
                startActivity(new Intent(TrackRideActivity.this, RidesActivity.class));
                finish();
            } else {
                Toast.makeText(this, "" + deviceId.getMsg(), Toast.LENGTH_SHORT).show();
            }
        }

        if (apiName.equals("View Reasons")) {
            ResultCheck resultCheck;
            resultCheck = gson.fromJson(response, ResultCheck.class);
            if (resultCheck.result.equals("1")) {

                cancelReasonCustomer = gson.fromJson(response, CancelReasonCustomer.class);
                showReasonDialog();
            } else {
                Toast.makeText(this, "No Data Found", Toast.LENGTH_SHORT).show();
            }
        }

        if (apiName.equals("Track Driver Before Arrived")) {

            Logger.e("track ride " + " before arrived k ander a gya");
            TrackRideBeforeArrived trackRideBeforeArrived;
            trackRideBeforeArrived = gson.fromJson(response, TrackRideBeforeArrived.class);

            if (trackRideBeforeArrived.getResult().toString().equals("1")) {
                Logger.e("track ride " + " before arrived k ander a gya with 1");
                String latitude = trackRideBeforeArrived.getDetails().getDriverLat();
                String longitude = trackRideBeforeArrived.getDetails().getDriverLong();
                LatLng currentLatLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));

//                    float rotationAngle = (float) getAngleFromLatLng(marker.getPosition(), currentLatLng);
//                    if (null != animationMove && animationMove.isRunning()) {
//                        animationMove.end();
//                    }
//                    smoothRotateAndGoTheCar(marker, rotationAngle, currentLatLng);
//
//                    Logger.e("Lat Long Location Changed     " + latitude + ", " + longitude);
//                    CameraPosition cameraPosition = new CameraPosition.Builder().target(marker.getPosition()).zoom(globalZoom).build();
//                    mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                //marker.remove();

//DG                Logger.e("Tracking lat long bahr try     " + latitude + ", " + longitude);
//                try {
//                    Logger.e("try " + "chal gya");
//                    Logger.e("Tracking lat long ander try     " + latitude + ", " + longitude);
//                    mGoogleMap.clear();
//                    mMarkerPoints.clear();
//                    LatLng finalDriverPos = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
//                    marker.setPosition(finalDriverPos);
//                    drawMarker(finalDriverPos);
//
//                    if(ride_status.equals("6")){
//                        drawMarker(new LatLng(Double.parseDouble(dropLat), Double.parseDouble(dropLong)));
//                        drawMarker(new LatLng(Double.parseDouble(dropLat), Double.parseDouble(dropLong)));
//                    }else{
//                        drawMarker(new LatLng(Double.parseDouble(pickupLat), Double.parseDouble(pickupLong)));
//                        drawMarker(new LatLng(Double.parseDouble(pickupLat), Double.parseDouble(pickupLong)));
//                    }
//
//                    CameraPosition cameraPosition = new CameraPosition.Builder()
//                            .target(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)))
//                            .zoom(16).build();
//                    mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//                } catch (Exception e) {
//                    Logger.e("Exception to get track        " + e.toString());
//DG                }
            }
        }
    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }

    public void showCancelRideDialog() {

        final Dialog dialog = new Dialog(TrackRideActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        dialog.setCancelable(false);
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_for_logout);

        LinearLayout yes = (LinearLayout) dialog.findViewById(R.id.yes);
        LinearLayout no = (LinearLayout) dialog.findViewById(R.id.no);
        TextView tv_cancel_ride = (TextView) dialog.findViewById(R.id.tv_cancel_ride);
        tv_cancel_ride.setText("Cancel Ride");

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomerModule customerModule = new CustomerModule(TrackRideActivity.this);
                customerModule.cancelReasonApi(language_id);
                dialog.dismiss();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void showReasonDialog() {

        final Dialog dialog = new Dialog(TrackRideActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        dialog.setCancelable(false);
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_for_cancel_reason);

        final ListView lv_reasons = (ListView) dialog.findViewById(R.id.lv_reasons);
        lv_reasons.setAdapter(new ReasonAdapter(TrackRideActivity.this, cancelReasonCustomer));
        final LinearLayout otherReason = (LinearLayout) dialog.findViewById(R.id.otherReason);
        final EditText otherReasonText = (EditText) dialog.findViewById(R.id.otherReasonText);
        Button otherReasonBtn = (Button) dialog.findViewById(R.id.otherReasonBtn);

        lv_reasons.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                reason_id = cancelReasonCustomer.getMsg().get(position).getReasonId();
                String reasonText = cancelReasonCustomer.getMsg().get(position).getReasonName();
                if(reasonText.equalsIgnoreCase("other") || reasonText.equalsIgnoreCase("others")){
                    YoYo.with(Techniques.SlideOutUp).duration(200).repeat(0).playOn(lv_reasons);
                    YoYo.with(Techniques.SlideInDown).duration(200).repeat(0).playOn(otherReason);
                }else{
                    RideModule rideModule = new RideModule(TrackRideActivity.this);
                    rideModule.cancelRideApi(ride_id, reason_id, "", language_id);
                }

                dialog.dismiss();
            }
        });

        otherReasonBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String reasonText = otherReasonText.getText().toString().trim();
                if(reasonText.length() <= 0){
                    Toasty.error(TrackRideActivity.this, "Please enter reason.", Toast.LENGTH_SHORT).show();
                }else{
                    RideModule rideModule = new RideModule(TrackRideActivity.this);
                    rideModule.cancelRideApi(ride_id, "0", reasonText, language_id);
                }
            }
        });

        dialog.show();
    }


    @Override
    public void onResume() {
        super.onResume();
        map_view_driver_detail.onResume();
        Contants.openActivity2 = 0;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        map_view_driver_detail.onResume();
        Contants.openActivity2 = 0;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Contants.openActivity2 = 0;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        map_view_driver_detail.onLowMemory();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Contants.openActivity2 = 1;
    }

    @Override
    protected void onStop() {
        super.onStop();
        Contants.openActivity2 = 1;
//        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Contants.openActivity2 = 1;
//        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mMarkerPoints = new ArrayList<>();
        MapsInitializer.initialize(this);
        mGoogleMap.setOnCameraMoveStartedListener(TrackRideActivity.this);
        mGoogleMap.setOnCameraIdleListener(TrackRideActivity.this);

        // Custom Map Style
        try {
            boolean success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style));
        } catch (Resources.NotFoundException e) {
        }

        switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext())) {
            case ConnectionResult.SUCCESS:
                mGoogleMap.setMyLocationEnabled(true);
                mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
                mGoogleMap.getUiSettings().setZoomGesturesEnabled(true);
                mGoogleMap.getUiSettings().setCompassEnabled(true);
                mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
                mGoogleMap.getUiSettings().setRotateGesturesEnabled(true);
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                buildGoogleApiClient();
                mGoogleApiClient.connect();

                updateRideUI();
                if (null != driverLat && null != driverLong) {
                    LatLng currentLatLng = new LatLng(Double.parseDouble(driverLat), Double.parseDouble(driverLong));
                    marker = mGoogleMap.addMarker(
                            new MarkerOptions()
                                    .position(currentLatLng)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.car_ola))
                    );
                    marker.setAnchor(0.5f, 0.5f);
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, globalZoom));
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(currentLatLng).zoom(globalZoom).build();
                    mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                }

//DG                try {
//                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.parseDouble(driverLat), Double.parseDouble(driverLong))).zoom(16).build();
//                    mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//
//                    if(ride_status.equals("3")){
//                        drawMarker(new LatLng(Double.parseDouble(driverLat), Double.parseDouble(driverLong)));
//                        drawMarker(new LatLng(Double.parseDouble(pickupLat), Double.parseDouble(pickupLong)));
//                    }
//
//                    mGoogleMap.setOnMyLocationChangeListener(this);
//
//                } catch (Exception e) {
//                    Logger.e("Exception in on Map Ready     " + e.toString());
//DG                }

                break;
            case ConnectionResult.SERVICE_MISSING:
                Toast.makeText(getApplicationContext(), "SERVICE MISSING", Toast.LENGTH_SHORT).show();
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                Toast.makeText(getApplicationContext(), "UPDATE REQUIRED", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(getApplicationContext(), GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext()), Toast.LENGTH_SHORT).show();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onMyLocationChange(Location location) {

        if (ride_status.equals("6")) {
            Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                double latitude = mLastLocation.getLatitude();
                double longitude = mLastLocation.getLongitude();
                LatLng latLng = new LatLng(latitude, longitude);

//DG                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(16).build();
//DG                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }

        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000); //5 seconds
        mLocationRequest.setFastestInterval(3000); //3 seconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        //mLocationRequest.setSmallestDisplacement(0.1F); //1/10 meter
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    /// ADDED BY DG
    private void updateRideUI() {
        if (ride_status.equals("5")) {
            tv_cancel.setVisibility(View.INVISIBLE);
            ll_cancel_ride.setOnClickListener(null);
        }else if (ride_status.equals("3")) {
            // Draw Route
            // Update Car
            drawRoute(new LatLng(Double.parseDouble(driverLat), Double.parseDouble(driverLong)), new LatLng(Double.parseDouble(pickupLat), Double.parseDouble(pickupLong)), false, true);
            drawRoute(new LatLng(Double.parseDouble(driverLat), Double.parseDouble(driverLong)), new LatLng(Double.parseDouble(pickupLat), Double.parseDouble(pickupLong)), false, true);
        } else if (ride_status.equals("6")) {
            tv_cancel.setVisibility(View.INVISIBLE);
            ll_cancel_ride.setOnClickListener(null);
            tv_location.setText(dropLocation);
            tv_set.setText("DROP LOCATION");
            fl_gps1.setVisibility(View.VISIBLE);
            drawRoute(new LatLng(Double.parseDouble(pickupLat), Double.parseDouble(pickupLong)), new LatLng(Double.parseDouble(dropLat), Double.parseDouble(dropLong)), false, true);
            drawRoute(new LatLng(Double.parseDouble(pickupLat), Double.parseDouble(pickupLong)), new LatLng(Double.parseDouble(dropLat), Double.parseDouble(dropLong)), false, true);
        }
        if (null != driverID && driverID.length() > 0) {
            currentDriver = allDriversLiveLatLng.child(driverID);
            ValueEventListener currentDriverLatLongListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    FirebaseDriverLatLng tmpCurrentDriverPosition = dataSnapshot.getValue(FirebaseDriverLatLng.class);
                    if (null != tmpCurrentDriverPosition) {
                        if(drawActualDriverRoute == 0){
                            driverLat = String.valueOf(tmpCurrentDriverPosition.getLatitude());
                            driverLong = String.valueOf(tmpCurrentDriverPosition.getLongitude());
                            updateRideUI();
                            drawActualDriverRoute++;
                        }
                        LatLng currentDriverPosition = new LatLng(tmpCurrentDriverPosition.getLatitude(), tmpCurrentDriverPosition.getLongitude());
                        if (null != currentDriverPosition) {
                            float rotationAngle = (float) getAngleFromLatLng(marker.getPosition(), currentDriverPosition);
                            if (null != animationMove && animationMove.isRunning()) {
                                animationMove.end();
                            }
                            smoothRotateAndGoTheCar(marker, rotationAngle, currentDriverPosition);
                            CameraPosition cameraPosition = new CameraPosition.Builder().target(marker.getPosition()).zoom(globalZoom).build();
                            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };
            currentDriver.addValueEventListener(currentDriverLatLongListener);
        }
    }

    // GET ROTATION ANGLE OF TWO LatLng
    private double getAngleFromLatLng(LatLng pointA, LatLng pointB) {
        double dLon = (pointB.longitude - pointA.longitude);

        double y = Math.sin(dLon) * Math.cos(pointB.latitude);
        double x = Math.cos(pointA.latitude) * Math.sin(pointB.latitude) - Math.sin(pointA.latitude)
                * Math.cos(pointB.latitude) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;
        brng = 360 - brng; // count degrees counter-clockwise - remove to make clockwise

        return brng;
    }

    // Smoothly Rotate Car
    private void smoothRotateCar(final Marker marker, float rotation) {
        final ValueAnimator animation = ValueAnimator.ofFloat(marker.getRotation(), rotation);
        animation.setDuration(3 * (int) rotation);
        animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float animatedValue = (float) valueAnimator.getAnimatedValue();
                marker.setRotation(animatedValue);
            }
        });
        animation.start();
    }

    // Smooth Change Position
    private void smoothGoTheCar(final Marker marker, LatLng finalLatLng) {
        animationMove = ValueAnimator.ofFloat(0f, 100f);
        final double deltaLatitude = finalLatLng.latitude - marker.getPosition().latitude;
        //Toast.makeText(MainActivity.this, "deltaLatitude" + " " + deltaLatitude, Toast.LENGTH_SHORT).show();
        final double deltaLongitude = finalLatLng.longitude - marker.getPosition().longitude;
        animationMove.setDuration(1500);
        animationMove.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator updatedAnimation) {
                float deltaStep = (float) updatedAnimation.getAnimatedValue() - previousStep;
                previousStep = (float) updatedAnimation.getAnimatedValue();
                marker.setPosition(new LatLng(marker.getPosition().latitude + deltaLatitude * deltaStep * 1 / 100, marker.getPosition().longitude + deltaStep * deltaLongitude * 1 / 100));
            }
        });
        animationMove.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                previousStep = 0;
            }
        });
        animationMove.start();
    }

    // Smoothly Rotate Car
    private void smoothRotateAndGoTheCar(final Marker marker, float rotation, LatLng finalLatLng) {

        animationMove = ValueAnimator.ofFloat(0f, 100f);
        final double deltaLatitude = finalLatLng.latitude - marker.getPosition().latitude;
        //Toast.makeText(MainActivity.this, "deltaLatitude" + " " + deltaLatitude, Toast.LENGTH_SHORT).show();
        final double deltaLongitude = finalLatLng.longitude - marker.getPosition().longitude;
        animationMove.setDuration(1500);
        animationMove.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator updatedAnimation) {
                float deltaStep = (float) updatedAnimation.getAnimatedValue() - previousStep;
                previousStep = (float) updatedAnimation.getAnimatedValue();
                marker.setPosition(new LatLng(marker.getPosition().latitude + deltaLatitude * deltaStep * 1 / 100, marker.getPosition().longitude + deltaStep * deltaLongitude * 1 / 100));
            }
        });
        animationMove.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                previousStep = 0;
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), globalZoom));
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                previousStep = 0;
            }
        });

        final ValueAnimator animationRotate = ValueAnimator.ofFloat(marker.getRotation(), rotation);
        animationRotate.setDuration(3 * (int) rotation);
        animationRotate.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float animatedValue = (float) valueAnimator.getAnimatedValue();
                marker.setRotation(animatedValue);
            }
        });
        animationRotate.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                animationMove.start();
            }
        });
        animationRotate.start();
    }

    // Change Car Size during ZOOM of MAP
    @Override
    public void onCameraMove() {
        if (mGoogleMap != null) {
            if (null != marker) {
                CameraPosition cameraPosition = mGoogleMap.getCameraPosition();
                int zoomLavel = (int) cameraPosition.zoom;
                // globalZoom = zoomLavel;
                Log.d("ZOOM_DG", zoomLavel + "");
                BitmapDrawable bitmapCarIcon = (BitmapDrawable) getResources().getDrawable(R.drawable.car_ola);
                Bitmap b = bitmapCarIcon.getBitmap();
                Bitmap bitmap = Bitmap.createScaledBitmap(b, zoomLavel * 4, zoomLavel * 8, false);
                try {
                    marker.setIcon(BitmapDescriptorFactory.fromBitmap(bitmap));
                } catch (Exception ex) {
                }
            }
        }
    }

    // Draw Route Between Two Routes
    private void drawRoute(LatLng pointA, LatLng pointB, boolean showStartPoint, boolean showEndPoint) {
        String url = "https://maps.googleapis.com/maps/api/directions/json?" + "origin=" + pointA.latitude + "," + pointA.longitude + "&destination=" + pointB.latitude + "," + pointB.longitude + "&sensor=false";
        Log.d(" Direction URL:", url);
        DownloadTask downloadTask = new DownloadTask();
        downloadTask.execute(url);

        // REMOVE PREVIOUS MARKER
        if (null != routeStart) {
            routeStart.remove();
        }
        if (null != routeEnd) {
            routeEnd.remove();
        }

        if (showStartPoint && null != mGoogleMap) {
            routeStart = mGoogleMap.addMarker(new MarkerOptions().position(pointA).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_pin)));
        }
        if (showEndPoint && null != mGoogleMap) {
            routeEnd = mGoogleMap.addMarker(new MarkerOptions().position(pointB).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_pin_end)));
        }
    }

    @Override
    public void onCameraMoveStarted(int reason) {
        if (reason == REASON_GESTURE) {
            isMapInteractedWithUser = true;
            YoYo.with(Techniques.SlideOutUp).duration(700).repeat(0).playOn(dgHeaderArea);
            YoYo.with(Techniques.SlideOutDown).duration(700).repeat(0).playOn(dgFooterArea);
            //dgLocationContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCameraIdle() {
        if (isMapInteractedWithUser) {
            YoYo.with(Techniques.SlideInDown).duration(500).repeat(0).playOn(dgHeaderArea);
            YoYo.with(Techniques.SlideInUp).duration(500).repeat(0).playOn(dgFooterArea);
            isMapInteractedWithUser = false;
        }
    }

}