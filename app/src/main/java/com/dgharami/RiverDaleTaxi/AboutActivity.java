package com.dgharami.RiverDaleTaxi;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.models.about.About;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.parsing.HelpModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AboutActivity extends AppCompatActivity implements ApiFetcher {

    LinearLayout back;
    TextView abottext, version;
    public static Activity aboutactivity;

    LanguageManager languageManager;

    String language_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        getSupportActionBar().hide();
        aboutactivity = this;

        back = (LinearLayout) findViewById(R.id.bck);
        abottext = (TextView) findViewById(R.id.abouustextv);
        version = (TextView) findViewById(R.id.versionnametextV);

        languageManager = new LanguageManager(this);
        language_id = languageManager.getLanguageDetail().get(LanguageManager.LANGUAGE_ID);

        HelpModule helpModule = new HelpModule(this);
        helpModule.aboutUsApi(language_id);

        PackageManager manager = aboutactivity.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(AboutActivity.this.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version1 = info.versionName;
        version.setText("Version : ZWM-" + version1);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onAPIRunningState(int a) {

    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("About Us")) {
            About about;
            about = gson.fromJson(response, About.class);
            if (about.getResult().toString().equals("1")) {

                if (language_id.equals("1")) {
                    String desc = about.getDetails().getDescription();
                    abottext.setText(desc);
                } else if (language_id.equals("2")) {
                    String desc = about.getDetails().getDescriptionFrench();
                    abottext.setText(desc);
                } else if (language_id.equals("3")) {
                    String desc = about.getDetails().getDescriptionArabic();
                    abottext.setText(desc);
                }
            }
        }
    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }
}
