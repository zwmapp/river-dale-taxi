package com.dgharami.RiverDaleTaxi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dgharami.RiverDaleTaxi.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;


public class PlacesAdapter extends BaseAdapter {
    Context context;
    ArrayList<String> mainText = new ArrayList<>();
    ArrayList<String> secondaryText = new ArrayList<>();

    public PlacesAdapter(Context context, ArrayList<String> mainText, ArrayList<String> secondaryText) {
        this.context = context;
        this.mainText = mainText;
        this.secondaryText = secondaryText;
    }

    @Override
    public int getCount() {
        return mainText.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyHolder myHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.itemlayoutfordroplocation, parent, false);
            myHolder = new MyHolder(convertView);
            convertView.setTag(myHolder);
        } else {
            myHolder = (MyHolder) convertView.getTag();
        }
        myHolder.tv_main.setText(mainText.get(position));
        if (secondaryText.get(position) == null) {
            myHolder.tv_secondary.setText("");
        } else {
            myHolder.tv_secondary.setText(secondaryText.get(position));
        }
        return convertView;
    }

    static class MyHolder {
        @Bind(R.id.tv_main)
        TextView tv_main;
        @Bind(R.id.tv_secondary)
        TextView tv_secondary;

        public MyHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
