package com.dgharami.RiverDaleTaxi.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.dgharami.RiverDaleTaxi.AddCardActivity;
import com.dgharami.RiverDaleTaxi.R;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.manager.SessionManager;
import com.dgharami.RiverDaleTaxi.models.deviceid.DeviceId;
import com.dgharami.RiverDaleTaxi.models.viewcard.ViewCard;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.parsing.CustomerModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CardsAdapter extends BaseAdapter implements ApiFetcher {
    Context context;
    ViewCard viewCard;

    CustomerModule customerModule;

    SessionManager sessionManager;
    LanguageManager languageManager;
    String user_id, language_id;

    ProgressDialog pd;

    public CardsAdapter(Context context, ViewCard viewCard) {
        this.context = context;
        this.viewCard = viewCard;
        customerModule = new CustomerModule(this);
        sessionManager = new SessionManager(context);
        languageManager = new LanguageManager(context);
        user_id = sessionManager.getUserDetails().get(SessionManager.USER_ID);
        language_id = languageManager.getLanguageDetail().get(LanguageManager.LANGUAGE_ID);

        pd = new ProgressDialog(context);
        pd.setMessage("Loading...");

    }

    @Override
    public int getCount() {
        return viewCard.getDetails().size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final MyHolder myHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.itemlayoutforcards, parent, false);
            myHolder = new MyHolder(convertView);
            convertView.setTag(myHolder);
        } else {
            myHolder = (MyHolder) convertView.getTag();
        }

        String cardLastNumber = viewCard.getDetails().get(position).getCardNumber().substring(12);

        myHolder.tv_card_number.setText("XXXX-XXXX-XXXX-" + cardLastNumber);
        myHolder.tv_card_type.setText(viewCard.getDetails().get(position).getCardType());
        myHolder.dgCardName.setText(viewCard.getDetails().get(position).getCardName());
        myHolder.dgCardExp.setText(viewCard.getDetails().get(position).getCardExpMonth() + "/" + viewCard.getDetails().get(position).getCardExpYear());
        myHolder.ll_delete_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPopupButtonClick(myHolder.iv_menu_adapter, position);
//              initiatePopupWindow(myHolder.iv_menu_adapter, position);
            }
        });
        return convertView;
    }

    @Override
    public void onAPIRunningState(int a) {

        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            pd.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            pd.dismiss();
        }
    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("Delete Card")) {
            DeviceId deviceId;
            deviceId = gson.fromJson(response, DeviceId.class);
            if (deviceId.getResult().toString().equals("1")) {
                customerModule.viewCardApi(user_id, language_id);
            } else {

            }
        }

        if (apiName.equals("View Card")) {

            viewCard = gson.fromJson(response, ViewCard.class);
            if (viewCard.getResult().toString().equals("1")) {
                this.notifyDataSetChanged();
            } else {
//                this.notifyDataSetChanged();
                AddCardActivity.lv_cards.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }

    static class MyHolder {
        @Bind(R.id.tv_card_number)
        TextView tv_card_number;

        @Bind(R.id.tv_card_type)
        TextView tv_card_type;

        @Bind(R.id.iv_menu_adapter)
        ImageView iv_menu_adapter;

        @Bind(R.id.ll_delete_card)
        LinearLayout ll_delete_card;

        @Bind(R.id.dgCardName)
        TextView dgCardName;

        @Bind(R.id.dgCardExp)
        TextView dgCardExp;

        public MyHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void onPopupButtonClick(View button, final int position) {
        PopupMenu popup = new PopupMenu(context, button);
        popup.getMenuInflater().inflate(R.menu.menu_adapter, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                String t = (String) item.getTitle();

                if (t.equals("Delete")) {
                    customerModule.deleteCardApi(viewCard.getDetails().get(position).getCardId(), language_id);
                }
                return true;
            }
        });
        popup.show();
    }
}
