package com.dgharami.RiverDaleTaxi.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.androidnetworking.error.ANError;
import com.dgharami.RiverDaleTaxi.R;
import com.dgharami.RiverDaleTaxi.manager.SessionManager;
import com.dgharami.RiverDaleTaxi.models.deviceid.DeviceId;
import com.dgharami.RiverDaleTaxi.models.viewrides.ViewRides;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.parsing.RideModule;
import com.dgharami.RiverDaleTaxi.urls.Apis;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

public class RidesAdapter extends BaseAdapter implements ApiFetcher, Apis {

    Context context;
    ViewRides viewRides;
    String rideId;
    RideModule rideModule;

    ProgressDialog pd;
    SessionManager sessionManager;


    int trial = 0;

    public RidesAdapter(Context context, ViewRides viewRides) {
        this.context = context;
        this.viewRides = viewRides;
        pd = new ProgressDialog(context);
        pd.setMessage("Loading");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        sessionManager = new SessionManager(context);
        rideModule = new RideModule(this);
    }

    @Override
    public int getCount() {
        return viewRides.getMsg().size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.itemlayoutforrides, parent, false);
            holder = new Holder();
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.tv_pickup_addresss = (TextView) convertView.findViewById(R.id.tv_pickup_addresss);
        holder.tv_drop_addresss = (TextView) convertView.findViewById(R.id.tv_drop_addresss);
        holder.tv_date_time = (TextView) convertView.findViewById(R.id.tv_date_time);
        holder.tv_status = (TextView) convertView.findViewById(R.id.tv_status);
//        holder.tv_cancel_r = (TextView) convertView.findViewById(R.id.tv_cancel_r);
        holder.tv_car_name_rides = (TextView) convertView.findViewById(R.id.tv_car_name_rides);
        holder.iv_cancel = (ImageView) convertView.findViewById(R.id.iv_cancel);
        holder.iv_completed = (ImageView) convertView.findViewById(R.id.iv_completed);
        holder.iv_car_type_image = (ImageView) convertView.findViewById(R.id.iv_car_type_image);

        holder.tv_date_time.setText(viewRides.getMsg().get(position).getRideDate().toString() + ", " + viewRides.getMsg().get(position).getRideTime().toString());

        holder.tv_car_name_rides.setText(viewRides.getMsg().get(position).getCarTypeName());
        Picasso.with(context)
                .load(imageDomain + viewRides.getMsg().get(position).getCarTypeImage())
                .placeholder(R.drawable.dummy_pic)
                .error(R.drawable.dummy_pic)
                .fit()
                .into(holder.iv_car_type_image);

        if (viewRides.getMsg().get(position).getRideStatus().toString().equals("2")) {
            holder.tv_status.setText("");
//            holder.tv_cancel_r.setVisibility(View.GONE);
            holder.tv_pickup_addresss.setText(viewRides.getMsg().get(position).getPickupLocation().trim());
            holder.tv_drop_addresss.setText(viewRides.getMsg().get(position).getDropLocation().trim());
            holder.iv_completed.setVisibility(View.GONE);
            holder.iv_cancel.setVisibility(View.VISIBLE);
        } else if (viewRides.getMsg().get(position).getRideStatus().toString().equals("1")) {
            holder.tv_status.setText("SCHEDULED");
//            holder.tv_cancel_r.setVisibility(View.VISIBLE);
            holder.tv_pickup_addresss.setText(viewRides.getMsg().get(position).getPickupLocation().trim());
            holder.tv_drop_addresss.setText(viewRides.getMsg().get(position).getDropLocation().trim());
            holder.iv_completed.setVisibility(View.GONE);
            holder.iv_cancel.setVisibility(View.GONE);
        } else if (viewRides.getMsg().get(position).getRideStatus().toString().equals("3")) {
            holder.tv_status.setText("SCHEDULED");
//            holder.tv_cancel_r.setVisibility(View.GONE);
            holder.tv_pickup_addresss.setText(viewRides.getMsg().get(position).getPickupLocation().trim());
            holder.tv_drop_addresss.setText(viewRides.getMsg().get(position).getDropLocation().trim());
            holder.iv_completed.setVisibility(View.GONE);
            holder.iv_cancel.setVisibility(View.GONE);
        } else if (viewRides.getMsg().get(position).getRideStatus().toString().equals("4")) {
            holder.tv_status.setText("REJECTED");
//            holder.tv_cancel_r.setVisibility(View.GONE);
            holder.tv_pickup_addresss.setText(viewRides.getMsg().get(position).getPickupLocation().trim());
            holder.tv_drop_addresss.setText(viewRides.getMsg().get(position).getDropLocation().trim());
            holder.iv_completed.setVisibility(View.GONE);
            holder.iv_cancel.setVisibility(View.GONE);
        } else if (viewRides.getMsg().get(position).getRideStatus().toString().equals("5")) {
            holder.tv_status.setText("Driver Arrived");
//            holder.tv_cancel_r.setVisibility(View.GONE);
            holder.tv_pickup_addresss.setText(viewRides.getMsg().get(position).getPickupLocation().trim());
            holder.tv_drop_addresss.setText(viewRides.getMsg().get(position).getDropLocation().trim());
            holder.iv_completed.setVisibility(View.GONE);
            holder.iv_cancel.setVisibility(View.GONE);
        } else if (viewRides.getMsg().get(position).getRideStatus().toString().equals("6")) {
            holder.tv_status.setText("ONGOING");
//            holder.tv_cancel_r.setVisibility(View.GONE);
            holder.tv_pickup_addresss.setText(viewRides.getMsg().get(position).getBeginLocation().trim());
            holder.tv_drop_addresss.setText(viewRides.getMsg().get(position).getDropLocation().trim());
            holder.iv_completed.setVisibility(View.GONE);
            holder.iv_cancel.setVisibility(View.GONE);
        } else if (viewRides.getMsg().get(position).getRideStatus().toString().equals("7")) {
            holder.tv_status.setText("$ " + viewRides.getMsg().get(position).getAmount());
            holder.tv_pickup_addresss.setText(viewRides.getMsg().get(position).getBeginLocation().trim());
            holder.tv_drop_addresss.setText(viewRides.getMsg().get(position).getEndLocation().trim());
            holder.iv_completed.setVisibility(View.VISIBLE);
//            holder.tv_cancel_r.setVisibility(View.GONE);
            holder.iv_cancel.setVisibility(View.GONE);
        } else if (viewRides.getMsg().get(position).getRideStatus().toString().equals("8")) {
            holder.tv_status.setText("SCHEDULED");
//            holder.tv_cancel_r.setVisibility(View.VISIBLE);
            holder.tv_pickup_addresss.setText(viewRides.getMsg().get(position).getPickupLocation().trim());
            holder.tv_drop_addresss.setText(viewRides.getMsg().get(position).getDropLocation().trim());
            holder.iv_completed.setVisibility(View.GONE);
            holder.iv_cancel.setVisibility(View.GONE);
        } else if (viewRides.getMsg().get(position).getRideStatus().toString().equals("9")) {
            holder.tv_status.setText("Cancel by driver");
//            holder.tv_cancel_r.setVisibility(View.GONE);
            holder.tv_pickup_addresss.setText(viewRides.getMsg().get(position).getPickupLocation().trim());
            holder.tv_drop_addresss.setText(viewRides.getMsg().get(position).getDropLocation().trim());
            holder.iv_completed.setVisibility(View.GONE);
            holder.iv_cancel.setVisibility(View.GONE);
        }

//        holder.tv_cancel_r.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                rideId = viewRides.getMsg().get(position).getRideId();
//                rideModule.cancelRideApi(rideId);
//                trial = position;
//            }
//        });


        return convertView;
    }

    @Override
    public void onAPIRunningState(int a) {

        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            pd.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            pd.dismiss();
        }
    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("Cancel Ride")) {
            DeviceId deviceId;
            deviceId = gson.fromJson(response, DeviceId.class);
            if (deviceId.getResult().toString().equals("1")) {
                Toast.makeText(context, "" + deviceId.getMsg().toString(), Toast.LENGTH_SHORT).show();
                viewRides.getMsg().get(trial).setRideStatus("2");
                this.notifyDataSetChanged();
            } else {
                Log.e("err", deviceId.getMsg().toString());
            }
        }
    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }

    public static class Holder {
        public static TextView tv_date_time, tv_status, tv_pickup_addresss, tv_drop_addresss, tv_cancel_r, tv_car_name_rides;
        ImageView iv_cancel, iv_completed, iv_car_type_image;
    }
}