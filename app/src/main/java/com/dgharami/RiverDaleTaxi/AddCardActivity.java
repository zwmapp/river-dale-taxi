package com.dgharami.RiverDaleTaxi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.dgharami.RiverDaleTaxi.adapter.CardsAdapter;
import com.dgharami.RiverDaleTaxi.logger.Logger;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.manager.SessionManager;
import com.dgharami.RiverDaleTaxi.models.deviceid.DeviceId;
import com.dgharami.RiverDaleTaxi.models.viewcard.ViewCard;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.parsing.CustomerModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import es.dmoral.toasty.Toasty;
//import com.stripe.android.model.Card;

public class AddCardActivity extends AppCompatActivity implements ApiFetcher {

    EditText edt_card_number, edt_expiry, edt_cvv;

    LinearLayout ll_back_card;
    Button ll_submit_add_card;

//    TextView tv_no_card_added;

    ProgressDialog pd;

    CustomerModule customerModule;
    SessionManager sessionManager;

    LanguageManager languageManager;
    ViewCard viewCard;
    String language_id;

    public int pos = 0;

    String a;
    int keyDel;

    public static ListView lv_cards;

    String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);
        getSupportActionBar().hide();

        pd = new ProgressDialog(this,R.style.MyAlertDialogStyle);
        pd.setMessage(getString(R.string.loading));
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);

        customerModule = new CustomerModule(this);
        sessionManager = new SessionManager(this);
        user_id = sessionManager.getUserDetails().get(SessionManager.USER_ID);
        final String user_email = sessionManager.getUserDetails().get(SessionManager.USER_EMAIL);
        languageManager = new LanguageManager(this);
        language_id = languageManager.getLanguageDetail().get(LanguageManager.LANGUAGE_ID);

        edt_card_number = (EditText) findViewById(R.id.edt_card_number);
        edt_expiry = (EditText) findViewById(R.id.edt_expiry);
        edt_cvv = (EditText) findViewById(R.id.edt_cvv);

        lv_cards = (ListView) findViewById(R.id.lv_cards);

        String from = super.getIntent().getExtras().getString("From");


//        tv_no_card_added = (TextView) findViewById(R.id.tv_card);

        ll_submit_add_card = (Button) findViewById(R.id.ll_submit_add_card);
        ll_back_card = (LinearLayout) findViewById(R.id.ll_back_card);

        customerModule.viewCardApi(user_id, language_id);

        ll_back_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        edt_expiry.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                pos = edt_expiry.getText().length();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (edt_expiry.getText().length() == 2 && pos != 3) {
                    edt_expiry.setText(edt_expiry.getText().toString() + "/");
                    edt_expiry.setSelection(3);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edt_card_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                boolean flag = true;
                String eachBlock[] = edt_card_number.getText().toString().split("-");
                for (int i = 0; i < eachBlock.length; i++) {
                    if (eachBlock[i].length() > 4) {
                        flag = false;
                    }
                }
                if (flag) {

                    edt_card_number.setOnKeyListener(new View.OnKeyListener() {

                        @Override
                        public boolean onKey(View v, int keyCode, KeyEvent event) {

                            if (keyCode == KeyEvent.KEYCODE_DEL)
                                keyDel = 1;
                            return false;
                        }
                    });

                    if (keyDel == 0) {

                        if (((edt_card_number.getText().length() + 1) % 5) == 0) {

                            if (edt_card_number.getText().toString().split("-").length <= 3) {
                                edt_card_number.setText(edt_card_number.getText() + "-");
                                edt_card_number.setSelection(edt_card_number.getText().length());
                            }
                        }
                        a = edt_card_number.getText().toString();
                    } else {
                        a = edt_card_number.getText().toString();
                        keyDel = 0;
                    }

                } else {
                    edt_card_number.setText(a);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ll_submit_add_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String card_number = edt_card_number.getText().toString();
                String expiry = edt_expiry.getText().toString();
                String cvv = edt_cvv.getText().toString();

                if(card_number.length() < 12){
                    Toasty.error(AddCardActivity.this, "Please enter valid card number", Toast.LENGTH_SHORT, true).show();
                }else if(expiry.length() < 5){
                    Toasty.error(AddCardActivity.this, "Please enter valid exp date", Toast.LENGTH_SHORT, true).show();
                }else if(cvv.length() < 3){
                    Toasty.error(AddCardActivity.this, "Please enter valid CVV number", Toast.LENGTH_SHORT, true).show();
                }else{
                    String[] parts = expiry.split("/");
                    if(parts.length >= 2){
                        String month = parts[0];
                        String year = parts[1];
                        customerModule.saveCardApi(user_id, card_number, cvv, month, year, language_id);
                        Toasty.success(AddCardActivity.this, "Saving Card Details...", Toast.LENGTH_SHORT, true).show();
                    }else{
                        Toasty.error(AddCardActivity.this, "Please enter valid exp date", Toast.LENGTH_SHORT, true).show();
                    }
                }

                }
            //}
        });

        if (from.equals("Navigation")) {
//            Ignore This Part
        } else if (from.equals("MainActivity")) {
            lv_cards.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    String card_id = viewCard.getDetails().get(position).getCardId();

                    Intent intent = new Intent();
                    intent.putExtra("card_id", card_id);
                    setResult(Activity.RESULT_OK, intent);
                    finish();


                }
            });
        }


    }

    @Override
    public void onAPIRunningState(int a) {

        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            pd.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            pd.dismiss();
        }
    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("Save Card")) {
            DeviceId deviceId;
            deviceId = gson.fromJson(response, DeviceId.class);
            if (deviceId.getResult().toString().equals("1")) {
                Toast.makeText(this, "" + deviceId.getMsg(), Toast.LENGTH_LONG).show();
                customerModule.viewCardApi(user_id, language_id);

                edt_expiry.setText("");
                edt_card_number.setText("");
                edt_cvv.setText("");
                Toasty.success(this, "Card Details Saved", Toast.LENGTH_LONG).show();
            } else {
                Toasty.error(this, "" + deviceId.getMsg(), Toast.LENGTH_LONG).show();
            }
        }

        if (apiName.equals("View Card")) {
            Logger.i("DG_RESPONSE", response);
            viewCard = gson.fromJson(response, ViewCard.class);
            if (viewCard.getResult().toString().equals("1")) {

                lv_cards.setAdapter(new CardsAdapter(this, viewCard));
            } else {
                Toasty.error(this, "" + viewCard.getMsg(), Toast.LENGTH_LONG).show();
            }
        }


    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }
}
