package com.dgharami.RiverDaleTaxi;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.dgharami.RiverDaleTaxi.logger.Logger;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.manager.SessionManager;
import com.dgharami.RiverDaleTaxi.models.register.Register;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.parsing.AccountModule;
//import com.digits.sdk.android.Digits;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class LoginRegisterActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, ApiFetcher {

    private static final int RC_SIGN_IN = 007;
    ProgressDialog pd;
    GoogleApiClient mGoogleApiClient;
    Button btn_google, face;
    LoginButton btn_facebook;
    CallbackManager callbackManager;
    public LinearLayout ll_container;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    String cityLocation, user_name, user_email, social_id, user_phone, social_type;

    public static Activity loginRegisterActivity;

    SessionManager sessionManager;
    String phoneNumberForFacebook;


    Dialog dialog;

    AccountModule accountModule;
    LanguageManager languageManager;

    String language_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login_register);
        getSupportActionBar().hide();
        loginRegisterActivity = this;

        pd = new ProgressDialog(this, R.style.MyAlertDialogStyle);
        pd.setMessage("Loading");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);

        callbackManager = CallbackManager.Factory.create();
        AppEventsLogger.activateApp(this);
        fragmentManager = getSupportFragmentManager();
        accountModule = new AccountModule(LoginRegisterActivity.this);
        sessionManager = new SessionManager(this);

        languageManager = new LanguageManager(this);
        language_id = languageManager.getLanguageDetail().get(LanguageManager.LANGUAGE_ID);

        ll_container = (LinearLayout) findViewById(R.id.ll_container);
        btn_google = (Button) findViewById(R.id.btn_google);
        btn_facebook = (LoginButton) findViewById(R.id.btn_facebook);
        face = (Button) findViewById(R.id.face);

        cityLocation = super.getIntent().getExtras().getString("cityLocation");
        Bundle bundle = new Bundle();
        bundle.putString("cityLocation", cityLocation);

        //PhoneEmailFragment phoneEmailFragment = new PhoneEmailFragment();
        LoginFragment loginFragment = new LoginFragment();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.ll_container, loginFragment);
        fragmentTransaction.commit();

        face.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_facebook.performClick();
            }
        });

        btn_facebook.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday", "user_friends"));
        btn_facebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Logger.e("login User Id     " + loginResult.getAccessToken().getUserId());
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    social_id = object.getString("id");
                                    user_name = object.getString("name");
                                    user_email = object.getString("email");
                                    social_type = "2";
                                    Logger.e("Facebook Info     " + social_id + " | " + user_name + " | " + user_email);
                                    accountModule.sendSocialTokenApi(social_id, language_id);
                                } catch (JSONException e) {
                                    Logger.e("Exception From facebook       " + e.toString());
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Logger.e("Facebook Cancel        " + "Cancel");
            }

            @Override
            public void onError(FacebookException e) {
                Logger.e("Exception From facebook Error        " + e.toString());
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        btn_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signOut();
                revokeAccess();
                signIn();
            }
        });
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        Logger.e("status from Sign Out       " + status);
                    }
                });
    }

    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        Logger.e("status from revoke Access     " + status);
                    }
                });
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Logger.e("handleSignInResult        " + result.isSuccess());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            social_id = acct.getId();
            user_name = acct.getDisplayName();
            user_email = acct.getEmail();
            social_type = "1";
            Logger.e("Gmail Info    " + social_id + " | " + user_name + " | " + user_email);
            accountModule.sendSocialTokenApi(social_id, language_id);
        } else {
            Logger.e("else      " + result.getStatus());
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Logger.e("OnConnectionFailed       " + connectionResult);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onAPIRunningState(int a) {

        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            pd.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            pd.dismiss();
        }
    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("Send Social Token")) {
            Register register;
            register = gson.fromJson(response, Register.class);

            if (register.getResult().toString().equals("1")) {

                String user_id = register.getDetails().getUserId();
                String user_name = register.getDetails().getUserName();
                String user_email = register.getDetails().getUserEmail();
                String user_mobile = register.getDetails().getUserPhone();
                String user_image = register.getDetails().getUserImage();

                Toast.makeText(this, "" + register.getMsg(), Toast.LENGTH_SHORT).show();
                new SessionManager(this).createLoginSession(user_id, user_name, user_email, user_mobile, user_image);
                startActivity(new Intent(this, MainActivity.class).putExtra("cityLocation", cityLocation));
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                finish();
//                SplashActivity.splashActivity.finish();
            } else if (register.getResult().toString().equals("2")) {

//                Logger.e("Yhan Tak  " + " a gya");
//                Digits.clearActiveSession();
//
//                digitsButton.setCallback(new AuthCallback() {
//                    @Override
//                    public void success(DigitsSession session, String phoneNumber) {
//                        Log.e("success        ", phoneNumber.toString());
//                        AccountModule accountModule = new AccountModule(LoginRegisterActivity.this);
//                        accountModule.registrationSocialTokenApi(social_id, user_name, user_email, user_phone, social_type, language_id);
//                    }
//
//                    @Override
//                    public void failure(DigitsException exception) {
//                        Log.e("Exception        ", exception.toString());
//                    }
//                });
                dialogForPhone();
            } else {
                Toast.makeText(this, "" + register.getMsg(), Toast.LENGTH_LONG).show();
            }
        }

        if (apiName.equals("Register")) {
            Register register;
            register = gson.fromJson(response, Register.class);

            if (register.getResult().toString().equals("1")) {
                String user_id = register.getDetails().getUserId();
                String user_name = register.getDetails().getUserName();
                String user_email = register.getDetails().getUserEmail();
                String user_mobile = register.getDetails().getUserPhone();
                String user_image = register.getDetails().getUserImage();

                Toast.makeText(this, register.getMsg().toString(), Toast.LENGTH_SHORT).show();
                sessionManager.createLoginSession(user_id, user_name, user_email, user_mobile, user_image);
                startActivity(new Intent(this, MainActivity.class).putExtra("cityLocation", cityLocation));
//                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
//                finish();
                LoginRegisterActivity.loginRegisterActivity.finish();
                SplashActivity.splashActivity.finish();
            } else {
                Toast.makeText(this, "" + register.getMsg().toString(), Toast.LENGTH_SHORT).show();
            }
        }

//        if (apiName.equals("Otp Sent")) {
//            OtpSent otpSent;
//            otpSent = gson.fromJson(response, OtpSent.class);
//            if (otpSent.getResult().toString().equals("1")) {
//                String otp = otpSent.getDetails().getOtpSent();
//                startActivity(new Intent(getApplicationContext(), OtpActivity.class)
//                        .putExtra("name", user_name)
//                        .putExtra("email", user_email)
//                        .putExtra("phone", user_phone)
//                        .putExtra("password", "Nothing")
//                        .putExtra("otp", otp)
//                        .putExtra("cityLocation", cityLocation)
//                        .putExtra("social_id", social_id)
//                        .putExtra("social_type", social_type));
//                dialog.dismiss();
//                finish();
//            } else {
//                Toast.makeText(getApplicationContext(), "" + otpSent.getMsg(), Toast.LENGTH_SHORT).show();
//            }
//        }
    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }

    public void dialogForPhone() {
        dialog = new Dialog(LoginRegisterActivity.this, android.R.style.Theme_Holo_Light_DarkActionBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        dialog.setCancelable(true);
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_for_phone);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        final EditText etForFacebookMobile = (EditText) dialog.findViewById(R.id.etForFacebookMobile);
        Button buttonSignUpForFacebook = (Button) dialog.findViewById(R.id.buttonSignUpForFacebook);

        //Digits.clearActiveSession();

        buttonSignUpForFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                phoneNumberForFacebook = etForFacebookMobile.getText().toString().trim();
                AccountModule accountModule = new AccountModule(LoginRegisterActivity.this);
                accountModule.registrationSocialTokenApi(social_id, user_name, user_email, phoneNumberForFacebook, social_type, language_id);
            }
        });


       /* DigitsAuthButton digitsButton = (DigitsAuthButton) dialog.findViewById(R.id.auth_button);
        digitsButton.setText("Click Here For Phone Number");
        digitsButton.setBackgroundColor(getResources().getColor(R.color.colorFooter));
        digitsButton.setTextColor(getResources().getColor(R.color.colorText));
        digitsButton.setTextSize(15);

//        Digits.getSessionManager().clearActiveSession();

        digitsButton.setCallback(new AuthCallback() {
            @Override
            public void success(DigitsSession session, String phoneNumber) {
                Log.e("success        ", phoneNumber.toString());
                dialog.dismiss();

                AccountModule accountModule = new AccountModule(LoginRegisterActivity.this);
                accountModule.registrationSocialTokenApi(social_id, user_name, user_email, phoneNumber, social_type, language_id);
            }

            @Override
            public void failure(DigitsException exception) {
                Log.e("Exception        ", exception.toString());
                dialog.dismiss();
            }
        });*/


//        LinearLayout ll_done_phone = (LinearLayout) dialog.findViewById(R.id.ll_done_phone);
//        final EditText edt_phone_dialog = (EditText) dialog.findViewById(R.id.edt_phone_dialog);
//
//        ll_done_phone.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                user_phone = edt_phone_dialog.getText().toString();
//                if (user_phone.equals("")) {
//                    Toast.makeText(LoginRegisterActivity.this, "Please Enter Phone", Toast.LENGTH_SHORT).show();
//                } else {
//                    accountModule.otpSentApi(user_phone, "2", language_id);
//                }
//            }
//        });
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = fragmentManager.findFragmentById(R.id.ll_container);
        if (fragment instanceof LoginFragment) {
            finish();
        } else {
            LoginFragment loginFragment = new LoginFragment();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.ll_container, loginFragment);
            fragmentTransaction.commit();
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        }
    }
}
