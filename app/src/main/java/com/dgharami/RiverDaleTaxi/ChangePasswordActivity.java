package com.dgharami.RiverDaleTaxi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.manager.SessionManager;
import com.dgharami.RiverDaleTaxi.models.deviceid.DeviceId;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.parsing.AccountModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ChangePasswordActivity extends AppCompatActivity implements ApiFetcher {

    EditText edt_o_pass, edt_n_pass, edt_c_pass;
    LinearLayout ll_done_change_password, ll_back_change_password;
    public static Activity changepassword;
    LanguageManager languageManager;

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        getSupportActionBar().hide();
        changepassword = this;

        pd = new ProgressDialog(this,R.style.MyAlertDialogStyle);
        pd.setMessage("Loading");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);

        languageManager=new LanguageManager(this);
        final String language_id=languageManager.getLanguageDetail().get(LanguageManager.LANGUAGE_ID);

        edt_o_pass = (EditText) findViewById(R.id.edt_o_pass);
        edt_n_pass = (EditText) findViewById(R.id.edt_n_pass);
        edt_c_pass = (EditText) findViewById(R.id.edt_c_pass);
        ll_done_change_password = (LinearLayout) findViewById(R.id.ll_done_change_password);
        ll_back_change_password = (LinearLayout) findViewById(R.id.ll_back_change_password);

        edt_o_pass.setTypeface(Typeface.createFromAsset(getAssets(), "OpenSans_Regular.ttf"));
        edt_n_pass.setTypeface(Typeface.createFromAsset(getAssets(), "OpenSans_Regular.ttf"));
        edt_c_pass.setTypeface(Typeface.createFromAsset(getAssets(), "OpenSans_Regular.ttf"));

        final String user_id = new SessionManager(this).getUserDetails().get(SessionManager.USER_ID);

        ll_done_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String o_p = edt_o_pass.getText().toString();
                String n_p = edt_n_pass.getText().toString();
                String c_p = edt_c_pass.getText().toString();

                if (o_p.equals("")) {
                    Toast.makeText(ChangePasswordActivity.this, "Old Password field is empty", Toast.LENGTH_SHORT).show();
                } else if (n_p.equals("")) {
                    Toast.makeText(ChangePasswordActivity.this, "New Passwprd field is empty", Toast.LENGTH_SHORT).show();
                } else if (c_p.equals("")) {
                    Toast.makeText(ChangePasswordActivity.this, "Confirm Password field is empty", Toast.LENGTH_SHORT).show();
                } else if (n_p.length() < 6) {
                    Toast.makeText(ChangePasswordActivity.this, "New password id of min. 6 character", Toast.LENGTH_SHORT).show();
                } else if (!(c_p.equals(n_p))) {
                    Toast.makeText(ChangePasswordActivity.this, "Password Not Matched !!!", Toast.LENGTH_SHORT).show();
                } else {
                    AccountModule accountModule = new AccountModule(ChangePasswordActivity.this);
                    accountModule.cpApi(user_id, o_p, n_p,language_id);
                }
            }
        });

        ll_back_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onAPIRunningState(int a) {

        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            pd.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            pd.dismiss();
        }

    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("Change Password")) {
            DeviceId deviceId;
            deviceId = gson.fromJson(response, DeviceId.class);
            if (deviceId.getResult().toString().equals("1")) {
                Toast.makeText(this, "" + deviceId.getMsg(), Toast.LENGTH_LONG).show();
                finish();
            } else {
                Toast.makeText(this, "" + deviceId.getMsg(), Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }
}