package com.dgharami.RiverDaleTaxi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.models.deviceid.DeviceId;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.parsing.ViewModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class CouponActivity extends AppCompatActivity implements ApiFetcher {
    public static Activity couponActivity;
    EditText couponcode;
    TextView tv_coupon_invalid, tv_coupon_error, tv_coupon_text;


    LanguageManager languageManager;
    LinearLayout ll_cancel, ll_apply, ll_back_coupons;

    ProgressDialog pd;

    String coupon_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon);
        getSupportActionBar().hide();
        couponActivity = this;

        tv_coupon_invalid = (TextView) findViewById(R.id.tv_coupon_invalid);
        tv_coupon_error = (TextView) findViewById(R.id.tv_coupon_error);
        tv_coupon_text = (TextView) findViewById(R.id.tv_coupon_text);
        ll_cancel = (LinearLayout) findViewById(R.id.ll_cancel);
        ll_apply = (LinearLayout) findViewById(R.id.ll_apply);
        ll_back_coupons = (LinearLayout) findViewById(R.id.ll_back_coupons);
        couponcode = (EditText) findViewById(R.id.editTextcc);

        couponcode.setTypeface(Typeface.createFromAsset(getAssets(), "OpenSans_Regular.ttf"));

        languageManager = new LanguageManager(this);
        final String language_id = languageManager.getLanguageDetail().get(LanguageManager.LANGUAGE_ID);

        pd = new ProgressDialog(this,R.style.MyAlertDialogStyle);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);

        ll_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                finish();
                overridePendingTransition(R.anim.animation5, R.anim.animation6);
            }
        });

        ll_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coupon_text = couponcode.getText().toString();
                ViewModule viewModule = new ViewModule(CouponActivity.this);
                viewModule.applyCouponsApi(coupon_text, language_id);
            }
        });

        ll_back_coupons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onAPIRunningState(int a) {

        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            pd.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            pd.dismiss();
        }

    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("Apply Coupons")) {
            DeviceId deviceId;
            deviceId = gson.fromJson(response, DeviceId.class);

            if (deviceId.getResult().toString().equals("1")) {
                Intent intent = new Intent();
                intent.putExtra("coupon_text", coupon_text);
                setResult(Activity.RESULT_OK, intent);
                finish();
                Toast.makeText(this, "" + deviceId.getMsg().toString(), Toast.LENGTH_SHORT).show();
            } else {
                tv_coupon_text.setVisibility(View.GONE);
                tv_coupon_error.setVisibility(View.VISIBLE);
                tv_coupon_invalid.setVisibility(View.VISIBLE);
                couponcode.setText("");
            }
        }
    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }
}
