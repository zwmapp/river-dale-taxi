package com.dgharami.RiverDaleTaxi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.manager.SessionManager;
import com.dgharami.RiverDaleTaxi.models.register.Register;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.parsing.AccountModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ResetPasswordActivity extends AppCompatActivity implements ApiFetcher {

    EditText edt_phone_forgot,edt_email_forgot;
    public static EditText edt_otp_enter;
    LinearLayout  ll_done_forgot;
    public static Activity resetPasswordActivity;

    LanguageManager languageManager;

    ProgressDialog pd;

    String cityLocation, phone, otp,language_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        getSupportActionBar().hide();
        resetPasswordActivity = this;

        pd = new ProgressDialog(this,R.style.MyAlertDialogStyle);
        pd.setMessage("Loading...");

        languageManager=new LanguageManager(this);
        language_id=languageManager.getLanguageDetail().get(LanguageManager.LANGUAGE_ID);


        ll_done_forgot = (LinearLayout) findViewById(R.id.ll_done_forgot);
        edt_email_forgot = (EditText) findViewById(R.id.edt_email_forgot);
        edt_phone_forgot = (EditText) findViewById(R.id.edt_phone_forgot);
        edt_otp_enter = (EditText) findViewById(R.id.edt_otp_enter);

        edt_email_forgot.setTypeface(Typeface.createFromAsset(getAssets(), "OpenSans_Regular.ttf"));
        edt_phone_forgot.setTypeface(Typeface.createFromAsset(getAssets(), "OpenSans_Regular.ttf"));

        phone = super.getIntent().getExtras().getString("phone");
        if(null != phone){
            edt_phone_forgot.setText(phone);
        }

        otp = super.getIntent().getExtras().getString("otp");
        cityLocation = super.getIntent().getExtras().getString("cityLocation");
        edt_otp_enter.setText(otp);

        ll_done_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String registeredMobileNo = edt_phone_forgot.getText().toString().trim();
                String password = edt_email_forgot.getText().toString().trim();
                String otp_enter = edt_otp_enter.getText().toString().trim();
                if (otp_enter.equals(otp)) {
                    AccountModule accountModule = new AccountModule(ResetPasswordActivity.this);
                    accountModule.createPasswordApi(registeredMobileNo, password,language_id);
                } else {
                    Toast.makeText(ResetPasswordActivity.this, "Wrong Otp", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onAPIRunningState(int a) {

        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            pd.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            pd.dismiss();
        }
    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("Create Password")) {
            Register register;
            register = gson.fromJson(response, Register.class);

            if (register.getResult().toString().equals("1")) {

                String user_id = register.getDetails().getUserId();
                String user_name = register.getDetails().getUserName();
                String user_email = register.getDetails().getUserEmail();
                String user_mobile = register.getDetails().getUserPhone();
                String user_image = register.getDetails().getUserImage();

                Toast.makeText(getApplicationContext(), "" + register.getMsg(), Toast.LENGTH_SHORT).show();
                new SessionManager(getApplicationContext()).createLoginSession(user_id, user_name, user_email, user_mobile, user_image);
                startActivity(new Intent(getApplicationContext(), MainActivity.class).putExtra("cityLocation", cityLocation));
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                finish();
                LoginRegisterActivity.loginRegisterActivity.finish();
//                SplashActivity.splashActivity.finish();
            } else {
                Toast.makeText(getApplicationContext(), "" + register.getMsg(), Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }
}
