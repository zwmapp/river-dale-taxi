package com.dgharami.RiverDaleTaxi.others;

public class MessageEvent {
    public final String value;
    public final int result;

    public MessageEvent(String value, int result) {
        this.value = value;
        this.result = result;
    }
}
