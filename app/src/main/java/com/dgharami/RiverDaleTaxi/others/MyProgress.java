package com.dgharami.RiverDaleTaxi.others;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dgharami.RiverDaleTaxi.R;
public class MyProgress {

    Context context;
    Drawable drawable;

    public MyProgress(Context context, Drawable drawable) {
        this(context);
        this.drawable = drawable;
    }

    public MyProgress(Context context) {
        this.context = context;
    }

    public Dialog getProgressDialog(final String title) {
        Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        dialog.setContentView(R.layout.dialog_for_loading);
        dialog.setCancelable(true);

        TextView txtLoading = (TextView) dialog.findViewById(R.id.txtLoading);
        ProgressBar progressBar=(ProgressBar)dialog.findViewById(R.id.progressBar);

        if (drawable != null)
        {
            progressBar.setVisibility(View.VISIBLE);
        }
        txtLoading.setText(title);
        return dialog;
    }
}
