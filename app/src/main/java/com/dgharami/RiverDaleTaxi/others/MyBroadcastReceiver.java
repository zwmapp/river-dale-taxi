package com.dgharami.RiverDaleTaxi.others;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;

import com.dgharami.RiverDaleTaxi.logger.Logger;

public class MyBroadcastReceiver extends BroadcastReceiver {

//    Dialog dialog;

    @Override
    public void onReceive(final Context context, Intent intent) {

        String ride_status = intent.getExtras().getString("ride_status");
        String ride_id = intent.getExtras().getString("ride_id");
        Logger.e("id        " + ride_id);
        Logger.e("status        " + ride_status);

        Intent i = new Intent("broadCastName");
        i.putExtra("ride_id",ride_id);
        i.putExtra("ride_status",ride_status);
        context.sendBroadcast(i);

        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(context, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }

//        dialog = new Dialog(MainActivity.mainActivity, android.R.style.Theme_Translucent_NoTitleBar);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        Window window = dialog.getWindow();
//        window.setGravity(Gravity.CENTER);
//        dialog.setContentView(R.layout.dialog_for_acceptride);
//        dialog.setCancelable(false);
//
//        LinearLayout ll_ok_accept = (LinearLayout) dialog.findViewById(R.id.ll_ok_accept);
//        ll_ok_accept.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                MainActivity.mainActivity.startActivity(new Intent(MainActivity.mainActivity, TrackRideActivity.class)
//                        .putExtra("rideId", MyFirebaseMessagingService.pn_ride_id)
//                        .putExtra("cancelButton", "0"));
//                dialog.dismiss();
//            }
//        });
//        dialog.show();
    }
}
