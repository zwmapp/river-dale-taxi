package com.dgharami.RiverDaleTaxi.others;

public class MessageEventLatLong {
    public final String lati;
    public final String longi;

    public MessageEventLatLong(String lati, String longi) {
        this.lati = lati;
        this.longi = longi;
    }
}
