package com.dgharami.RiverDaleTaxi.others;

/**
 * Created by debasish on 3/23/2018.
 */

public class FirebaseDriverLatLng {

    public double latitude = 0.0;

    public double longitude = 0.0;

    public FirebaseDriverLatLng(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public FirebaseDriverLatLng() {
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
