package com.dgharami.RiverDaleTaxi.parsing;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.dgharami.RiverDaleTaxi.logger.Logger;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.urls.Apis;

/**
 * Created by Bhuvneshwar on 11/3/2016.
 */
public class CustomerModule implements Apis {
    ApiFetcher apiFetcher;

    public CustomerModule(ApiFetcher apiFetcher) {
        this.apiFetcher = apiFetcher;
    }

    public void trackDriverApi(String trip_id, String language_id) {

        String url = trackDriver + "?ride_id=" + trip_id;
        Logger.e("url       " + url);

        AndroidNetworking.post(trackDriver)
                .addBodyParameter("ride_id", trip_id)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onFetchComplete("" + response, "Track Driver Before Arrived");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void ratingApi(String user_id, String driver_id, String rating1, String language_id) {

        String url = rating + "?user_id=" + user_id + "&driver_id=" + driver_id + "&rating=" + rating1;
        Logger.e("url       " + url);

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(rating)
                .addBodyParameter("user_id", user_id)
                .addBodyParameter("driver_id", driver_id)
                .addBodyParameter("rating_star", rating1)
                .addBodyParameter("comment", "")
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Rating");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void paymentApi(String order_id, String user_id, String payment_id, String payment_method, String payment_platform, String payment_amount, String payment_date_time, String payment_status, String language_id) {

        String url = payment + "?order_id=" + order_id + "&user_id=" + user_id + "&payment_id=" + payment_id + "&payment_method=" + payment_method + "&payment_platform=" + payment_platform + "&payment_amount=" + payment_amount + "&payment_date_time=" + payment_date_time + "&payment_status=" + payment_status;
        Logger.e("url     " + url);

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(payment)
                .addBodyParameter("order_id", order_id)
                .addBodyParameter("user_id", user_id)
                .addBodyParameter("payment_id", payment_id)
                .addBodyParameter("payment_method", payment_method)
                .addBodyParameter("payment_platform", payment_platform)
                .addBodyParameter("payment_amount", payment_amount)
                .addBodyParameter("payment_date_time", payment_date_time)
                .addBodyParameter("payment_status", payment_status)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Payment");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                    }
                });
    }

    public void resendInvoiceApi(String ride_id, String language_id) {

        String url = resendInvoice + "?ride_id=" + ride_id;
        Logger.e("url     " + url);

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(resendInvoice)
                .addBodyParameter("ride_id", ride_id)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Resend Invoice");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                    }
                });
    }

    public void forceUpdateApi(String version_code, String language_id) {

        String url = forceUpdate + "?version_id=1&version_code=" + version_code;
        Logger.e("url       " + url);

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(forceUpdate)
                .addBodyParameter("version_id", "1")
                .addBodyParameter("version_code", version_code)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Force Update");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                        Logger.e("err body       " + anError.getErrorBody());
                        Logger.e("err detail      " + anError.getErrorDetail());
                        Logger.e("err message      " + anError.getMessage());
                        Logger.e("err code      " + anError.getErrorCode());
                        Logger.e("err localize message      " + anError.getLocalizedMessage());
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                    }
                });
    }

    public void updateApi(String version_code, String language_id) {

        String url = update + "?version_id=1&version_code=" + version_code;
        Logger.e("url       " + url);

        AndroidNetworking.post(update)
                .addBodyParameter("version_id", "1")
                .addBodyParameter("version_code", version_code)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onFetchComplete("" + response, "Update");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void cancelReasonApi(String language_id) {

        String url = cancelReason;
        Logger.e("url       " + url);

        AndroidNetworking.post(cancelReason)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onFetchComplete("" + response, "View Reasons");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void saveCardApi(String user_id, String card_number, String cvv, String month, String year, String language_id) {

        String url = saveCard + "?user_id=" + user_id + "&card_number=" + card_number + "&card_cvv=" + cvv + "&card_exp_month=" + month + "&card_exp_year=" + year + "&language_id=" + language_id;
        Logger.e("url       " + url);

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(saveCard)
                .addBodyParameter("user_id", user_id)
                .addBodyParameter("card_number", card_number)
                .addBodyParameter("card_cvv", cvv)
                .addBodyParameter("card_exp_month", month)
                .addBodyParameter("card_exp_year", year)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Save Card");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                    }
                });
    }

    public void viewCardApi(String user_id, String language_id) {

        String url = viewCard + "?user_id=" + user_id + "&language_id=" + language_id;
        Logger.e("url       " + url);

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(viewCard)
                .addBodyParameter("user_id", user_id)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "View Card");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                    }
                });
    }

    public void deleteCardApi(String card_id, String language_id) {

        String url = deleteCard + "?card_id=" + card_id + "language_id=" + language_id;
        Logger.e("url       " + url);

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(deleteCard)
                .addBodyParameter("card_id", card_id)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Delete Card");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                    }
                });
    }

    public void payWithCardApi(String ride_id, String language_id, String amount) {

        String url = payWthCard + "?ride_id=" + ride_id + "language_id=" + language_id + "&amount=" + amount;
        Logger.e("url       " + url);

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(payWthCard)
                .addBodyParameter("ride_id", ride_id)
                .addBodyParameter("language_id", language_id)
                .addBodyParameter("amount", amount)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Pay With Card");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                    }
                });
    }

    public void viewInvoiceApi(String order_id, String language_id) {

        String url = viewInvoice + "?order_id=" + order_id + "&language_id=" + language_id;
        Logger.e("url       " + url);

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(viewInvoice)
                .addBodyParameter("order_id", order_id)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onFetchComplete("" + response, "View Invoice");
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                    }
                });
    }
}
