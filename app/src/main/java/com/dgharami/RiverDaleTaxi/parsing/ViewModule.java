package com.dgharami.RiverDaleTaxi.parsing;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.dgharami.RiverDaleTaxi.logger.Logger;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.urls.Apis;

/**
 * Created by Bhuvneshwar on 11/2/2016.
 */
public class ViewModule implements Apis {
    ApiFetcher apiFetcher;

    public ViewModule(ApiFetcher apiFetcher) {
        this.apiFetcher = apiFetcher;
    }

    public void viewCitiesApi(String language_id) {

        AndroidNetworking.post(viewCities)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onFetchComplete("" + response, "View Cities");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void viewCarsApi(String city, String user_lat, String user_long,String language_id) {

        String url = viewCars + "?city=" + city + "&user_lat=" + user_lat + "&user_long=" + user_long;
        Logger.e("url       " + url);

        AndroidNetworking.post(viewCars)
                .addBodyParameter("city", city)
                .addBodyParameter("user_lat", user_lat)
                .addBodyParameter("user_long", user_long)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
//                        Logger.e("response" + response);
                        apiFetcher.onFetchComplete("" + response, "View Cars");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void viewCarsByCityApi(String city,String language_id) {

        AndroidNetworking.post(viewCarByCities)
                .addBodyParameter("city", city)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onFetchComplete("" + response, "View Cars");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void viewDriversApi(String lati, String longi, String city_id, String car_id,String language_id) {

        String url = viewDrivers + "?user_lat=" + lati + "&user_long=" + longi + "&city_id=" + city_id + "&car_type_id=" + car_id;
        Logger.e("url       " + url);

        AndroidNetworking.post(viewDrivers)
                .addBodyParameter("user_lat", lati)
                .addBodyParameter("user_long", longi)
                .addBodyParameter("city_id", city_id)
                .addBodyParameter("car_type_id", car_id)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
//                        Logger.e("response" + response);
                        apiFetcher.onFetchComplete("" + response, "View Drivers");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void applyCouponsApi(String coupon_text,String language_id) {

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(applyCoupon)
                .addBodyParameter("coupon_code", coupon_text)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Apply Coupons");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void viewRateCardApi(String city, String car_type_id,String language_id) {

        String url = viewRateCardCity + "?city=" + city + "&car_type_id=" + car_type_id;
        Logger.e("url       " + url);

        AndroidNetworking.post(viewRateCardCity)
                .addBodyParameter("city", city)
                .addBodyParameter("car_type_id", car_type_id)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onFetchComplete("" + response, "View Rate Card");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void viewRateCardCityApi(String city, String car_type_id,String language_id) {

        String url = viewRateCardCity + "?city=" + city + "&car_type_id=" + car_type_id;
        Logger.e("url       " + url);

        AndroidNetworking.post(viewRateCard)
                .addBodyParameter("city", city)
                .addBodyParameter("car_type_id", car_type_id)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onFetchComplete("" + response, "View Rate Card");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void viewPaymentOptionApi(String language_id) {

        String url = viewPaymentOption + "?language_id=" + language_id ;
        Logger.e("url       " + url);

        AndroidNetworking.post(viewPaymentOption)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onFetchComplete("" + response, "View Payment Option");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }
}
