package com.dgharami.RiverDaleTaxi.parsing;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.dgharami.RiverDaleTaxi.logger.Logger;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.urls.Apis;

/**
 * Created by Bhuvneshwar on 10/27/2016.
 */
public class RideModule implements Apis {

    ApiFetcher apiFetcher;

    public RideModule(ApiFetcher apiFetcher) {
        this.apiFetcher = apiFetcher;
    }

    public void rideNowApi(String user_id, String coupon_code, String pickup_lat, String pickup_long, String pickup_location, String drop_lat, String drop_long, String drop_location, String car_type_id, String language_id, String payment_option_id, String card_id) {

        String url = rideNow + "?user_id=" + user_id + "&coupon_code=" + coupon_code + "&pickup_lat=" + pickup_lat + "&pickup_long=" + pickup_long + "&pickup_location=" + pickup_location + "&drop_lat=" + drop_lat + "&drop_long=" + drop_long + "&drop_location=" + drop_location + "&ride_type=1&ride_status=1&car_type_id=" + car_type_id;
        Logger.e("ride now url      " + url);

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(rideNow)
                .addBodyParameter("user_id", user_id)
                .addBodyParameter("coupon_code", coupon_code)
                .addBodyParameter("pickup_lat", pickup_lat)
                .addBodyParameter("pickup_long", pickup_long)
                .addBodyParameter("pickup_location", pickup_location)
                .addBodyParameter("drop_lat", drop_lat)
                .addBodyParameter("drop_long", drop_long)
                .addBodyParameter("drop_location", drop_location)
                .addBodyParameter("ride_type", "1")
                .addBodyParameter("ride_status", "1")
                .addBodyParameter("car_type_id", car_type_id)
                .addBodyParameter("language_id", language_id)
                .addBodyParameter("payment_option_id", payment_option_id)
                .addBodyParameter("card_id", card_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onFetchComplete("" + response, "Ride Now");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void rideLaterApi(String user_id, String coupon_code, String pickup_lat, String pickup_long, String pickup_location, String drop_lat, String drop_long, String drop_location, String pickup_date, String pickup_time, String car_type_id, String language_id, String payment_option_id, String card_id) {

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(rideLater)
                .addBodyParameter("user_id", user_id)
                .addBodyParameter("coupon_code", coupon_code)
                .addBodyParameter("pickup_lat", pickup_lat)
                .addBodyParameter("pickup_long", pickup_long)
                .addBodyParameter("pickup_location", pickup_location)
                .addBodyParameter("drop_lat", drop_lat)
                .addBodyParameter("drop_long", drop_long)
                .addBodyParameter("drop_location", drop_location)
                .addBodyParameter("later_date", pickup_date)
                .addBodyParameter("later_time", pickup_time)
                .addBodyParameter("ride_type", "2")
                .addBodyParameter("ride_status", "1")
                .addBodyParameter("car_type_id", car_type_id)
                .addBodyParameter("language_id", language_id)
                .addBodyParameter("payment_option_id", payment_option_id)
                .addBodyParameter("card_id", card_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Ride Later");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void viewRideInfoApi(String ride_id, String language_id) {
        String url = viewRideInfo + "?ride_id=" + ride_id;
        Logger.e("url       " + url);

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(viewRideInfo)
                .addBodyParameter("ride_id", ride_id)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "View Ride Info");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                    }
                });
    }

    public void rideEstimateApi(String time, String distance, String city_id, String car_id, String language_id) {
        String url = rideEstimate + "?distance=" + distance + "&city_id=" + city_id + "&car_type_id=" + car_id + "&time=" + time;
        Logger.e("url       " + url);

//        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(rideEstimate)
                .addBodyParameter("distance", distance)
                .addBodyParameter("time", time)
                .addBodyParameter("city_id", city_id)
                .addBodyParameter("car_type_id", car_id)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
//                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Ride Estimate");
                    }

                    @Override
                    public void onError(ANError anError) {
//                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                    }
                });
    }

    public void cancelRideApi(String ride_id, String reason_id, String reason_text, String language_id) {
        String url = cancelRide + "?ride_id" + ride_id + "&ride_status=2&reason_text="+reason_text+"&reason_id=" + reason_id;
        Logger.e("url       " + url);

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(cancelRide)
                .addBodyParameter("ride_id", ride_id)
                .addBodyParameter("ride_status", "2")
                .addBodyParameter("reason_id", reason_id)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Cancel Ride");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                    }
                });
    }

    public void myRidesApi(String user_id, String language_id) {
        String url = viewRides + "?user_id" + user_id;
        Logger.e("url       " + url);

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(viewRides)
                .addBodyParameter("user_id", user_id)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "My Rides");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                    }
                });
    }

    public void viewDoneRideApi(String done_ride_id, String language_id) {

        String url = viewDoneRide + "?done_ride_id" + done_ride_id;
        Logger.e("url       " + url);

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(viewDoneRide)
                .addBodyParameter("done_ride_id", done_ride_id)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "View Done Ride");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                    }
                });


    }
}
