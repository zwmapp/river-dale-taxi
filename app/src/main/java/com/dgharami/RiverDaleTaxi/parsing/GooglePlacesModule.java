package com.dgharami.RiverDaleTaxi.parsing;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dgharami.RiverDaleTaxi.adapter.PlacesAdapter;
import com.dgharami.RiverDaleTaxi.logger.Logger;
import com.dgharami.RiverDaleTaxi.models.distance.Distance;
import com.dgharami.RiverDaleTaxi.models.googleplaces.GooglePlaces;
import com.dgharami.RiverDaleTaxi.models.googleplaceslatlong.GooglePlaceLatLong;
import com.dgharami.RiverDaleTaxi.others.MessageEvent;
import com.dgharami.RiverDaleTaxi.others.MessageEventLatLong;
import com.dgharami.RiverDaleTaxi.others.MyProgress1;
import com.dgharami.RiverDaleTaxi.singleton.VolleySingleton;
import com.dgharami.RiverDaleTaxi.DropLocationActivity;
import com.dgharami.RiverDaleTaxi.MainActivity;
import com.dgharami.RiverDaleTaxi.urls.Apis;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class GooglePlacesModule implements Apis {

    public static String latitude = "", longitude = "", value = "";

    public static String time = "Unknown";
    public static String timeVal = "0";

    Context context;

    Dialog dialog;

    ProgressDialog pd;

    public static ArrayList<String> description = new ArrayList<>();
    public static ArrayList<String> placeId = new ArrayList<>();
    public static ArrayList<String> mainText = new ArrayList<>();
    public static ArrayList<String> secondaryText = new ArrayList<>();
    ArrayList<Integer> alValue = new ArrayList<>();
    ArrayList<String> alDriverId = new ArrayList<>();

    int number;
    int increment;

    public GooglePlacesModule(Context context) {
        this.context = context;
    }

    public void parsingDrop(String text) {
        String url = googlePlace + "&input=" + text;
        url = url.replace(" ", "%20");
        Logger.e("url       " + url);

//        DropLocationActivity.progressBar2.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Logger.e("response       " + response);
                description.clear();
                mainText.clear();
                secondaryText.clear();
                placeId.clear();
                try {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    GooglePlaces googlePlaces = new GooglePlaces();
                    googlePlaces = gson.fromJson(response, GooglePlaces.class);
                    if (googlePlaces.getStatus().equals("OK")) {
                        DropLocationActivity.progressBar2.setVisibility(View.GONE);
                        for (int i = 0; i < googlePlaces.getPredictions().size(); i++) {
                            description.add(googlePlaces.getPredictions().get(i).getDescription());
                            mainText.add(googlePlaces.getPredictions().get(i).getStructuredFormatting().getMainText());
                            secondaryText.add(googlePlaces.getPredictions().get(i).getStructuredFormatting().getSecondaryText());
                            placeId.add(googlePlaces.getPredictions().get(i).getPlaceId());
                        }
                        Logger.e("secondary     " + secondaryText + "");
                        DropLocationActivity.lv_drop_location.setVisibility(View.VISIBLE);
                        DropLocationActivity.lv_drop_location.setAdapter(new PlacesAdapter(context, mainText, secondaryText));
                    } else {
                        DropLocationActivity.progressBar2.setVisibility(View.GONE);
                        DropLocationActivity.lv_drop_location.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DropLocationActivity.progressBar2.setVisibility(View.GONE);
                if (error instanceof NetworkError)
                    Toast.makeText(context, "Internet Problem", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
        DropLocationActivity.progressBar2.setVisibility(View.VISIBLE);
        queue.add(stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
    }

    public void parsingLatLong(String place_id) {

        String url = googlePlaceLatLong + "&placeid=" + place_id;
        url = url.replace(" ", "%20");
        Logger.e("url           " + url);

        dialog = new MyProgress1(context).getProgressDialog();


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Logger.e("response     " + response);
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                try {
                    GooglePlaceLatLong googlePlaceLatLong = new GooglePlaceLatLong();
                    googlePlaceLatLong = gson.fromJson(response, GooglePlaceLatLong.class);
                    if (googlePlaceLatLong.getStatus().equals("OK")) {
                        dialog.dismiss();
                        latitude = googlePlaceLatLong.getResult().getGeometry().getLocation().getLat().toString();
                        longitude = googlePlaceLatLong.getResult().getGeometry().getLocation().getLng().toString();
                        EventBus.getDefault().post(new MessageEventLatLong(latitude, longitude));
                    } else {
                        dialog.dismiss();
                    }
                } catch (Exception e) {
                    Logger.e("Exception in Google Place Lat Long    " + e.toString());
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("exception", "" + error);
                dialog.dismiss();
            }
        });
        RequestQueue requestQueue = VolleySingleton.getInstance(context).getRequestQueue();
        dialog.show();
        requestQueue.add(stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
    }

    public void minDistance(double pickup_lat, double pickup_long, double driver_lat, double driver_long, final int size, final String driver_id) {

        String url = googlePlaceDistance + "&origins=" + pickup_lat + "," + pickup_long + "&destinations=" + driver_lat + "," + driver_long + "&language=en-EN";
        url = url.replace(" ", "%20");
        Logger.e("url           " + url);

        MainActivity.dialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Logger.e("response     " + response);
                increment++;
                Log.e("increment", increment + "");
                Log.e("size", size + "");
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                try {
                    Distance distance;
                    distance = gson.fromJson(response, Distance.class);

                    if (distance.getStatus().toString().equals("OK")) {
                        String value = distance.getRows().get(0).getElements().get(0).getDistance().getValue().toString();
                        alValue.add(Integer.valueOf(value));
                        alDriverId.add(driver_id);

                        if (increment == size) {
                            Log.e("arraylist", alValue + "");
                            int min = alValue.get(0);
                            for (int i = 0; i < alValue.size(); i++) {
                                number = alValue.get(i);
                                if (number < min) {
                                    min = number;
                                }
                            }
                            Log.e("min", min + "");
                            increment = 0;
                            alValue.clear();
                            EventBus.getDefault().post(new MessageEvent(driver_id, 1));
                            Log.e("if       ", driver_id + ", " + min);
                        } else {
                            Log.e("else", "nahi chalega");
                        }
                    } else {
                        Logger.e("error_message     " + "You have exceeded your daily request quota for this API.");
                    }
                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("exception", "" + error);
            }
        });
        RequestQueue requestQueue = VolleySingleton.getInstance(context).getRequestQueue();
        requestQueue.add(stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
    }

    public void distance(double pickup_lat, double pickup_long, double dropLat, double dropLong) {

        String url = googlePlaceDistance + "&origins=" + pickup_lat + "," + pickup_long + "&destinations=" + dropLat + "," + dropLong + "&language=en-EN";
        url = url.replace(" ", "%20");
        Logger.e("url distance          " + url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Logger.e("response     " + response);
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                try {
                    Distance distance;
                    distance = gson.fromJson(response, Distance.class);

                    if (distance.getStatus().toString().equals("OK")) {
                        value = distance.getRows().get(0).getElements().get(0).getDistance().getValue().toString();
                        time = distance.getRows().get(0).getElements().get(0).getDuration().getText();
                        timeVal = distance.getRows().get(0).getElements().get(0).getDuration().getValue().toString();
                        EventBus.getDefault().post(new MessageEvent(value, 2));
                    } else {
                        Logger.e("error_message     " + "You have exceeded your daily request quota for this API.");
                    }
                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("exception", "" + error);
            }
        });
        RequestQueue requestQueue = VolleySingleton.getInstance(context).getRequestQueue();
        requestQueue.add(stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
    }
}
