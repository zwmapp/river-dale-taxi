package com.dgharami.RiverDaleTaxi.parsing;

import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.dgharami.RiverDaleTaxi.logger.Logger;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.urls.Apis;

import java.io.File;

/**
 * Created by Bhuvneshwar on 10/27/2016.
 */
public class AccountModule implements Apis {
    ApiFetcher apiFetcher;

    public AccountModule(ApiFetcher apiFetcher) {
        this.apiFetcher = apiFetcher;
    }

    public void registrationApi(String name, String email, String phone, String password, String language_id) {
        String url = register + "?user_name=" + name + "&user_email=" + email +
                "&user_phone=" + phone +
                "&user_password=" + password +
                "&language_id=" + language_id;
        Log.e("url",url);

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(register)
                .addBodyParameter("user_name", name)
                .addBodyParameter("user_email", email)
                .addBodyParameter("user_phone", phone)
                .addBodyParameter("user_password", password)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Register");
                    }

                    @Override
                    public void onError(ANError anError) {

                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void loginApi(String phone_number, String password, String language_id) {

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(login)
                .addBodyParameter("user_email_phone", phone_number)
                .addBodyParameter("user_password", password)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Login");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void checkPhoneApi(String user_phone, String language_id) {

        String url = checkPhone + "?user_phone=" + user_phone;
        Logger.e("url       " + url);

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(checkPhone)
                .addBodyParameter("user_phone", user_phone)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Check Phone");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void editProfile(String user_id, String user_name, String user_mobile, String user_image, String language_id) {

        if (user_image.equals("")) {
            apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
            AndroidNetworking.post(editProfile)
                    .addBodyParameter("user_id", user_id)
                    .addBodyParameter("user_name", user_name)
                    .addBodyParameter("user_phone", user_mobile)
                    .addBodyParameter("language_id", language_id)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String response) {
                            Logger.e("response          " + response);
                            apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                            apiFetcher.onFetchComplete("" + response, "Edit Profile");
                        }

                        @Override
                        public void onError(ANError anError) {

                            apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        }
                    });
        } else {
            apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
            AndroidNetworking.upload(editProfile)
                    .addMultipartParameter("user_id", user_id)
                    .addMultipartParameter("user_name", user_name)
                    .addMultipartParameter("user_phone", user_mobile)
                    .addMultipartFile("user_image", new File(user_image))
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String response) {
                            Logger.e("response          " + response);
                            apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                            apiFetcher.onFetchComplete("" + response, "Edit Profile");
                        }

                        @Override
                        public void onError(ANError anError) {

                            apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        }
                    });
        }
    }

    public void cpApi(String user_id, String o_p, String n_p, String language_id) {

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(changePassword)
                .addBodyParameter("user_id", user_id)
                .addBodyParameter("old_password", o_p)
                .addBodyParameter("new_password", n_p)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Change Password");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void fpApi(String email, String language_id) {

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(forgotPassword)
                .addBodyParameter("user_email", email)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Forgot Password");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void deviceIdApi(String user_id, String deviceToken, String language_id) {

        AndroidNetworking.post(deviceId)
                .addBodyParameter("user_id", user_id)
                .addBodyParameter("device_id", deviceToken)
                .addBodyParameter("flag", "2")
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onFetchComplete("" + response, "Device Id");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void logoutApi(String user_id, String language_id) {

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(logout)
                .addBodyParameter("user_id", user_id)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Logout");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void otpSentApi(String phone, String otp, String language_id) {

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(sentOtp)
                .addBodyParameter("user_phone", phone)
                .addBodyParameter("flag", otp)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Otp Sent");
                    }

                    @Override
                    public void onError(ANError anError) {

                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void createPasswordApi(String phone, String password, String language_id) {

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(createPassword)
                .addBodyParameter("user_phone", phone)
                .addBodyParameter("user_password", password)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Create Password");
                    }

                    @Override
                    public void onError(ANError anError) {

                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void sendSocialTokenApi(String social_token, String language_id) {

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(sendSocialToken)
                .addBodyParameter("social_token", social_token)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Send Social Token");
                    }

                    @Override
                    public void onError(ANError anError) {

                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchFailed(anError);
                        Logger.e("err a gyi     " + anError.toString());
                    }
                });
    }

    public void registrationSocialTokenApi(String social_id, String name, String email, String phone, String social_type, String language_id) {

        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_RUNNING);
        AndroidNetworking.post(registerSocial)
                .addBodyParameter("social_token", social_id)
                .addBodyParameter("user_name", name)
                .addBodyParameter("user_email", email)
                .addBodyParameter("user_phone", phone)
                .addBodyParameter("social_type", social_type)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchComplete("" + response, "Register");
                    }

                    @Override
                    public void onError(ANError anError) {

                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void registerTest() {

        AndroidNetworking.get("http://apporioinfolabs.com/bhuvi/api/Signup/register?first_name=abc&last_name=def&email=dedfcfvf@gmail.com&phone=985rft55546&password=123456&address=1234&city=Maur&state=Punjab&pincode=756767&aadhar_card=12345")
//                .addBodyParameter("first_name", "abcdes")
//                .addBodyParameter("last_name", "dcfvgb")
//                .addBodyParameter("email", "garg@gail")
//                .addBodyParameter("phone", "dgudjds")
//                .addBodyParameter("password", "password")
//                .addBodyParameter("address", "garg@gail")
//                .addBodyParameter("city", "maur")
//                .addBodyParameter("state", "punjab")
//                .addBodyParameter("pincode", "115908")
//                .addBodyParameter("aadhar_card", "1234")
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Logger.e("response" + response);
//                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
//                        apiFetcher.onFetchComplete("" + response, "Register");
                    }

                    @Override
                    public void onError(ANError anError) {

//                        apiFetcher.onAPIRunningState(ApiFetcher.KEY_API_IS_STOPPED);
                        apiFetcher.onFetchFailed(anError);

                        Logger.e("err" + anError.getErrorBody());
                        Logger.e("err1" + anError.getErrorCode());
                        Logger.e("err" + anError.getErrorDetail());
                        Logger.e("err" + anError.getMessage());
                        Logger.e("err" + anError.getData());
                    }
                });
    }
}
