package com.dgharami.RiverDaleTaxi.parsing;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.urls.Apis;

/**
 * Created by Bhuvneshwar on 11/2/2016.
 */
public class HelpModule implements Apis {
    ApiFetcher apiFetcher;

    public HelpModule(ApiFetcher apiFetcher) {
        this.apiFetcher = apiFetcher;
    }

    public void callSupportApi(String language_id) {

        AndroidNetworking.post(callSupport)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
//                        Logger.e("response" + response);
                        apiFetcher.onFetchComplete("" + response, "Call Support");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void aboutUsApi(String language_id) {

        AndroidNetworking.post(aboutUs)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
//                        Logger.e("response" + response);
                        apiFetcher.onFetchComplete("" + response, "About Us");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }

    public void tcApi(String language_id) {

        AndroidNetworking.post(tC)
                .addBodyParameter("language_id", language_id)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
//                        Logger.e("response" + response);
                        apiFetcher.onFetchComplete("" + response, "T and C");
                    }

                    @Override
                    public void onError(ANError anError) {
                        apiFetcher.onFetchFailed(anError);
                    }
                });
    }
}
