package com.dgharami.RiverDaleTaxi.urls;

public interface Apis {

    String baseDomain = "http://riverdaletaxitracker.com/zoom-admin/api/";
    String imageDomain = "http://riverdaletaxitracker.com/zoom-admin/";

    String register = baseDomain + "register_user.php";
    String login = baseDomain + "login_user.php";
    String changePassword = baseDomain + "change_password_user.php";
    String forgotPassword = baseDomain + "forgot_password_user.php";
    String deviceId = baseDomain + "deviceid_user.php";
    String editProfile = baseDomain + "edit_profile_user.php";
    String logout = baseDomain + "logout_user.php";

    String checkPhone = baseDomain + "phone.php";
    String sentOtp = baseDomain + "otp_sent.php";
    String createPassword = baseDomain + "create_password.php";
    String sendSocialToken = baseDomain + "send_social_token.php";
    String registerSocial = baseDomain + "register_user_google.php";

    String callSupport = baseDomain + "call_support.php";
    String aboutUs = baseDomain + "about.php";
    String tC = baseDomain + "tc.php";
    String viewCities = baseDomain + "city.php";
    String viewCars = baseDomain + "car_type.php";
    String viewCarByCities = baseDomain + "car_by_city.php";
    String viewDrivers = baseDomain + "view_driver.php";
    String viewRateCard = baseDomain + "rate_card.php";
    String viewRateCardCity = baseDomain + "rate_card_city.php";
    String viewRideInfo = baseDomain + "view_ride_info_user.php";
    String viewDoneRide = baseDomain + "view_done_ride_info.php";
    String viewRides = baseDomain + "view_rides_user.php";

    String applyCoupon = baseDomain + "coupon.php";
    String rideNow = baseDomain + "ride_now.php";
    String rideLater = baseDomain + "ride_later.php";
    String rideEstimate = baseDomain + "ride_estimate.php";
    String cancelRide = baseDomain + "ride_cancel.php";

    String trackDriver = baseDomain + "view_driver_location.php";
    String rating = baseDomain + "rating_user.php";
    String payment = baseDomain + "payment_saved.php";
    String resendInvoice = baseDomain + "resend_invoice.php";

    String forceUpdate = baseDomain + "force_update.php";
    String update = baseDomain + "update.php";

    String cancelReason = baseDomain + "cancel_reason_customer.php";
    String saveCard = baseDomain + "save_card.php";
    String payWthCard = baseDomain + "pay_with_card.php";
    String deleteCard = baseDomain + "delete_card.php";
    String viewCard = baseDomain + "view_card.php";
    String viewPaymentOption = baseDomain + "view_payment_option.php";
    String viewInvoice = baseDomain + "view_invoice.php";

    String API_KEY = "AIzaSyB0pPtV1-9yVE2dnvE6U_YGgh0EESeGvpQ";
    String googlePlace = "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=" + API_KEY;
    String googlePlaceLatLong = "https://maps.googleapis.com/maps/api/place/details/json?key=" + API_KEY;
    String googlePlaceDistance = "https://maps.googleapis.com/maps/api/distancematrix/json?key=" + API_KEY;
}