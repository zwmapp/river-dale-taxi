package com.dgharami.RiverDaleTaxi;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.dgharami.RiverDaleTaxi.adapter.CarAdapter;
import com.dgharami.RiverDaleTaxi.adapter.CityAdapter;
import com.dgharami.RiverDaleTaxi.logger.Logger;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.models.ResultCheck;
import com.dgharami.RiverDaleTaxi.models.rate_card.RateCard;
import com.dgharami.RiverDaleTaxi.models.viewcartype.ViewCarType;
import com.dgharami.RiverDaleTaxi.models.viewcity.ViewCity;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.parsing.ViewModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class RateCardActivity extends AppCompatActivity implements ApiFetcher {

    LinearLayout back, ll_city, ll_car;
    public static Activity ratecard;
    String city_id, car_id, car_type_name,city_name;
    TextView tv_city, tv_car_type, tv_base_miles, tv_base_price_miles, tv_after_miles,
            tv_price_per_mile, tv_base_ride_time, tv_base_price_minute,
            tv_after_minutes, tv_price_per_minute, tv_base_waiting_time, tv_base_price_waiting, tv_after_waiting_time, tv_price_per_minute_waiting;

    ProgressDialog pd;
    ViewModule viewModule;

    String cityCheck = "", carTypeCheck = "";

    String cityLocation = "", car_type_id="",car_name="";

    LanguageManager languageManager;

    ViewCity viewCity;
    ViewCarType viewCarType;

    String language_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_card);
        getSupportActionBar().hide();
        ratecard = this;

        pd = new ProgressDialog(this,R.style.MyAlertDialogStyle);
        pd.setMessage("Loading...");

        back = (LinearLayout) findViewById(R.id.bck);
        ll_car = (LinearLayout) findViewById(R.id.ll_car);
        ll_city = (LinearLayout) findViewById(R.id.ll_city);
        tv_city = (TextView) findViewById(R.id.tv_city);
        tv_car_type = (TextView) findViewById(R.id.tv_car_type);

        tv_base_miles = (TextView) findViewById(R.id.tv_base_miles);
        tv_after_miles = (TextView) findViewById(R.id.tv_after_miles);
        tv_base_price_miles = (TextView) findViewById(R.id.tv_base_price_miles);
        tv_price_per_mile = (TextView) findViewById(R.id.tv_price_per_mile);

        tv_base_ride_time = (TextView) findViewById(R.id.tv_base_ride_time);
        tv_base_price_minute = (TextView) findViewById(R.id.tv_base_price_minute);
        tv_after_minutes = (TextView) findViewById(R.id.tv_after_minutes);
        tv_price_per_minute = (TextView) findViewById(R.id.tv_price_per_minute);

        tv_base_waiting_time = (TextView) findViewById(R.id.tv_base_waiting_time);
        tv_base_price_waiting = (TextView) findViewById(R.id.tv_base_price_waiting);
        tv_after_waiting_time = (TextView) findViewById(R.id.tv_after_waiting_time);
        tv_price_per_minute_waiting = (TextView) findViewById(R.id.tv_price_per_minute_waiting);

        viewModule = new ViewModule(this);

        cityLocation = super.getIntent().getExtras().getString("cityLocation");
        car_type_id = super.getIntent().getExtras().getString("car_type_id");
        car_type_name = super.getIntent().getExtras().getString("car_type_name");

        languageManager=new LanguageManager(this);
        language_id=languageManager.getLanguageDetail().get(LanguageManager.LANGUAGE_ID);

        Logger.e("cityLocation  " + cityLocation);

        tv_car_type.setText(car_type_name);
        tv_city.setText(cityLocation);

        viewModule.viewRateCardApi(cityLocation, car_type_id,language_id);

        viewModule.viewCitiesApi(language_id);

        ll_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cityCheck.equals("1")) {
                    final Dialog dialog = new Dialog(RateCardActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    Window window = dialog.getWindow();
                    window.setGravity(Gravity.CENTER);
                    dialog.setContentView(R.layout.dialog_for_city);

                    ListView lv_cities = (ListView) dialog.findViewById(R.id.lv_cities);
                    lv_cities.setAdapter(new CityAdapter(RateCardActivity.this, viewCity));

                    lv_cities.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            city_id = viewCity.getMsg().get(position).getCityId();
                            if (language_id.equals("1")) {
                                city_name = viewCity.getMsg().get(position).getCityName();
                            } else if (language_id.equals("2")) {
                                city_name = viewCity.getMsg().get(position).getCityNameFrench();
                            } else if (language_id.equals("3")) {
                                city_name = viewCity.getMsg().get(position).getCityNameArabic();
                            }
                            tv_city.setText(city_name);

                            viewModule.viewCarsByCityApi(city_name,language_id);
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                } else {
                    Toast.makeText(RateCardActivity.this, "No City Found", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ll_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (carTypeCheck.equals("1")) {
                    final Dialog dialog = new Dialog(RateCardActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    Window window = dialog.getWindow();
                    window.setGravity(Gravity.CENTER);
                    dialog.setContentView(R.layout.dialog_for_car);

                    ListView lv_cars = (ListView) dialog.findViewById(R.id.lv_cars);
                    lv_cars.setAdapter(new CarAdapter(RateCardActivity.this, viewCarType));

                    lv_cars.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            car_type_id = viewCarType.getMsg().get(position).getCarTypeId();
                            if (language_id.equals("1")) {
                                car_name = viewCarType.getMsg().get(position).getCarTypeName();
                            } else if (language_id.equals("2")) {
                                car_name = viewCarType.getMsg().get(position).getCarTypeNameFrench();
                            } else if (language_id.equals("3")) {
                                car_name = viewCarType.getMsg().get(position).getCarNameArabic();
                            }
                            tv_car_type.setText(car_name);
                            dialog.dismiss();
                            viewModule.viewRateCardApi(city_id, car_type_id,language_id);
                        }
                    });
                    dialog.show();
                } else {
                    Toast.makeText(RateCardActivity.this, "No Car type Found", Toast.LENGTH_SHORT).show();
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onAPIRunningState(int a) {

        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            pd.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            pd.dismiss();
        }

    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("View Cities")) {
            ResultCheck resultCheck;
            resultCheck = gson.fromJson(response, ResultCheck.class);
            if (resultCheck.result.equals("1")) {
                cityCheck = "1";
                viewCity = gson.fromJson(response, ViewCity.class);
            } else {
                cityCheck = "2";
            }
        }

        if (apiName.equals("View Cars")) {

            ResultCheck resultCheck;
            resultCheck = gson.fromJson(response, ResultCheck.class);
            if (resultCheck.result.equals("1")) {
                carTypeCheck = "1";
                viewCarType = gson.fromJson(response, ViewCarType.class);
            } else {
                carTypeCheck = "2";
            }
        }

        if (apiName.equals("View Rate Card")) {
            RateCard rateCard;
            rateCard = gson.fromJson(response, RateCard.class);

            if (rateCard.getResult().toString().equals("1")) {
                String base_miles = rateCard.getDetails().getBaseMiles();
                String base_price_miles = rateCard.getDetails().getBasePriceMiles();
                String price_per_miles = rateCard.getDetails().getPricePerMile();

                String base_minute = rateCard.getDetails().getBaseMinutes();
                String base_price_minute = rateCard.getDetails().getBasePriceMinute();
                String price_per_minute = rateCard.getDetails().getPricePerMinute();

                String base_minute_waiting = rateCard.getDetails().getBaseWaitingMinutes();
                String base_price_minute_waiting = rateCard.getDetails().getBasePriceMinuteWaiting();
                String price_per_minute_waiting = rateCard.getDetails().getPricePerMinuteWaiting();

                tv_base_miles.setText("First " + base_miles + " miles (Base fare)");
                tv_after_miles.setText("After " + base_miles + " miles");
                tv_base_price_miles.setText("$" + base_price_miles);
                tv_price_per_mile.setText("$" + price_per_miles + " per mile");

                tv_base_ride_time.setText("First " + base_minute + " min.");
                tv_after_minutes.setText("After " + base_minute + " min.");
                tv_base_price_minute.setText("$" + base_price_minute);
                tv_price_per_minute.setText("$" + price_per_minute + " per min.");

                tv_base_waiting_time.setText("First " + base_minute_waiting + " min.");
                tv_after_waiting_time.setText("After " + base_minute_waiting + " min.");
                tv_base_price_waiting.setText("$" + base_price_minute_waiting);
                tv_price_per_minute_waiting.setText("$" + price_per_minute_waiting + " per min.");
            } else {
//                tv_first_two.setText("");
//                tv_after_two.setText("");
                Toast.makeText(this, "" + rateCard.getMsg(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }
}
