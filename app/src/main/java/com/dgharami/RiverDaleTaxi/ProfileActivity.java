package com.dgharami.RiverDaleTaxi;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.dgharami.RiverDaleTaxi.logger.Logger;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.manager.SessionManager;
import com.dgharami.RiverDaleTaxi.models.deviceid.DeviceId;
import com.dgharami.RiverDaleTaxi.models.register.Register;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.others.ImageCompressMode;
import com.dgharami.RiverDaleTaxi.parsing.AccountModule;
import com.dgharami.RiverDaleTaxi.urls.Apis;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

public class ProfileActivity extends AppCompatActivity implements ApiFetcher, Apis {

    LinearLayout back, ll_change_password, ll_logout, ll_done_profile;

    ImageView iv_edit_name, iv_edit_phone;
    TextView tv_email;
    EditText edt_name, edt_mobile;
    String user_id, user_name, user_mobile, user_image, user_email;

    Dialog dialog;

    public static Activity editProfileactivity;

    LanguageManager languageManager;

    ImageView iv_profile_pic_upload;
    Uri selectedImage;
    Bitmap bitmap1;
    String imagePath = "", imagePathCompressed = "";

    SessionManager sessionManager;
    ProgressDialog pd;

    AccountModule accountModule;

    String language_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_profile);
        getSupportActionBar().hide();
        editProfileactivity = this;

        pd = new ProgressDialog(this,R.style.MyAlertDialogStyle);
        pd.setMessage("Loading...");

        accountModule = new AccountModule(this);

        back = (LinearLayout) findViewById(R.id.bck);
        ll_change_password = (LinearLayout) findViewById(R.id.ll_change_password);
        ll_logout = (LinearLayout) findViewById(R.id.ll_logout);
        ll_done_profile = (LinearLayout) findViewById(R.id.ll_done_profile);
        iv_profile_pic_upload = (ImageView) findViewById(R.id.iv_profile_pic_upload);
        tv_email = (TextView) findViewById(R.id.email);
        edt_name = (EditText) findViewById(R.id.name);
        edt_mobile = (EditText) findViewById(R.id.mob);

        edt_name.setEnabled(false);
        edt_mobile.setEnabled(false);

        languageManager=new LanguageManager(this);
        language_id=languageManager.getLanguageDetail().get(LanguageManager.LANGUAGE_ID);

        iv_edit_name = (ImageView) findViewById(R.id.iv_edit_name);
        iv_edit_phone = (ImageView) findViewById(R.id.iv_edit_phone);

        edt_name.setTypeface(Typeface.createFromAsset(getAssets(), "OpenSans_Regular.ttf"));
        edt_mobile.setTypeface(Typeface.createFromAsset(getAssets(), "OpenSans_Regular.ttf"));

        sessionManager = new SessionManager(this);

        user_id = sessionManager.getUserDetails().get(SessionManager.USER_ID);
        user_name = sessionManager.getUserDetails().get(SessionManager.USER_NAME);
        user_email = sessionManager.getUserDetails().get(SessionManager.USER_EMAIL);
        user_mobile = sessionManager.getUserDetails().get(SessionManager.USER_PHONE);
        user_image = sessionManager.getUserDetails().get(SessionManager.USER_IMAGE);

        tv_email.setText(user_email);
        edt_name.setText(user_name);
        edt_mobile.setText(user_mobile);
        Picasso.with(ProfileActivity.this)
                .load(imageDomain + user_image)
                .placeholder(R.drawable.dummy_pic)
                .error(R.drawable.dummy_pic)
                .fit()
                .into(iv_profile_pic_upload);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//        edt_name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(ProfileActivity.this, "click on name", Toast.LENGTH_SHORT).show();
//                edt_name.setEnabled(true);
//                edt_name.requestFocus();
//
//                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                inputMethodManager.showSoftInput(edt_name, InputMethodManager.SHOW_FORCED);
//            }
//        });
//
//        edt_mobile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(ProfileActivity.this, "click on phone", Toast.LENGTH_SHORT).show();
//                edt_mobile.setEnabled(true);
//                edt_mobile.requestFocus();
//
//                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                inputMethodManager.showSoftInput(edt_mobile, InputMethodManager.SHOW_FORCED);
//            }
//        });

        iv_profile_pic_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i1 = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                i1.setType("image/*");
                startActivityForResult(i1, 101);
            }
        });

        ll_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(ProfileActivity.this, ChangePasswordActivity.class));
            }
        });

        ll_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showlogoutdialog();
            }
        });

        ll_done_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = edt_name.getText().toString().trim();
                String phone = edt_mobile.getText().toString().trim();

                if (name.equals("")) {
                    Toast.makeText(ProfileActivity.this, "Name can't be empty", Toast.LENGTH_SHORT).show();
                } else if (phone.equals("")) {
                    Toast.makeText(ProfileActivity.this, "Phone can't be empty", Toast.LENGTH_SHORT).show();
                } else if (!(phone.length() <= 10)) {
                    Toast.makeText(getApplicationContext(), "Phone should be of 10 digits", Toast.LENGTH_SHORT).show();
                } else {
                    accountModule.editProfile(user_id, name, phone, imagePathCompressed,language_id);
                }
            }
        });

        iv_edit_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                edt_name.setClickable(true);
//                edt_name.setFocusable(true);
//                edt_name.setCursorVisible(true);
//                edt_name.setFocusableInTouchMode(true);
                edt_name.setEnabled(true);
                edt_name.setSelection(edt_name.getText().length());
                edt_name.requestFocus();

                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.showSoftInput(edt_name, InputMethodManager.SHOW_FORCED);

//                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.showSoftInput(yourEditText, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        iv_edit_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                edt_mobile.setClickable(true);
//                edt_mobile.setFocusable(true);
//                edt_mobile.setCursorVisible(true);
//                edt_mobile.setFocusableInTouchMode(true);
                edt_mobile.setEnabled(true);
                edt_mobile.setSelection(edt_mobile.getText().length());
                edt_mobile.requestFocus();

                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.showSoftInput(edt_mobile, InputMethodManager.SHOW_FORCED);
            }
        });
    }

    public void onActivityResult(int req, int res, Intent data) {
        super.onActivityResult(req, res, data);
        if (res == Activity.RESULT_OK) {
            try {
                if (req == 101) {
                    selectedImage = data.getData();
                    imagePath = getPath(selectedImage);

                    ImageCompressMode imageCompressMode = new ImageCompressMode(this);
                    imagePathCompressed = imageCompressMode.compressImage(imagePath);

                    // imagePathCompressed = compressImage(imagePath);
                    Logger.e("image Compreed        " + imagePathCompressed);

                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String filePath = cursor.getString(columnIndex);
                    cursor.close();

                    // Set the Image in ImageView after decoding the String
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(filePath, options);
                    final int REQUIRED_SIZE = 300;
                    int scale = 1;
                    while (options.outWidth / scale / 2 >= REQUIRED_SIZE && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                        scale *= 2;
                    options.inSampleSize = scale;
                    options.inJustDecodeBounds = false;
                    bitmap1 = BitmapFactory.decodeFile(filePath, options);
                    iv_profile_pic_upload.setImageBitmap(bitmap1);
                }
            } catch (Exception e) {
                Logger.e("res         " + e.toString());
            }
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void showlogoutdialog() {

        dialog = new Dialog(ProfileActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        dialog.setCancelable(false);
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_for_logout);

        LinearLayout yes = (LinearLayout) dialog.findViewById(R.id.yes);
        LinearLayout no = (LinearLayout) dialog.findViewById(R.id.no);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accountModule.logoutApi(user_id,language_id);
            }
        });

        no.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onAPIRunningState(int a) {

        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            pd.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            pd.dismiss();
        }

    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("Edit Profile")) {
            Register register = new Register();
            register = gson.fromJson(response, Register.class);

            if (register.getResult().toString().equals("1")) {

                String user_id = register.getDetails().getUserId();
                String user_name = register.getDetails().getUserName();
                String user_email = register.getDetails().getUserEmail();
                String user_mobile = register.getDetails().getUserPhone();
                String user_image = register.getDetails().getUserImage();

                Toast.makeText(this, "Profile Updated" , Toast.LENGTH_LONG).show();
                new SessionManager(this).createLoginSession(user_id, user_name, user_email, user_mobile, user_image);
                finish();
            } else {
                Toast.makeText(this, "Error occurred", Toast.LENGTH_LONG).show();
            }
        }

        if (apiName.equals("Logout")) {
            DeviceId deviceId = new DeviceId();
            deviceId = gson.fromJson(response, DeviceId.class);

            if (deviceId.getResult().toString().equals("1")) {
                startActivity(new Intent(ProfileActivity.this, SplashActivity.class));
                Toast.makeText(this, "" + deviceId.getMsg(), Toast.LENGTH_LONG).show();
                finish();
                MainActivity.mainActivity.finish();
                sessionManager.logoutUser();
                dialog.dismiss();
                Profile profile = Profile.getCurrentProfile().getCurrentProfile();
                if (profile != null) {
                    LoginManager.getInstance().logOut();
                }
            } else {
                Toast.makeText(this, "" + deviceId.getMsg(), Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }

//    public String compressImage(String imageUri) {
//
//        String filePath = getRealPathFromURI(imageUri);
//        Bitmap scaledBitmap = null;
//
//        BitmapFactory.Options options = new BitmapFactory.Options();
//
////      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
////      you try the use the bitmap here, you will get null.
//        options.inJustDecodeBounds = true;
//        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);
//
//        int actualHeight = options.outHeight;
//        int actualWidth = options.outWidth;
//
////      max Height and width values of the compressed image is taken as 816x612
//
//        float maxHeight = 816.0f;
//        float maxWidth = 612.0f;
//        float imgRatio = actualWidth / actualHeight;
//        float maxRatio = maxWidth / maxHeight;
//
////      width and height values are set maintaining the aspect ratio of the image
//
//        if (actualHeight > maxHeight || actualWidth > maxWidth) {
//            if (imgRatio < maxRatio) {
//                imgRatio = maxHeight / actualHeight;
//                actualWidth = (int) (imgRatio * actualWidth);
//                actualHeight = (int) maxHeight;
//            } else if (imgRatio > maxRatio) {
//                imgRatio = maxWidth / actualWidth;
//                actualHeight = (int) (imgRatio * actualHeight);
//                actualWidth = (int) maxWidth;
//            } else {
//                actualHeight = (int) maxHeight;
//                actualWidth = (int) maxWidth;
//
//            }
//        }
//
////      setting inSampleSize value allows to load a scaled down version of the original image
//
//        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
//
////      inJustDecodeBounds set to false to load the actual bitmap
//        options.inJustDecodeBounds = false;
//
////      this options allow android to claim the bitmap memory if it runs low on memory
//        options.inPurgeable = true;
//        options.inInputShareable = true;
//        options.inTempStorage = new byte[16 * 1024];
//
//        try {
////          load the bitmap from its path
//            bmp = BitmapFactory.decodeFile(filePath, options);
//        } catch (OutOfMemoryError exception) {
//            exception.printStackTrace();
//
//        }
//        try {
//            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
//        } catch (OutOfMemoryError exception) {
//            exception.printStackTrace();
//        }
//
//        float ratioX = actualWidth / (float) options.outWidth;
//        float ratioY = actualHeight / (float) options.outHeight;
//        float middleX = actualWidth / 2.0f;
//        float middleY = actualHeight / 2.0f;
//
//        Matrix scaleMatrix = new Matrix();
//        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
//
//        Canvas canvas = new Canvas(scaledBitmap);
//        canvas.setMatrix(scaleMatrix);
//        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
//
////      check the rotation of the image and display it properly
//        ExifInterface exif;
//        try {
//            exif = new ExifInterface(filePath);
//
//            int orientation = exif.getAttributeInt(
//                    ExifInterface.TAG_ORIENTATION, 0);
//            Log.d("EXIF", "Exif: " + orientation);
//            Matrix matrix = new Matrix();
//            if (orientation == 6) {
//                matrix.postRotate(90);
//                Log.d("EXIF", "Exif: " + orientation);
//            } else if (orientation == 3) {
//                matrix.postRotate(180);
//                Log.d("EXIF", "Exif: " + orientation);
//            } else if (orientation == 8) {
//                matrix.postRotate(270);
//                Log.d("EXIF", "Exif: " + orientation);
//            }
//            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
//                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
//                    true);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        FileOutputStream out = null;
//        String filename = getFilename();
//        try {
//            out = new FileOutputStream(filename);
//
////          write the compressed bitmap at the destination specified by filename.
//            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        return filename;
//
//    }
//
//    public String getFilename() {
//        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
//        if (!file.exists()) {
//            file.mkdirs();
//        }
//        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
//        return uriSting;
//
//    }
//
//    private String getRealPathFromURI(String contentURI) {
//        Uri contentUri = Uri.parse(contentURI);
//        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
//        if (cursor == null) {
//            return contentUri.getPath();
//        } else {
//            cursor.moveToFirst();
//            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
//            return cursor.getString(index);
//        }
//    }
//
//    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
//        final int height = options.outHeight;
//        final int width = options.outWidth;
//        int inSampleSize = 1;
//
//        if (height > reqHeight || width > reqWidth) {
//            final int heightRatio = Math.round((float) height / (float) reqHeight);
//            final int widthRatio = Math.round((float) width / (float) reqWidth);
//            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
//        }
//        final float totalPixels = width * height;
//        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
//        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
//            inSampleSize++;
//        }
//
//        return inSampleSize;
//    }
}
