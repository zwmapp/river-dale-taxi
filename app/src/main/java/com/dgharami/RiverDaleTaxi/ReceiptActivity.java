package com.dgharami.RiverDaleTaxi;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.manager.SessionManager;
import com.dgharami.RiverDaleTaxi.models.donerideinfo.DoneRideInfo;
import com.dgharami.RiverDaleTaxi.models.paymentsaved.PaymentSaved;
import com.dgharami.RiverDaleTaxi.models.paywithcard.PayWithCard;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.parsing.CustomerModule;
import com.dgharami.RiverDaleTaxi.parsing.RideModule;
import com.dgharami.RiverDaleTaxi.urls.Apis;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
//import com.paypal.android.sdk.payments.PayPalConfiguration;
//import com.paypal.android.sdk.payments.PayPalPayment;
//import com.paypal.android.sdk.payments.PayPalService;
//import com.paypal.android.sdk.payments.PaymentActivity;
//import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;

public class ReceiptActivity extends AppCompatActivity implements ApiFetcher, Apis {

    String user_id;
    TextView tv_ride_fare, tv_driver_name, tv_ride_distance, tv_ride_time;
    ImageView iv_driver_pic;
    LinearLayout ll_make_payment;
    public static Activity receipt_activity;

    //private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
    //private static final String CONFIG_CLIENT_ID = "AFcWxV21C7fd0v3bYYYRCpSSRl31AW.nrY8UUmkTDBx-TSEQlHYBvptc";
    //private static PayPalConfiguration config = new PayPalConfiguration().environment(CONFIG_ENVIRONMENT).clientId(CONFIG_CLIENT_ID);

    private static final int REQUEST_CODE_PAYMENT = 1;

    public static String rideId, driverId, driverName, driverImage, amount, distance, rideTime;
    TextView tv_change_text;
    String dateString;
    CustomerModule customerModule;
    RideModule rideModule;
    String orderid, userid, paymentmethod, paymentpaltform, paymentid, paymentamount, paymentdate, paymentstatus, payment_option_id, ride_id;

    ProgressDialog pd;

    LanguageManager languageManager;

    String language_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt);
        getSupportActionBar().hide();
        receipt_activity = this;

        tv_change_text = (TextView) findViewById(R.id.tv_change_text);
        tv_ride_fare = (TextView) findViewById(R.id.tv_ride_fare);
        tv_driver_name = (TextView) findViewById(R.id.tv_driver_name);
        tv_ride_distance = (TextView) findViewById(R.id.tv_ride_distance);
        tv_ride_time = (TextView) findViewById(R.id.tv_ride_time);
        iv_driver_pic = (ImageView) findViewById(R.id.iv_driver_pic);

        pd = new ProgressDialog(this,R.style.MyAlertDialogStyle);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);

        rideId = super.getIntent().getExtras().getString("ride_id");
        driverId = super.getIntent().getExtras().getString("driverId");
        user_id = new SessionManager(ReceiptActivity.this).getUserDetails().get(SessionManager.USER_ID);

        languageManager = new LanguageManager(this);
        language_id = languageManager.getLanguageDetail().get(LanguageManager.LANGUAGE_ID);

        customerModule = new CustomerModule(this);
        rideModule = new RideModule(this);
        rideModule.viewDoneRideApi(rideId, language_id);

        ll_make_payment = (LinearLayout) findViewById(R.id.ll_make_payment);
        ll_make_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                dialogForPayment();

                if (payment_option_id.equals("1")) {

                    long date = System.currentTimeMillis();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM, yyyy");
                    dateString = sdf.format(date);

                    customerModule.paymentApi(rideId, user_id, "1", "Cash", "Android", amount, dateString, "Done", language_id);
                } else if (payment_option_id.equals("2")) {

//                    Intent intent = new Intent(ReceiptActivity.this, PayPalService.class);
//                    intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
//                    startService(intent);
//
//                    PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(String.valueOf(amount)), "GBP", "Pay", PayPalPayment.PAYMENT_INTENT_SALE);
//                    Intent intent1 = new Intent(ReceiptActivity.this, PaymentActivity.class);
//                    intent1.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
//                    intent1.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
//                    startActivityForResult(intent1, REQUEST_CODE_PAYMENT);
                } else if (payment_option_id.equals("3")) {
                    customerModule.payWithCardApi(ride_id, language_id, amount);
                }
            }
        });
    }

    @Override
    public void onAPIRunningState(int a) {

        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            pd.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            pd.dismiss();
        }
    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("View Done Ride")) {
            DoneRideInfo doneRideInfo = new DoneRideInfo();
            doneRideInfo = gson.fromJson(response, DoneRideInfo.class);
            if (doneRideInfo.getResult().toString().equals("1")) {
                amount = doneRideInfo.getMsg().getAmount();
                distance = doneRideInfo.getMsg().getDistance();
                rideTime = doneRideInfo.getMsg().getTotTime();
                driverId = doneRideInfo.getMsg().getDriverId();
                driverName = doneRideInfo.getMsg().getDriverName();
                driverImage = doneRideInfo.getMsg().getDriverImage();
                payment_option_id = doneRideInfo.getMsg().getPaymentOptionId();
                ride_id = doneRideInfo.getMsg().getRideId();

                tv_driver_name.setText(driverName);
                tv_ride_distance.setText(distance + " Miles");
                tv_ride_fare.setText("$ " + amount);
                tv_ride_time.setText(rideTime);
                Picasso.with(this)
                        .load(imageDomain + driverImage)
                        .placeholder(R.drawable.dummy_pic)
                        .error(R.drawable.dummy_pic)
                        .fit()
                        .into(iv_driver_pic);
            } else {
                Toast.makeText(ReceiptActivity.this, "" + doneRideInfo.getMsg().toString(), Toast.LENGTH_SHORT).show();
            }
        }

        if (apiName.equals("Payment")) {

            PaymentSaved paymentSaved;
            paymentSaved = gson.fromJson(response, PaymentSaved.class);

            if (paymentSaved.getResult().toString().equals("1")) {

                tv_change_text.setText("Payment Done");
                ll_make_payment.setOnClickListener(null);

                orderid = paymentSaved.getDetails().getOrderId();
                userid = paymentSaved.getDetails().getUserId();
                paymentmethod = paymentSaved.getDetails().getPaymentMethod();
                paymentpaltform = paymentSaved.getDetails().getPaymentPlatform();
                paymentid = paymentSaved.getDetails().getPaymentId();
                paymentamount = paymentSaved.getDetails().getPaymentAmount();
                paymentdate = paymentSaved.getDetails().getPaymentDateTime();
                paymentstatus = paymentSaved.getDetails().getPaymentStatus();

                Intent i = new Intent(ReceiptActivity.this, InvoiceActivity.class);
                i.putExtra("order_id", orderid);
                i.putExtra("payment_id", paymentid);
                i.putExtra("payment_amount", paymentamount);
                i.putExtra("payment_date", paymentdate);
                i.putExtra("payment_status", paymentstatus);
                i.putExtra("payment_platform", paymentpaltform);
                i.putExtra("driverId", driverId);
                startActivity(i);
                finish();
            } else {
                Toast.makeText(this, "" + paymentSaved.getMsg(), Toast.LENGTH_SHORT).show();
            }
        }

        if (apiName.equals("Pay With Card")) {

            PayWithCard payWithCard = new PayWithCard();
            payWithCard = gson.fromJson(response, PayWithCard.class);
            if (payWithCard.getResult().toString().equals("1")) {
                String status = payWithCard.getMsg();
                String payment_id = payWithCard.getPaymentId();

                long date = System.currentTimeMillis();
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM, yyyy");
                dateString = sdf.format(date);

                customerModule.paymentApi(rideId, user_id, payment_id, "Credit Card", "Android", amount, dateString, status, language_id);

            } else {
                Toast.makeText(ReceiptActivity.this, "" + payWithCard.getMsg().toString(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }

    public void dialogForPayment() {
        final Dialog dialog = new Dialog(ReceiptActivity.this, android.R.style.Theme_Holo_Light_DarkActionBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        dialog.setCancelable(false);
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_for_payment);

        final RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.rg);
        LinearLayout ll_done_payment = (LinearLayout) dialog.findViewById(R.id.ll_done_payment);

        ll_done_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton r = (RadioButton) dialog.findViewById(radioGroup.getCheckedRadioButtonId());
                String option = r.getText().toString();

                if (option.equalsIgnoreCase("By Cash")) {
                    long date = System.currentTimeMillis();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM, yyyy");
                    dateString = sdf.format(date);
                    tv_change_text.setText("Payment Done");
                    ll_make_payment.setOnClickListener(null);

                    customerModule.paymentApi(rideId, user_id, "1", "Cash", "Android", amount, dateString, "Done", language_id);
                } else if (option.equalsIgnoreCase("By Paypal")) {

//                    Intent intent = new Intent(ReceiptActivity.this, PayPalService.class);
//                    intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
//                    startService(intent);
//
//                    PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(String.valueOf(amount)), "GBP", "Pay", PayPalPayment.PAYMENT_INTENT_SALE);
//                    Intent intent1 = new Intent(ReceiptActivity.this, PaymentActivity.class);
//                    intent1.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
//                    intent1.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
//                    startActivityForResult(intent1, REQUEST_CODE_PAYMENT);
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (resultCode == Activity.RESULT_OK) {
//            PaymentConfirmation paymentConfirmation = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
//            if (paymentConfirmation != null) {
//                try {
//                    String response = paymentConfirmation.toJSONObject().toString(4);
//                    Logger.e("PaypalModel Response       " + response);
//
//                    GsonBuilder builder = new GsonBuilder();
//                    Gson gson = builder.create();
//                    PaypalModel pay_pal = new PaypalModel();
//                    pay_pal = gson.fromJson(response, PaypalModel.class);
//
//                    if (pay_pal != null) {
//                        Logger.e("create time       " + pay_pal.response.create_time);
//                        Logger.e("ids       " + pay_pal.response.ids);
//                        Logger.e("intents       " + pay_pal.response.intents);
//                        Logger.e("state         " + pay_pal.response.state);
//
//                        customerModule = new CustomerModule(ReceiptActivity.this);
//                        customerModule.paymentApi(rideId, user_id, pay_pal.response.ids, "Paypal", "Android", amount, pay_pal.response.create_time, pay_pal.response.state, language_id);
//
////                        tv_change_text.setText("Payment Done");
////                        ll_make_payment.setOnClickListener(null);
//                    } else {
//                        Toast.makeText(ReceiptActivity.this, "No Response", Toast.LENGTH_SHORT).show();
//                    }
//                } catch (JSONException e) {
//                    Logger.e("Exception in PaypalModel Response      " + e.toString());
//                }
//            }
//        } else if (resultCode == Activity.RESULT_CANCELED) {
//            Logger.e("Payment ", "Cancelled");
//            Toast.makeText(ReceiptActivity.this, "Cancelled ", Toast.LENGTH_SHORT).show();
//        } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
//            Toast.makeText(ReceiptActivity.this, "Invalid Payment ", Toast.LENGTH_SHORT).show();
//            Log.e("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
//        }
    }
}
