package com.dgharami.RiverDaleTaxi;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.dgharami.RiverDaleTaxi.adapter.ReasonAdapter;
import com.dgharami.RiverDaleTaxi.logger.Logger;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.models.ResultCheck;
import com.dgharami.RiverDaleTaxi.models.cancelreasoncustomer.CancelReasonCustomer;
import com.dgharami.RiverDaleTaxi.models.deviceid.DeviceId;
import com.dgharami.RiverDaleTaxi.models.viewrideinfo.ViewRideInfo;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.parsing.CustomerModule;
import com.dgharami.RiverDaleTaxi.parsing.RideModule;
import com.dgharami.RiverDaleTaxi.urls.Apis;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import es.dmoral.toasty.Toasty;

public class RidesSelectedActivity extends AppCompatActivity implements ApiFetcher, Apis {

    ImageView iv_map, iv_image_driver, iv_image_car;
    TextView tv_name_driver, tv_name_car, tv_dis, tv_time1, tv_start_location, tv_end_location, tv_tota, tv_date_time1;
    LinearLayout ll_back_ride_select, ll_coupons, ll_cancel_r, ll_mail_invoice, ll_view_invoice,
            ll_track_ride, ll_driver_ki_detail, ll_driver_car_detail, ll_bill, ll_location_module, ll_total_bill;
    RatingBar rating_selected;
    String ride_id, ride_status, ride_type;
    ProgressDialog pd;
    RideModule rideModule;
    CustomerModule customerModule;

    LanguageManager languageManager;

    String language_id;

    String reason_id;

    CancelReasonCustomer cancelReasonCustomer;

    String driverId, driverName, driverPhone, driverImage, rating, carNumber, done_ride_id, rideId, userId, pickupLat, pickupLong, driverLat, driverLong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rides_selected);
        getSupportActionBar().hide();

        pd = new ProgressDialog(this,R.style.MyAlertDialogStyle);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);

        rideModule = new RideModule(this);
        customerModule = new CustomerModule(this);
        languageManager=new LanguageManager(this);
        language_id=languageManager.getLanguageDetail().get(LanguageManager.LANGUAGE_ID);

        iv_map = (ImageView) findViewById(R.id.iv_map);
        iv_image_car = (ImageView) findViewById(R.id.iv_image_car);
        iv_image_driver = (ImageView) findViewById(R.id.iv_image_driver);

        tv_name_driver = (TextView) findViewById(R.id.tv_name_driver);
        tv_name_car = (TextView) findViewById(R.id.tv_name_car);
        tv_dis = (TextView) findViewById(R.id.tv_dis);
        tv_time1 = (TextView) findViewById(R.id.tv_time1);
        tv_start_location = (TextView) findViewById(R.id.tv_start_location);
        tv_end_location = (TextView) findViewById(R.id.tv_end_location);
        tv_tota = (TextView) findViewById(R.id.tv_tota);
        tv_date_time1 = (TextView) findViewById(R.id.tv_date_time1);

        rating_selected = (RatingBar) findViewById(R.id.rating_selected);

        ll_back_ride_select = (LinearLayout) findViewById(R.id.ll_back_ride_select);
        ll_driver_ki_detail = (LinearLayout) findViewById(R.id.ll_driver_ki_detail);
        ll_driver_car_detail = (LinearLayout) findViewById(R.id.ll_driver_car_detail);
        ll_bill = (LinearLayout) findViewById(R.id.ll_bill);
        ll_location_module = (LinearLayout) findViewById(R.id.ll_location_module);
        ll_total_bill = (LinearLayout) findViewById(R.id.ll_total_bill);

        ll_coupons = (LinearLayout) findViewById(R.id.ll_coupons);
        ll_cancel_r = (LinearLayout) findViewById(R.id.ll_cancel_r);
        ll_mail_invoice = (LinearLayout) findViewById(R.id.ll_mail_invoice);
        ll_view_invoice = (LinearLayout) findViewById(R.id.ll_view_invoice);
        ll_track_ride = (LinearLayout) findViewById(R.id.ll_track_ride);

        ride_id = getIntent().getExtras().getString("ride_id");
        ride_status = getIntent().getExtras().getString("ride_status");
        ride_type = getIntent().getExtras().getString("ride_type");
        String date_time = getIntent().getExtras().getString("date_time");
        tv_date_time1.setText(date_time);

        if (ride_status.equals("1")) {
            ll_coupons.setVisibility(View.GONE);

            ll_track_ride.setVisibility(View.GONE);
            ll_cancel_r.setVisibility(View.VISIBLE);
            ll_mail_invoice.setVisibility(View.GONE);

            ll_view_invoice.setVisibility(View.GONE);

            ll_driver_ki_detail.setVisibility(View.GONE);
            ll_driver_car_detail.setVisibility(View.GONE);
            ll_bill.setVisibility(View.GONE);
            ll_location_module.setVisibility(View.VISIBLE);
            ll_total_bill.setVisibility(View.GONE);
        } else if (ride_status.equals("2")) {
            ll_coupons.setVisibility(View.GONE);
            ll_track_ride.setVisibility(View.GONE);
            ll_cancel_r.setVisibility(View.GONE);
            ll_mail_invoice.setVisibility(View.GONE);
            ll_view_invoice.setVisibility(View.GONE);
            if (ride_type.equals("1")) {
                ll_driver_ki_detail.setVisibility(View.VISIBLE);
                ll_driver_car_detail.setVisibility(View.VISIBLE);
                ll_bill.setVisibility(View.GONE);
                ll_location_module.setVisibility(View.VISIBLE);
                ll_total_bill.setVisibility(View.GONE);
            } else if (ride_type.equals("2")) {
                ll_driver_ki_detail.setVisibility(View.GONE);
                ll_driver_car_detail.setVisibility(View.GONE);
                ll_bill.setVisibility(View.GONE);
                ll_location_module.setVisibility(View.VISIBLE);
                ll_total_bill.setVisibility(View.GONE);
            }
        } else if (ride_status.equals("3")) {
            ll_coupons.setVisibility(View.GONE);
            ll_track_ride.setVisibility(View.VISIBLE);
            ll_cancel_r.setVisibility(View.VISIBLE);
            ll_mail_invoice.setVisibility(View.GONE);
            ll_view_invoice.setVisibility(View.GONE);
            ll_driver_ki_detail.setVisibility(View.VISIBLE);
            ll_driver_car_detail.setVisibility(View.VISIBLE);
            ll_bill.setVisibility(View.GONE);
            ll_location_module.setVisibility(View.VISIBLE);
            ll_total_bill.setVisibility(View.GONE);

        } else if (ride_status.equals("4")) {
            ll_coupons.setVisibility(View.GONE);
            ll_track_ride.setVisibility(View.GONE);
            ll_cancel_r.setVisibility(View.GONE);
            ll_mail_invoice.setVisibility(View.GONE);
            ll_view_invoice.setVisibility(View.GONE);
            ll_driver_ki_detail.setVisibility(View.VISIBLE);
            ll_driver_car_detail.setVisibility(View.VISIBLE);
            ll_bill.setVisibility(View.GONE);
            ll_location_module.setVisibility(View.VISIBLE);
            ll_total_bill.setVisibility(View.GONE);
        } else if (ride_status.equals("5")) {
            ll_coupons.setVisibility(View.GONE);
            ll_track_ride.setVisibility(View.VISIBLE);
            ll_cancel_r.setVisibility(View.GONE);
            ll_mail_invoice.setVisibility(View.GONE);
            ll_view_invoice.setVisibility(View.GONE);
            ll_driver_ki_detail.setVisibility(View.VISIBLE);
            ll_driver_car_detail.setVisibility(View.VISIBLE);
            ll_bill.setVisibility(View.GONE);
            ll_location_module.setVisibility(View.VISIBLE);
            ll_total_bill.setVisibility(View.GONE);
        } else if (ride_status.equals("6")) {
            ll_coupons.setVisibility(View.GONE);
            ll_track_ride.setVisibility(View.VISIBLE);
            ll_cancel_r.setVisibility(View.GONE);
            ll_mail_invoice.setVisibility(View.GONE);
            ll_view_invoice.setVisibility(View.GONE);
            ll_driver_ki_detail.setVisibility(View.VISIBLE);
            ll_driver_car_detail.setVisibility(View.VISIBLE);
            ll_bill.setVisibility(View.GONE);
            ll_location_module.setVisibility(View.VISIBLE);
            ll_total_bill.setVisibility(View.GONE);
        } else if (ride_status.equals("7")) {
            ll_coupons.setVisibility(View.GONE);
            ll_track_ride.setVisibility(View.GONE);
            ll_cancel_r.setVisibility(View.GONE);
            ll_mail_invoice.setVisibility(View.VISIBLE);
            ll_view_invoice.setVisibility(View.VISIBLE);
            ll_driver_ki_detail.setVisibility(View.VISIBLE);
            ll_driver_car_detail.setVisibility(View.VISIBLE);
            ll_bill.setVisibility(View.VISIBLE);
            ll_location_module.setVisibility(View.VISIBLE);
            ll_total_bill.setVisibility(View.VISIBLE);
        } else if (ride_status.equals("8")) {
            ll_coupons.setVisibility(View.GONE);

            ll_track_ride.setVisibility(View.GONE);
            ll_cancel_r.setVisibility(View.VISIBLE);
            ll_mail_invoice.setVisibility(View.GONE);
            ll_view_invoice.setVisibility(View.GONE);
            ll_driver_ki_detail.setVisibility(View.GONE);
            ll_driver_car_detail.setVisibility(View.GONE);
            ll_bill.setVisibility(View.GONE);
            ll_location_module.setVisibility(View.VISIBLE);
            ll_total_bill.setVisibility(View.GONE);
        } else if (ride_status.equals("9")) {
            ll_coupons.setVisibility(View.GONE);

            ll_track_ride.setVisibility(View.GONE);
            ll_cancel_r.setVisibility(View.GONE);
            ll_mail_invoice.setVisibility(View.GONE);
            ll_view_invoice.setVisibility(View.GONE);

            ll_driver_ki_detail.setVisibility(View.VISIBLE);
            ll_driver_car_detail.setVisibility(View.VISIBLE);
            ll_bill.setVisibility(View.GONE);
            ll_location_module.setVisibility(View.VISIBLE);
            ll_total_bill.setVisibility(View.GONE);
        }

        rideModule.viewRideInfoApi(ride_id,language_id);

        ll_mail_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerModule.resendInvoiceApi(done_ride_id, language_id);
            }
        });

        ll_cancel_r.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showCancelRideDialog();
//                rideModule.cancelRideApi(ride_id,reason_id);
            }
        });

        ll_back_ride_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ll_track_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RidesSelectedActivity.this, TrackRideActivity.class)
                        .putExtra("ride_id", ride_id)
                        .putExtra("ride_status", ride_status));
                finish();
            }
        });

        ll_view_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RidesSelectedActivity.this, InvoiceActivity.class)
                        .putExtra("order_id", done_ride_id)
                        .putExtra("ride_id", ride_id)
                        .putExtra("is_first_entry", false)
                        .putExtra("ride_status", ride_status));
            }
        });
    }

    public void showCancelRideDialog() {

        final Dialog dialog = new Dialog(RidesSelectedActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        dialog.setCancelable(false);
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_for_logout);

        LinearLayout yes = (LinearLayout) dialog.findViewById(R.id.yes);
        LinearLayout no = (LinearLayout) dialog.findViewById(R.id.no);
        TextView tv_cancel_ride = (TextView) dialog.findViewById(R.id.tv_cancel_ride);
        tv_cancel_ride.setText("Cancel Ride");

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomerModule customerModule = new CustomerModule(RidesSelectedActivity.this);
                customerModule.cancelReasonApi(language_id);
                dialog.dismiss();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void showReasonDialog() {

        final Dialog dialog = new Dialog(RidesSelectedActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        dialog.setCancelable(false);
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_for_cancel_reason);

        final ListView lv_reasons = (ListView) dialog.findViewById(R.id.lv_reasons);
        lv_reasons.setAdapter(new ReasonAdapter(RidesSelectedActivity.this, cancelReasonCustomer));
        final LinearLayout otherReason = (LinearLayout) dialog.findViewById(R.id.otherReason);
        final EditText otherReasonText = (EditText) dialog.findViewById(R.id.otherReasonText);
        Button otherReasonBtn = (Button) dialog.findViewById(R.id.otherReasonBtn);

        lv_reasons.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                reason_id = cancelReasonCustomer.getMsg().get(position).getReasonId();
                String reasonText = cancelReasonCustomer.getMsg().get(position).getReasonName();
                if(reasonText.equalsIgnoreCase("other") || reasonText.equalsIgnoreCase("others")){
                    lv_reasons.setVisibility(View.GONE);
                    otherReason.setVisibility(View.VISIBLE);
                }else{
                    RideModule rideModule = new RideModule(RidesSelectedActivity.this);
                    rideModule.cancelRideApi(ride_id, reason_id, "", language_id);
                    dialog.dismiss();
                }
            }
        });

        otherReasonBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String reasonText = otherReasonText.getText().toString().trim();
                if(reasonText.length() <= 0){
                    Toasty.error(RidesSelectedActivity.this, "Please enter reason.", Toast.LENGTH_SHORT).show();
                }else{
                    RideModule rideModule = new RideModule(RidesSelectedActivity.this);
                    rideModule.cancelRideApi(ride_id, "0", reasonText, language_id);
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }

    @Override
    public void onAPIRunningState(int a) {
        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            pd.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            pd.dismiss();
        }
    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("View Ride Info")) {
            ViewRideInfo viewRideInfo;
            viewRideInfo = gson.fromJson(response, ViewRideInfo.class);

            if (viewRideInfo.getResult().toString().equals("1")) {
                rideId = viewRideInfo.getDetails().getRideId();
                pickupLat = viewRideInfo.getDetails().getPickupLat();
                pickupLong = viewRideInfo.getDetails().getPickupLong();
                String pickup_location = viewRideInfo.getDetails().getPickupLocation();
                String drop_location = viewRideInfo.getDetails().getDropLocation();
                driverName = viewRideInfo.getDetails().getDriverName();
                driverImage = viewRideInfo.getDetails().getDriverImage();
                driverPhone = viewRideInfo.getDetails().getDriverPhone();
                driverLat = viewRideInfo.getDetails().getDriverLat();
                driverLong = viewRideInfo.getDetails().getDriverLong();
                carNumber = viewRideInfo.getDetails().getCarNumber();
                rating = viewRideInfo.getDetails().getDriverRating();
                String car_type_name = viewRideInfo.getDetails().getCarTypeName();
                String car_model_name = viewRideInfo.getDetails().getCarModelName();
                String amount = viewRideInfo.getDetails().getAmount();
                String distance = viewRideInfo.getDetails().getDistance();
                String time = viewRideInfo.getDetails().getTime();
                String start_loation = viewRideInfo.getDetails().getBeginLocation();
                String end_location = viewRideInfo.getDetails().getEndLocation();
                String car_type_image = viewRideInfo.getDetails().getCarTypeImage();
                done_ride_id = viewRideInfo.getDetails().getDoneRideId();
                String map_image = viewRideInfo.getDetails().getRideImage();

                Logger.e("done Ride id      " + done_ride_id);

                Picasso.with(RidesSelectedActivity.this)
                        .load(map_image)
                        .placeholder(R.drawable.dummy_pic)
                        .error(R.drawable.dummy_pic)
                        .fit()
                        .into(iv_map);
                tv_name_driver.setText(driverName);
                tv_name_car.setText(car_type_name + ", " + car_model_name);
                tv_dis.setText(distance + " miles");
                tv_time1.setText(time);
                tv_start_location.setText(start_loation);
                tv_end_location.setText(end_location);
                tv_tota.setText("$ " + amount);
//                tv_rating.setText(rating);
                Picasso.with(this)
                        .load(imageDomain + driverImage)
                        .placeholder(R.drawable.dummy_pic)
                        .error(R.drawable.dummy_pic)
                        .fit()
                        .into(iv_image_driver);
                Picasso.with(this)
                        .load(imageDomain + car_type_image)
                        .placeholder(R.drawable.dummy_pic)
                        .error(R.drawable.dummy_pic)
                        .fit()
                        .into(iv_image_car);

                if (rating.equals("")) {
                } else {
                    Float rate = Float.valueOf(rating);
                    rating_selected.setRating(rate);
                }
                if (ride_status.equals("1")) {
                    tv_start_location.setText(pickup_location);
                    tv_end_location.setText(drop_location);
                } else if (ride_status.equals("2")) {
                    tv_start_location.setText(pickup_location);
                    tv_end_location.setText(drop_location);
                } else if (ride_status.equals("3")) {
                    tv_start_location.setText(pickup_location);
                    tv_end_location.setText(drop_location);
                } else if (ride_status.equals("4")) {
                    tv_start_location.setText(pickup_location);
                    tv_end_location.setText(drop_location);
                } else if (ride_status.equals("5")) {
                    tv_start_location.setText(pickup_location);
                    tv_end_location.setText(drop_location);
                } else if (ride_status.equals("6")) {
                    tv_start_location.setText(start_loation);
                    tv_end_location.setText(drop_location);
                } else if (ride_status.equals("7")) {
                    tv_start_location.setText(start_loation);
                    tv_end_location.setText(end_location);
                } else if (ride_status.equals("8")) {
                    tv_start_location.setText(pickup_location);
                    tv_end_location.setText(drop_location);
                } else if (ride_status.equals("9")) {
                    tv_start_location.setText(pickup_location);
                    tv_end_location.setText(drop_location);
                }
            } else {
                Toast.makeText(RidesSelectedActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
            }
        }

        if (apiName.equals("Cancel Ride")) {
            DeviceId deviceId;
            deviceId = gson.fromJson(response, DeviceId.class);
            if (deviceId.getResult().toString().equals("1")) {
                finish();
            } else {
                Toast.makeText(this, "" + deviceId.getMsg(), Toast.LENGTH_SHORT).show();
            }
        }

        if (apiName.equals("View Reasons")) {
            ResultCheck resultCheck;
            resultCheck = gson.fromJson(response, ResultCheck.class);
            if (resultCheck.result.equals("1")) {
                cancelReasonCustomer = gson.fromJson(response, CancelReasonCustomer.class);
                showReasonDialog();
            } else {
                Toast.makeText(this, "No Data Found", Toast.LENGTH_SHORT).show();
            }
        }

        if (apiName.equals("Resend Invoice")) {
            DeviceId deviceId;
            deviceId = gson.fromJson(response, DeviceId.class);
            if (deviceId.getResult().toString().equals("1")) {
                Toast.makeText(this, "" + deviceId.getMsg().toString(), Toast.LENGTH_SHORT).show();
            } else {
                Logger.e("err       " + deviceId.getMsg().toString());
            }
        }

    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }
}
