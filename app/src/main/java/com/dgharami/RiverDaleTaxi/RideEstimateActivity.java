package com.dgharami.RiverDaleTaxi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class RideEstimateActivity extends AppCompatActivity implements ApiFetcher {
    public static Activity rideestimateactivity;

    TextView tv_price, tv_pickup, tv_drop, tv_time;
    LinearLayout ll_back_ride_estimate;
    ProgressDialog pd;

    LanguageManager languageManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_estimate);
        getSupportActionBar().hide();
        rideestimateactivity = this;

        tv_drop = (TextView) findViewById(R.id.tv_drop);
        tv_pickup = (TextView) findViewById(R.id.tv_pickup);
        tv_price = (TextView) findViewById(R.id.tv_price);
        tv_time = (TextView) findViewById(R.id.tv_time);
        ll_back_ride_estimate = (LinearLayout) findViewById(R.id.ll_back_ride_estimate);

        String pickup = super.getIntent().getExtras().getString("pickup");
        String drop = super.getIntent().getExtras().getString("drop");
        String price = super.getIntent().getExtras().getString("price");
        String time = super.getIntent().getExtras().getString("time");

        tv_pickup.setText(pickup);
        tv_drop.setText(drop);
        tv_price.setText("$ " + price);
        tv_time.setText(time);

        ll_back_ride_estimate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onAPIRunningState(int a) {

        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            pd.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            pd.dismiss();
        }
    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }
}
