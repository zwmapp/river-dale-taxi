package com.dgharami.RiverDaleTaxi;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.dgharami.RiverDaleTaxi.adapter.CardsAdapter;
import com.dgharami.RiverDaleTaxi.adapter.PaymentAdapter;
import com.dgharami.RiverDaleTaxi.logger.Logger;
import com.dgharami.RiverDaleTaxi.manager.DeviceManager;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.manager.SessionManager;
import com.dgharami.RiverDaleTaxi.models.ResultCheck;
import com.dgharami.RiverDaleTaxi.models.about.About;
import com.dgharami.RiverDaleTaxi.models.cartype.CarType;
import com.dgharami.RiverDaleTaxi.models.deviceid.DeviceId;
import com.dgharami.RiverDaleTaxi.models.ridenow.RideNow;
import com.dgharami.RiverDaleTaxi.models.viewcard.ViewCard;
import com.dgharami.RiverDaleTaxi.models.viewdrivers.ViewDrivers;
import com.dgharami.RiverDaleTaxi.models.viewpaymentoption.ViewPaymentOption;
import com.dgharami.RiverDaleTaxi.others.AerialDistance;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.others.Contants;
import com.dgharami.RiverDaleTaxi.others.DownloadTask;
import com.dgharami.RiverDaleTaxi.others.FirebaseDriverLatLng;
import com.dgharami.RiverDaleTaxi.others.MessageEvent;
import com.dgharami.RiverDaleTaxi.others.MyProgress;
import com.dgharami.RiverDaleTaxi.others.RadiusAnimation;
import com.dgharami.RiverDaleTaxi.parsing.AccountModule;
import com.dgharami.RiverDaleTaxi.parsing.CustomerModule;
import com.dgharami.RiverDaleTaxi.parsing.GooglePlacesModule;
import com.dgharami.RiverDaleTaxi.parsing.HelpModule;
import com.dgharami.RiverDaleTaxi.parsing.RideModule;
import com.dgharami.RiverDaleTaxi.parsing.ViewModule;
import com.dgharami.RiverDaleTaxi.urls.Apis;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.h6ah4i.android.materialshadowninepatch.MaterialShadowContainerView;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MainActivity extends BaseActivity implements
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener,
        ApiFetcher,
        Apis,
        NavigationView.OnNavigationItemSelectedListener,
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        GoogleMap.OnCameraMoveStartedListener,
        GoogleMap.OnCameraIdleListener {

    TextView pickup_address, tv_time_calc, tv_car_type_selected, tv_drop_address, tv_profile_name, tv_profile_email, tv_ride_estimate, tv_selected_payment;
    LinearLayout ll_addview, ll_for_ride, ll_for_confirm, ll_drop_location, ll_ride_now,
            ll_ride_later, ll_confirm_ride, ll_cancel_ride,
            ll_ride_estimate, ll_coupon, ll_driver_detail, ll_set_payment;
    View v;
    ImageView iv_car, iv_car_selected, iv_selected_car, iv_profile_pic;
    FloatingActionButton fab;

    BroadcastReceiver broadcastReceiver;

    public static Activity mainActivity;
    public static Dialog dialog;

    String city = "";

    String car_id = "", car_name = "", car_image = "", coupon_code = "", dropLocation = "",
            pickupLocation = "", carTypeCheck = "", driverCheck = "", price,
            city_id, name, email, user_id, profile, deviceToken,
            laterTime, laterDate, message = "", callSupport, android_id, paymentOptionCheck = "";

    MapView mapView;
    GoogleMap mGoogleMap;
    Geocoder geocoder;
    List<Address> addresses;
    //    GPSTracker gpsTracker;
    double pickupLat, pickupLong, dropLat, dropLong, newLat, newLong;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    CameraPosition cameraPosition;

    LatLng latLng;

    int checkRideType = 0;

    SessionManager sessionManager;
    DeviceManager deviceManager;
    LanguageManager languageManager;


    Calendar calendar;
    CarType carType;
    ViewModule viewModule;
    GooglePlacesModule googlePlacesModule;
    RideModule rideModule;
    AccountModule accountModule;
    HelpModule helpModule;
    AerialDistance aerialDistance;
    ViewPaymentOption viewPaymentOption;

    String ride_id, ride_status;

    CustomerModule customerModule;

    String language_id, card_id = "", payment_option_id = "3";

    ViewCard viewCard;

    String ACCESS_FINE_LOCATION = "android.permission.ACCESS_FINE_LOCATION";

    List<String> alCarId = new ArrayList<>();
    List<String> alCarName = new ArrayList<>();
    List<String> alCarImage = new ArrayList<>();
    List<String> alDistance = new ArrayList<>();
    ArrayList<String> alDriverId = new ArrayList<>();
    ArrayList<String> alDriverLat = new ArrayList<>();
    ArrayList<String> alDriverLong = new ArrayList<>();
    List<String> alSelected = new ArrayList<>();
    String etaFroSelectedCar = "";

//    String from1 = "";
//    boolean flag = true;

    // MOVE CAR ANIMATION DEFAULT VALUE previousStep = 0f;
    private Marker marker;
    //private List<Marker> allDriversMarker = new ArrayList<>();
    private Map<String, Marker> allDriversMarker = new HashMap<>();
    float previousStep = 0f;
    ValueAnimator animationMove;
    Marker routeStart, routeEnd;
    int globalZoom = 16;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference allDriversLiveLatLng;

    MaterialShadowContainerView dgLocationContainer;
    MaterialShadowContainerView dgFooterArea;
    boolean isMapInteractedWithUser = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActivity = this;

        firebaseDatabase = FirebaseDatabase.getInstance();
        allDriversLiveLatLng = firebaseDatabase.getReference("RiverDaleCabProject").child("allDrivers");

        dgLocationContainer = (MaterialShadowContainerView) findViewById(R.id.dgLocationContainer);
        dgFooterArea = (MaterialShadowContainerView) findViewById(R.id.dgFooterArea);

        ll_for_confirm = (LinearLayout) findViewById(R.id.ll_for_confirm);
        ll_for_ride = (LinearLayout) findViewById(R.id.ll_for_ride);
        ll_ride_later = (LinearLayout) findViewById(R.id.ll_ride_later);
        ll_ride_now = (LinearLayout) findViewById(R.id.ll_ride_now);
        ll_cancel_ride = (LinearLayout) findViewById(R.id.ll_cancel_ride);
        ll_confirm_ride = (LinearLayout) findViewById(R.id.ll_confirm_ride);
        ll_set_payment = (LinearLayout) findViewById(R.id.ll_set_payment);
        ll_ride_estimate = (LinearLayout) findViewById(R.id.ll_ride_estimate);
        ll_coupon = (LinearLayout) findViewById(R.id.ll_coupon);
        ll_driver_detail = (LinearLayout) findViewById(R.id.ll_driver_detail);
        ll_drop_location = (LinearLayout) findViewById(R.id.ll_drop_location);
        tv_drop_address = (TextView) findViewById(R.id.tv_drop_address);
        pickup_address = (TextView) findViewById(R.id.pickup_address);
        tv_car_type_selected = (TextView) findViewById(R.id.tv_car_type_selected);
        tv_time_calc = (TextView) findViewById(R.id.tv_time_calc);
        iv_selected_car = (ImageView) findViewById(R.id.iv_selected_car);
        ll_addview = (LinearLayout) findViewById(R.id.ll_addview);

        tv_selected_payment = (TextView) findViewById(R.id.tv_selected_payment);
        tv_ride_estimate = (TextView) findViewById(R.id.tv_ride_estimate);

        tv_ride_estimate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // "Ride Estimate"
                EventBus.getDefault().post(new MessageEvent(GooglePlacesModule.value, 2));
            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab);
        mapView = (MapView) findViewById(R.id.map_main);
        mapView.onCreate(savedInstanceState);

        dialog = new MyProgress(this).getProgressDialog("Looking for Drivers...");

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                dialog.dismiss();
                Bundle b = intent.getExtras();
                ride_id = b.getString("ride_id");
                ride_status = b.getString("ride_status");
                if (ride_status.equals("3")) {
                    dialogForAcceptRide();
                } else if (ride_status.equals("4")) {
                    dialogForRejectRide();
                }
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("broadCastName"));

        try {
            onNewIntent(getIntent());
        } catch (Exception e) {
            Logger.e("Exception in OnNew Intent     " + e.toString());
        }

        android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        Logger.e("android id        " + android_id);

        Contants.mainActivity = 0;
        rideModule = new RideModule(MainActivity.this);
        googlePlacesModule = new GooglePlacesModule(this);
        sessionManager = new SessionManager(this);
        deviceManager = new DeviceManager(this);
        viewModule = new ViewModule(this);
        accountModule = new AccountModule(MainActivity.this);
        helpModule = new HelpModule(this);
//        gpsTracker = new GPSTracker(this);
        aerialDistance = new AerialDistance();
        languageManager = new LanguageManager(this);
        language_id = languageManager.getLanguageDetail().get(LanguageManager.LANGUAGE_ID);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View hView = navigationView.getHeaderView(0);
        iv_profile_pic = (ImageView) hView.findViewById(R.id.iv_profile_pic);
        tv_profile_name = (TextView) hView.findViewById(R.id.tv_profile_name);
        tv_profile_email = (TextView) hView.findViewById(R.id.tv_profile_email);

        name = sessionManager.getUserDetails().get(SessionManager.USER_NAME);
        email = sessionManager.getUserDetails().get(SessionManager.USER_PHONE);
        user_id = sessionManager.getUserDetails().get(SessionManager.USER_ID);
        profile = sessionManager.getUserDetails().get(SessionManager.USER_IMAGE);
        deviceToken = FirebaseInstanceId.getInstance().getToken();

//        cityLocation = super.getIntent().getExtras().getString("cityLocation");
//        Logger.e("cityLocation  " + cityLocation);

        tv_profile_name.setText(name);
        tv_profile_email.setText(email);
        Picasso.with(this)
                .load(imageDomain + profile)
                .placeholder(R.drawable.dummy_pic)
                .error(R.drawable.dummy_pic)
                .fit()
                .into(iv_profile_pic);

        accountModule.deviceIdApi(user_id, deviceToken, language_id);
        helpModule.callSupportApi(language_id);
//        viewModule.viewPaymentOptionApi(language_id);
        customerModule = new CustomerModule(this);

//        customerModule.viewCardApi(user_id, language_id);

        ll_ride_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dropLocation.equals("")) {
                    Toast.makeText(MainActivity.this, "Please Enter drop Location", Toast.LENGTH_SHORT).show();
                } else {
                    if (driverCheck.equals("2")) {
                        showDialogForCars();
                    } else {
                        checkRideType = 0;
                        ll_for_ride.setVisibility(View.GONE);
                        ll_for_confirm.setVisibility(View.VISIBLE);
                        if (car_id.equals("")) {
                            tv_car_type_selected.setText(alCarName.get(0));
                            Picasso.with(MainActivity.this).load(imageDomain + alCarImage.get(0)).fit().into(iv_selected_car);
                        } else {
                            tv_car_type_selected.setText(car_name);
                            Picasso.with(MainActivity.this).load(imageDomain + car_image).fit().into(iv_selected_car);
                        }
                        tv_time_calc.setText(etaFroSelectedCar);
                    }
                }
            }
        });

        ll_ride_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dropLocation.equals("")) {
                    Toast.makeText(MainActivity.this, "Please Enter drop Location", Toast.LENGTH_SHORT).show();
                } else {
                    checkRideType = 1;
                    calendar = Calendar.getInstance();
                    DatePickerDialog dpd = DatePickerDialog.newInstance(MainActivity.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                    dpd.setMinDate(calendar);
                    dpd.setAccentColor(Color.parseColor("#000000"));
                    dpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                        }
                    });
                    dpd.show(getFragmentManager(), "Date Picker Dialog");
                }
            }
        });

        ll_confirm_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (card_id.equals("")) {
                    Toast.makeText(MainActivity.this, "Please Select Payment Method", Toast.LENGTH_SHORT).show();
                } else {

                    if (car_id.equals("")) {
                        if (checkRideType == 0) {
                            RideModule rideModule = new RideModule(MainActivity.this);
                            rideModule.rideNowApi(user_id, coupon_code, String.valueOf(pickupLat), String.valueOf(pickupLong), pickupLocation, GooglePlacesModule.latitude, GooglePlacesModule.longitude, dropLocation, alCarId.get(0), language_id, payment_option_id, card_id);
                        }
                        if (checkRideType == 1) {
                            RideModule rideModule = new RideModule(MainActivity.this);
                            rideModule.rideLaterApi(user_id, coupon_code, String.valueOf(pickupLat), String.valueOf(pickupLong), pickupLocation, GooglePlacesModule.latitude, GooglePlacesModule.longitude, dropLocation, laterDate, laterTime, alCarId.get(0), language_id, payment_option_id, card_id);
                        }
                    } else {
                        if (checkRideType == 0) {
                            RideModule rideModule = new RideModule(MainActivity.this);
                            rideModule.rideNowApi(user_id, coupon_code, String.valueOf(pickupLat), String.valueOf(pickupLong), pickupLocation, GooglePlacesModule.latitude, GooglePlacesModule.longitude, dropLocation, car_id, language_id, payment_option_id, card_id);
                        }
                        if (checkRideType == 1) {
                            RideModule rideModule = new RideModule(MainActivity.this);
                            rideModule.rideLaterApi(user_id, coupon_code, String.valueOf(pickupLat), String.valueOf(pickupLong), pickupLocation, GooglePlacesModule.latitude, GooglePlacesModule.longitude, dropLocation, laterDate, laterTime, car_id, language_id, payment_option_id, card_id);
                        }
                    }
                }
            }
        });

        ll_cancel_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_for_ride.setVisibility(View.VISIBLE);
                ll_for_confirm.setVisibility(View.GONE);
            }
        });

        ll_coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(MainActivity.this, CouponActivity.class), 101);
                overridePendingTransition(R.anim.animation3, R.anim.animation4);
            }
        });

        ll_drop_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivityForResult(new Intent(MainActivity.this, DropLocationActivity.class)
                        .putExtra("name", "1"), 102);
            }
        });

        ll_set_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivityForResult(new Intent(MainActivity.this, AddCardActivity.class)
                        .putExtra("From", "MainActivity"), 103);

//                startActivityForResult(new Intent(MainActivity.this, CouponActivity.class), 101);


//                if (paymentOptionCheck.equals("1")) {
//                    dialogForSelectPayment();
//                } else {
//                    Toast.makeText(MainActivity.this, "No Record Found", Toast.LENGTH_SHORT).show();
//                }
            }
        });

//        ll_ride_estimate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(MainActivity.this, RideEstimateActivity.class)
//                        .putExtra("pickup", pickupLocation)
//                        .putExtra("drop", dropLocation)
//                        .putExtra("price", price)
//                        .putExtra("time", GooglePlacesModule.time));
//            }
//        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if (mLastLocation != null) {
                    pickupLat = mLastLocation.getLatitude();
                    pickupLong = mLastLocation.getLongitude();
                    latLng = new LatLng(pickupLat, pickupLong);
                    cameraPosition = new CameraPosition.Builder().target(latLng).zoom(17).build();
                    mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                    try {
                        new GetLocationAsync(pickupLat, pickupLong).execute();
                    } catch (Exception e) {
                        Log.e("err", e.toString());
                    }

                    try {
                        viewModule.viewCarsApi(city, String.valueOf(pickupLat), String.valueOf(pickupLong), language_id);
                        if (car_id.equals("")) {
                            viewModule.viewDriversApi(String.valueOf(pickupLat), String.valueOf(pickupLong), city_id, alCarId.get(0), language_id);
                        } else {
                            viewModule.viewDriversApi(String.valueOf(pickupLat), String.valueOf(pickupLong), city_id, car_id, language_id);
                        }
                    } catch (Exception e) {
                        Log.e("err", e.toString());
                    }
                }
//                mGoogleMap.setOnMyLocationChangeListener(MainActivity.this);
            }
        });

        if ((ContextCompat.checkSelfPermission(MainActivity.this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{"android.permission.ACCESS_FINE_LOCATION"}, 1);
        } else {
            mapView.getMapAsync(this);
        }

        PackageManager manager = this.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(this.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version_name = info.versionName;
        int version_code = info.versionCode;

        CustomerModule customerModule = new CustomerModule(this);
        customerModule.updateApi(version_code + "", language_id);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        int month = monthOfYear + 1;
        laterDate = dayOfMonth + "/" + month + "/" + year;
        calendar = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(MainActivity.this, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false);
        tpd.setAccentColor(Color.parseColor("#000000"));
        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {

            }
        });
        tpd.show(getFragmentManager(), "Time Picker Dialog");
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        laterTime = hourString + ":" + minuteString;
        Logger.e("Date & Time       " + laterDate + ",  " + laterTime);
        ll_for_ride.setVisibility(View.GONE);
        ll_for_confirm.setVisibility(View.VISIBLE);
    }

//    public void showLocationDialog() {
//
//        dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        Window window = dialog.getWindow();
//        window.setGravity(Gravity.CENTER);
//        dialog.setContentView(R.layout.dialog_for_location);
//        dialog.setCancelable(false);
//        dialog.show();
//    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        Contants.mainActivity = 0;

//        from1 = "";

        name = sessionManager.getUserDetails().get(SessionManager.USER_NAME);
        email = sessionManager.getUserDetails().get(SessionManager.USER_EMAIL);
        profile = sessionManager.getUserDetails().get(SessionManager.USER_IMAGE);
        tv_profile_name.setText(name);
        tv_profile_email.setText(email);
        Picasso.with(this)
                .load(imageDomain + profile)
                .placeholder(R.drawable.dummy_pic)
                .error(R.drawable.dummy_pic)
                .fit()
                .into(iv_profile_pic);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Contants.mainActivity = 0;
        mapView.onResume();

//        from1 = "";
    }

    @Override
    protected void onPause() {
        super.onPause();
        Contants.mainActivity = 1;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Contants.mainActivity = 1;
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onAPIRunningState(int a) {

        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            dialog.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            dialog.dismiss();
        }
    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("Device Id")) {
            DeviceId deviceId;
            deviceId = gson.fromJson(response, DeviceId.class);
            if (deviceId.getResult().toString().equals("1")) {
                Logger.e("Device Id         " + deviceId.getMsg());
            } else {
                Logger.e("Device Id         " + deviceId.getMsg());
            }
        }

        if (apiName.equals("Call Support")) {
            About about;
            about = gson.fromJson(response, About.class);
            if (about.getResult().toString().equals("1")) {
                callSupport = about.getDetails().getDescription();
            }
        }

        if (apiName.equals("Update")) {

            DeviceId deviceId;
            deviceId = gson.fromJson(response, DeviceId.class);

            if (deviceId.getResult().toString().equals("1")) {
                Logger.e("success" + deviceId.getMsg());
            } else if (deviceId.getResult().toString().equals("418")) {
                Logger.e("Yhan Tak  " + "a gya");
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(3000);
                            MainActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    dialogForUpdate();
                                }
                            });

                        } catch (Exception e) {
                            Logger.e("Exception Caught in Thread      " + e.toString());
                        }
                    }
                };
                Thread t = new Thread(r);
                t.start();
            } else {
                Logger.e("err a gyi" + deviceId.getMsg());
            }
        }

        if (apiName.equals("View Cars")) {
            alCarId.clear();
            alCarImage.clear();
            alCarName.clear();
            alDistance.clear();
            ResultCheck resultCheck;
            resultCheck = gson.fromJson(response, ResultCheck.class);
            if (resultCheck.result.equals("1")) {
                carTypeCheck = "1";
                alSelected.clear();
                carType = gson.fromJson(response, CarType.class);
                city_id = carType.getMsg().get(0).getCityId();
                for (int i = 0; i < carType.getMsg().size(); i++) {
                    alCarId.add(carType.getMsg().get(i).getCarTypeId());
                    alCarName.add(carType.getMsg().get(i).getCarTypeName());
                    alCarImage.add(carType.getMsg().get(i).getCarTypeImage());
                    alDistance.add(carType.getMsg().get(i).getDistance());
                    if (i == 0) {
                        alSelected.add("1");
                    } else {
                        alSelected.add("0");
                    }
                }
                ll_addview.removeAllViews();
                for (int j = 0; j < alCarId.size(); j++) {
                    ll_addview.addView(HorizontalList(R.layout.itemlayoutforhorizontal, alCarId.get(j), alCarName.get(j), alCarImage.get(j), alSelected.get(j), j, alDistance.get(j)));
                }
            } else {
                carTypeCheck = "2";
            }
        }

        if (apiName.equals("View Payment Option")) {

            ResultCheck resultCheck;
            resultCheck = gson.fromJson(response, ResultCheck.class);
            if (resultCheck.result.equals("1")) {
                paymentOptionCheck = "1";
                viewPaymentOption = gson.fromJson(response, ViewPaymentOption.class);
            } else {
                paymentOptionCheck = "2";
            }
        }

        if (apiName.equals("View Drivers")) {
            alDriverId.clear();
            alDriverLat.clear();
            alDriverLong.clear();
//DG            mGoogleMap.clear();
            ResultCheck resultCheck;
            resultCheck = gson.fromJson(response, ResultCheck.class);

            for (Map.Entry<String, Marker> tmpMerker : allDriversMarker.entrySet()) {
                ((Marker) tmpMerker.getValue()).remove();
            }
            allDriversMarker.clear();

            if (resultCheck.result.equals("1")) {
                driverCheck = "1";
                ViewDrivers viewDrivers;
                viewDrivers = gson.fromJson(response, ViewDrivers.class);
                for (int i = 0; i < viewDrivers.getMsg().size(); i++) {
                    alDriverId.add(viewDrivers.getMsg().get(i).getDriverId());
                    alDriverLat.add(viewDrivers.getMsg().get(i).getDriverLat());
                    alDriverLong.add(viewDrivers.getMsg().get(i).getDriverLong());
                }

//                Logger.e("id    " + alDriverId);
//                Logger.e("lat   " + alDriverLat);
//                Logger.e("long    " + alDriverLong);


                try {
                    for (int k = 0; k < alDriverId.size(); k++) {
                        LatLng currentDriverPosition = new LatLng(Double.parseDouble((alDriverLat.get(k))), Double.parseDouble((alDriverLong.get(k))));
                        Marker tmpMarker = mGoogleMap.addMarker(
                                new MarkerOptions()
                                        .position(currentDriverPosition)
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.car_ola))
                        );
                        tmpMarker.setAnchor(0.5f, 0.5f);
                        allDriversMarker.put("" + alDriverId.get(k), tmpMarker);
                    }
                } catch (Exception e) {
                    Logger.e("Exception in View Drivers         " + e.toString());
                }
            } else {
                driverCheck = "2";
//DG                mGoogleMap.clear();
            }
        }

        if (apiName.equals("Ride Now")) {
            RideNow rideNow;
            rideNow = gson.fromJson(response, RideNow.class);
            if (rideNow.getResult().toString().equals("1")) {
                ll_for_ride.setVisibility(View.VISIBLE);
                ll_for_confirm.setVisibility(View.GONE);
            } else {
                Toast.makeText(MainActivity.this, "" + rideNow.getMsg(), Toast.LENGTH_SHORT).show();
            }
        }

        if (apiName.equals("Ride Later")) {
            RideNow rideNow;
            rideNow = gson.fromJson(response, RideNow.class);

            if (rideNow.getResult().toString().equals("1")) {
                ll_for_ride.setVisibility(View.VISIBLE);
                ll_for_confirm.setVisibility(View.GONE);
                Toast.makeText(this, "" + rideNow.getMsg().toString(), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "" + rideNow.getMsg().toString(), Toast.LENGTH_SHORT).show();
            }
        }

        if (apiName.equals("Ride Estimate")) {
            DeviceId deviceId;
            deviceId = gson.fromJson(response, DeviceId.class);
            if (deviceId.getResult().toString().equals("1")) {
                price = deviceId.getMsg().toString();

                tv_ride_estimate.setText("Estimated Fare - $" + price + "      Estimated Time - " + GooglePlacesModule.time);

            } else {
                Log.e("err", deviceId.getMsg().toString());
            }
        }

        if (apiName.equals("View Card")) {

            ResultCheck resultCheck;
            resultCheck = gson.fromJson(response, ResultCheck.class);
            if (resultCheck.result.equals("1")) {
                viewCard = gson.fromJson(response, ViewCard.class);
//                dialogForViewCards();
            } else {
//                Toast.makeText(this, "Please Add Card First", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id == R.id.book_a_ride) {
        } else if (id == R.id.profile) {
            startActivity(new Intent(MainActivity.this, ProfileActivity.class));
        } else if (id == R.id.rides) {
            startActivity(new Intent(this, RidesActivity.class));
        }/* else if (id == R.id.rate_card) {
            if (car_id.equals("")) {
                startActivity(new Intent(MainActivity.this, RateCardActivity.class)
                        .putExtra("cityLocation", "Dummy City")
                        .putExtra("car_type_id", carType.getMsg().get(0).getCarTypeId())
                        .putExtra("car_type_name", carType.getMsg().get(0).getCarTypeName()));
            } else {
                startActivity(new Intent(MainActivity.this, RateCardActivity.class).putExtra("cityLocation", "Dummy City")
                        .putExtra("car_type_id", car_id)
                        .putExtra("car_type_name", car_name));
            }
        }*/ else if (id == R.id.report_issue) {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "debashish@zoomwebmedia.ca", null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Report Issue River Dale Passenger (Android)");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "");
            startActivity(Intent.createChooser(emailIntent, "Send email..."));
            emailIntent.setType("text/plain");
        }/* else if (id == R.id.call_support) {
            String uri = "tel:" + callSupport.trim();
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse(uri));
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                startActivity(intent);
            }
        }*/ else if (id == R.id.t_c) {
            startActivity(new Intent(this, TcActivity.class));
        } else if (id == R.id.about) {
            startActivity(new Intent(this, AboutActivity.class));
        } else if (id == R.id.credit_card) {
            startActivity(new Intent(MainActivity.this, AddCardActivity.class)
                    .putExtra("From", "Navigation"));
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mGoogleMap = googleMap;
        MapsInitializer.initialize(this);
        mGoogleMap.setOnCameraMoveStartedListener(MainActivity.this);
        mGoogleMap.setOnCameraIdleListener(MainActivity.this);

        // Custom Map Style
        try {
            boolean success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style));
        } catch (Resources.NotFoundException e) {
        }

        switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this)) {
            case ConnectionResult.SUCCESS:
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    mGoogleMap.setMyLocationEnabled(true);
                }
                mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
                mGoogleMap.getUiSettings().setZoomGesturesEnabled(true);
                mGoogleMap.getUiSettings().setCompassEnabled(true);
                mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
                mGoogleMap.getUiSettings().setRotateGesturesEnabled(true);
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                buildGoogleApiClient();
                mGoogleApiClient.connect();

                mGoogleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(CameraPosition cameraPosition) {
//                        if (flag == true) {
//
//                            Toast.makeText(MainActivity.this, "Camera if chal gya", Toast.LENGTH_SHORT).show();
//                        } else if (flag == false) {

//                            Toast.makeText(MainActivity.this, "Camera else chal gya", Toast.LENGTH_SHORT).show();
//                            mGoogleMap.setOnMyLocationChangeListener(null);
                        LatLng latLng1 = mGoogleMap.getCameraPosition().target;
                        pickupLat = latLng1.latitude;
                        pickupLong = latLng1.longitude;


                        try {
                            viewModule.viewCarsApi(city, String.valueOf(pickupLat), String.valueOf(pickupLong), language_id);
                            if (car_id.equals("")) {
                                viewModule.viewDriversApi(String.valueOf(pickupLat), String.valueOf(pickupLong), city_id, alCarId.get(0), language_id);
                            } else {
                                viewModule.viewDriversApi(String.valueOf(pickupLat), String.valueOf(pickupLong), city_id, car_id, language_id);
                            }
                        } catch (Exception e) {
                            Log.e("err", e.toString());
                        }

                        try {
                            new GetLocationAsync(pickupLat, pickupLong).execute();
                        } catch (Exception e) {
                            Log.e("err", e.toString());
                        }
//                        }
                    }
                });

//DG                ValueEventListener allDriversLatLongListener = new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        List<FirebaseDriverLatLng> allDriversData = dataSnapshot.getValue();
//                        FirebaseDriverLatLng tmpCurrentDriversPosition =  dataSnapshot.getValue(FirebaseDriverLatLng.class);
//                        Logger.d("TEST", tmpCurrentDriversPosition.toString());
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//DG                };
                ChildEventListener allDriverDataListener = new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//NEWDG                        FirebaseDriverLatLng tmpCurrentDriverPosition =  dataSnapshot.getValue(FirebaseDriverLatLng.class);
//                        LatLng currentDriverPosition = new LatLng(tmpCurrentDriverPosition.getLatitude(), tmpCurrentDriverPosition.getLongitude());
//                        Marker tmpMarker = mGoogleMap.addMarker(
//                                new MarkerOptions()
//                                        .position(currentDriverPosition)
//                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.car_ola))
//                        );
//                        tmpMarker.setAnchor(0.5f, 0.5f);
//NEWDG                        allDriversMarker.put(dataSnapshot.getKey(), tmpMarker);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        FirebaseDriverLatLng tmpCurrentDriverPosition = dataSnapshot.getValue(FirebaseDriverLatLng.class);
                        LatLng currentDriverPosition = new LatLng(tmpCurrentDriverPosition.getLatitude(), tmpCurrentDriverPosition.getLongitude());
                        if (null != allDriversMarker.get(dataSnapshot.getKey())) {
                            Marker marker = (Marker) allDriversMarker.get(dataSnapshot.getKey());
                            float rotationAngle = (float) getAngleFromLatLng(marker.getPosition(), currentDriverPosition);
                            if (null != animationMove && animationMove.isRunning()) {
                                animationMove.end();
                            }
                            smoothRotateAndGoTheCar(marker, rotationAngle, currentDriverPosition);
                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        ((Marker) allDriversMarker.get(dataSnapshot.getKey())).remove();
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                };
                allDriversLiveLatLng.addChildEventListener(allDriverDataListener);

//                mGoogleMap.setOnMyLocationChangeListener(this);

//                mGoogleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
//                    @Override
//                    public void onMyLocationChange(Location location) {
//
//                        from1.equals("1");
//                        newLat = location.getLatitude();
//                        newLong = location.getLongitude();
//
//                        double distance_between = aerialDistance.aerialDistanceFunction(pickupLat, pickupLong, newLat, newLong);
//                        Toast.makeText(MainActivity.this, ""+distance_between, Toast.LENGTH_SHORT).show();
//                        if (distance_between < 0.4) {
//                            // Ignore this code
////            Toast.makeText(this, "Location Changed if", Toast.LENGTH_SHORT).show();
//                        } else {
////            Toast.makeText(this, "Location Changed else", Toast.LENGTH_SHORT).show();
//                            pickupLat = newLat;
//                            pickupLong = newLong;
//                            latLng = new LatLng(newLat, newLong);
//                            cameraPosition = new CameraPosition.Builder().target(latLng).zoom(15).build();
//                            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//                            try {
//                                new GetLocationAsync(pickupLat, pickupLong).execute();
//                            } catch (Exception e) {
//                                Log.e("err", e.toString());
//                            }
//                            try {
//                                viewModule.viewCarsApi(city, String.valueOf(pickupLat), String.valueOf(pickupLong));
//                                if (car_id.equals("")) {
//                                    viewModule.viewDriversApi(String.valueOf(pickupLat), String.valueOf(pickupLong), city_id, alCarId.get(0));
//                                } else {
//                                    viewModule.viewDriversApi(String.valueOf(pickupLat), String.valueOf(pickupLong), city_id, car_id);
//                                }
//                            } catch (Exception e) {
//                                Log.e("err", e.toString());
//                            }
//                        }
//                    }
//                });

                break;
            case ConnectionResult.SERVICE_MISSING:
                Toast.makeText(this, "SERVICE MISSING", Toast.LENGTH_SHORT).show();
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                Toast.makeText(this, "UPDATE REQUIRED", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(this, GooglePlayServicesUtil.isGooglePlayServicesAvailable(this), Toast.LENGTH_SHORT).show();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            pickupLat = mLastLocation.getLatitude();
            pickupLong = mLastLocation.getLongitude();
            latLng = new LatLng(pickupLat, pickupLong);
            cameraPosition = new CameraPosition.Builder().target(latLng).zoom(17).build();
            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            try {
                new GetLocationAsync(pickupLat, pickupLong).execute();
            } catch (Exception e) {
                Logger.e("err in location connected     " + e.toString());
            }
            try {
                viewModule.viewCarsApi(city, String.valueOf(pickupLat), String.valueOf(pickupLong), language_id);
                if (car_id.equals("")) {
                    viewModule.viewDriversApi(String.valueOf(pickupLat), String.valueOf(pickupLong), city_id, alCarId.get(0), language_id);
                } else {
                    viewModule.viewDriversApi(String.valueOf(pickupLat), String.valueOf(pickupLong), city_id, car_id, language_id);
                }
            } catch (Exception e) {
                Logger.e("err in cars connected     " + e.toString());
            }
        }
//        mGoogleMap.setOnMyLocationChangeListener(MainActivity.this);
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000); //5 seconds
        mLocationRequest.setFastestInterval(3000); //3 seconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        //mLocationRequest.setSmallestDisplacement(0.1F); //1/10 meter
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

////        from1.equals("1");
//        newLat = location.getLatitude();
//        newLong = location.getLongitude();
//
//        double distance_between = aerialDistance.aerialDistanceFunction(pickupLat, pickupLong, newLat, newLong);
//        Toast.makeText(this, ""+distance_between, Toast.LENGTH_SHORT).show();
//        if (distance_between < 0.4) {
//            // Ignore this code
////            Toast.makeText(this, "Location Changed if", Toast.LENGTH_SHORT).show();
//        } else {
////            Toast.makeText(this, "Location Changed else", Toast.LENGTH_SHORT).show();
//            pickupLat = newLat;
//            pickupLong = newLong;
//            latLng = new LatLng(newLat, newLong);
//            cameraPosition = new CameraPosition.Builder().target(latLng).zoom(15).build();
//            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//            try {
//                new GetLocationAsync(pickupLat, pickupLong).execute();
//            } catch (Exception e) {
//                Log.e("err", e.toString());
//            }
//            try {
//                viewModule.viewCarsApi(city, String.valueOf(pickupLat), String.valueOf(pickupLong));
//                if (car_id.equals("")) {
//                    viewModule.viewDriversApi(String.valueOf(pickupLat), String.valueOf(pickupLong), city_id, alCarId.get(0));
//                } else {
//                    viewModule.viewDriversApi(String.valueOf(pickupLat), String.valueOf(pickupLong), city_id, car_id);
//                }
//            } catch (Exception e) {
//                Log.e("err", e.toString());
//            }
//            if (mGoogleApiClient != null) {
//                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
//            }
//        }
    }


//    @Override
//    public void onMyLocationChange(Location location) {
//        newLat = location.getLatitude();
//        newLong = location.getLongitude();
//
//        double distance_between = aerialDistance.aerialDistanceFunction(pickupLat, pickupLong, newLat, newLong);
//        Toast.makeText(MainActivity.this, "" + distance_between, Toast.LENGTH_SHORT).show();
//        if (distance_between < 0.4) {
//            // Ignore this code
////            Toast.makeText(this, "Location Changed if", Toast.LENGTH_SHORT).show();
//        } else {
////            Toast.makeText(this, "Location Changed else", Toast.LENGTH_SHORT).show();
//            pickupLat = newLat;
//            pickupLong = newLong;
//            latLng = new LatLng(newLat, newLong);
//            cameraPosition = new CameraPosition.Builder().target(latLng).zoom(16).build();
//            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//            try {
//                new GetLocationAsync(pickupLat, pickupLong).execute();
//            } catch (Exception e) {
//                Log.e("err", e.toString());
//            }
//            try {
//                viewModule.viewCarsApi(city, String.valueOf(pickupLat), String.valueOf(pickupLong));
//                if (car_id.equals("")) {
//                    viewModule.viewDriversApi(String.valueOf(pickupLat), String.valueOf(pickupLong), city_id, alCarId.get(0));
//                } else {
//                    viewModule.viewDriversApi(String.valueOf(pickupLat), String.valueOf(pickupLong), city_id, car_id);
//                }
//            } catch (Exception e) {
//                Log.e("err", e.toString());
//            }
//        }
//    }


    private class GetLocationAsync extends AsyncTask<String, Void, String> {

        double x, y;
        StringBuilder str;

        public GetLocationAsync(double latitude, double longitude) {
            x = latitude;
            y = longitude;
        }

        @Override
        protected void onPreExecute() {
            pickup_address.setText(" Getting Location ... ");
        }

        @Override
        protected String doInBackground(String... params) {
            str = new StringBuilder();
            try {
                geocoder = new Geocoder(MainActivity.this, Locale.ENGLISH);
                addresses = geocoder.getFromLocation(x, y, 1);
                if (geocoder.isPresent()) {
                    Address returnAddress = addresses.get(0);
                    String addressLine = returnAddress.getAddressLine(0);
                    String addressLine1 = returnAddress.getAddressLine(1);
                    String addressLine2 = returnAddress.getAddressLine(2);
                    String city = returnAddress.getLocality();
                    str.append(addressLine + ", " + addressLine1 + ", " + addressLine2 + "__" + city);
                } else {
                }
            } catch (Exception e) {
                Log.e("tag", e.getMessage());
            }
            return str.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                if (result.equals("")) {
                    new GetLocationAsync(pickupLat, pickupLong).execute();
                } else {
                    String CurrentString = result;
                    String[] separated = result.split("__");
                    city = separated[1];
                    pickupLocation = result;
                    pickup_address.setText(pickupLocation);
                    viewModule.viewCarsApi(city, String.valueOf(pickupLat), String.valueOf(pickupLong), language_id);
                }
            } catch (Exception e) {
                Log.e("err", e.toString());
            }
        }
    }

    @Override
    protected void onActivityResult(int req, int res, Intent data) {

        super.onActivityResult(req, res, data);
        if (res == Activity.RESULT_OK) {
            try {
                if (req == 101) {
                    coupon_code = data.getStringExtra("coupon_text");
                    Logger.e("coupon_code   " + coupon_code);
//                    Toast.makeText(MainActivity.this, "Coupon Applied Successfully!!!", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Log.e("res ", e.toString());
            }
            try {
                if (req == 102) {
                    dropLocation = data.getStringExtra("description");
                    String lati = data.getStringExtra("lati");
                    String longi = data.getStringExtra("longi");
                    dropLat = Double.parseDouble(lati);
                    dropLong = Double.parseDouble(longi);

                    tv_drop_address.setText(dropLocation);
                    googlePlacesModule.distance(pickupLat, pickupLong, dropLat, dropLong);
                }
            } catch (Exception e) {
                Log.e("res ", e.toString());
            }

            try {
                if (req == 103) {
                    card_id = data.getStringExtra("card_id");
                    Logger.e("card_id   " + card_id);
                }
            } catch (Exception e) {
                Log.e("res ", e.toString());
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == 1) {
            for (int i = 0, len = permissions.length; i < len; i++) {
                String permission = permissions[i];
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    boolean showRationale = false;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        showRationale = shouldShowRequestPermissionRationale(permission);
                    }
                    if (!showRationale) {
                        Toast.makeText(MainActivity.this, "u cant use our services coz of permission denied", Toast.LENGTH_SHORT).show();
                    } else if (ACCESS_FINE_LOCATION.equals(permission)) {
                        Log.e("user ", "denied location");
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{"android.permission.ACCESS_FINE_LOCATION"}, 1);
                    }
                }
            }
        }
    }

    public View HorizontalList(int layout_name, final String carId, final String carName, final String carImage, final String value, final int position, String distance) {

        LayoutInflater layoutInflater = (LayoutInflater) MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(layout_name, null);

        TextView tv_car_name = (TextView) addView.findViewById(R.id.tv_car_name);
        TextView tv_car_distance = (TextView) addView.findViewById(R.id.tv_ditance);
        iv_car = (ImageView) addView.findViewById(R.id.iv_car);
        iv_car_selected = (ImageView) addView.findViewById(R.id.iv_car_selected);

        tv_car_name.setText(carName);
        tv_car_distance.setText(distance);
        Picasso.with(this).load(imageDomain + carImage).placeholder(R.drawable.car22).error(R.drawable.car22).fit().into(iv_car);
        Picasso.with(this).load(imageDomain + carImage).placeholder(R.drawable.car22).error(R.drawable.car22).fit().into(iv_car_selected);
        demo(value);

        addView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                car_id = carId;
                car_image = carImage;
                car_name = carName;
                for (int i = 0; i < alSelected.size(); i++) {
                    alSelected.set(i, "0");
                }
                alSelected.set(position, "1");
                ll_addview.removeAllViews();
                for (int j = 0; j < alCarId.size(); j++) {
                    View v = HorizontalList(R.layout.itemlayoutforhorizontal, alCarId.get(j), alCarName.get(j), alCarImage.get(j), alSelected.get(j), j, alDistance.get(j));
                    ll_addview.addView(v);
                    if(alSelected.get(j).equals("1")){
                        MaterialShadowContainerView mainBgColor = (MaterialShadowContainerView) v.findViewById(R.id.dgCarMainBg);
                        mainBgColor.setBackgroundResource(R.drawable.car_item_bg_selected);
                        mainBgColor.setBackground(getResources().getDrawable(R.drawable.car_item_bg_selected));
                        etaFroSelectedCar = alDistance.get(j);
                    }
                }
                viewModule.viewDriversApi(String.valueOf(pickupLat), String.valueOf(pickupLong), city_id, car_id, language_id);
            }
        });
        return addView;
    }

    public void showDialogForCars() {
        Dialog dialog = new Dialog(MainActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        dialog.setCancelable(true);
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_for_toast);
        dialog.show();
    }

    public void dialogForAcceptRide() {
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        dialog.setContentView(R.layout.dialog_for_acceptride);
        dialog.setCancelable(false);

        LinearLayout ll_ok_accept = (LinearLayout) dialog.findViewById(R.id.ll_ok_accept);
        ll_ok_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, TrackRideActivity.class)
                        .putExtra("ride_id", ride_id)
                        .putExtra("ride_status", ride_status));
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void dialogForRejectRide() {
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        dialog.setContentView(R.layout.dialog_for_rejectride);
        dialog.setCancelable(false);

        LinearLayout ll_ok_reject = (LinearLayout) dialog.findViewById(R.id.ll_ok_reject);
        ll_ok_reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(MainActivity.this, RidesActivity.class));
            }
        });
        dialog.show();
    }

    @Subscribe
    public void onMessageEvent(MessageEvent event) {
        final int result = event.result;

        if (result == 2) {
            String distance = event.value;
            if (car_id.equals("")) {
                rideModule.rideEstimateApi(GooglePlacesModule.timeVal, distance, city_id, alCarId.get(0), language_id);
            } else {
                rideModule.rideEstimateApi(GooglePlacesModule.timeVal, distance, city_id, car_id, language_id);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        Contants.mainActivity = 0;
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        Contants.mainActivity = 1;
    }

    public void demo(String position) {
        if (position.equals("0")) {
            iv_car.setVisibility(View.VISIBLE);
            iv_car_selected.setVisibility(View.GONE);
        } else {
            iv_car_selected.setVisibility(View.VISIBLE);
            iv_car.setVisibility(View.GONE);
        }
    }

    public void dialogForUpdate() {

        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        dialog.setContentView(R.layout.dialog_for_update);
        dialog.setCancelable(true);

        LinearLayout ll_update_formal = (LinearLayout) dialog.findViewById(R.id.ll_update_formal);
        LinearLayout ll_remind_me_later = (LinearLayout) dialog.findViewById(R.id.ll_remind_me_later);

        ll_update_formal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                Logger.e("package name      " + appPackageName);
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        ll_remind_me_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    protected void onNewIntent(Intent intent) {

        Bundle extras = intent.getExtras();
        if (extras != null) {
            ride_status = extras.getString("ride_status");
            ride_id = extras.getString("ride_id");

            if (ride_status.equals("3")) {
                dialog.dismiss();
                dialogForAcceptRide();
            } else if (ride_status.equals("4")) {
                dialog.dismiss();
                dialogForRejectRide();
            } else if (ride_status.equals("5")) {
                startActivity(new Intent(this, RidesActivity.class));
            } else if (ride_status.equals("6")) {
                startActivity(new Intent(this, RidesActivity.class));
            } else if (ride_status.equals("7")) {
                startActivity(new Intent(this, InvoiceActivity.class)
                        .putExtra("ride_id", ride_id)
                        .putExtra("ride_status", ride_status));
            } else if (ride_status.equals("9")) {
                startActivity(new Intent(this, RidesActivity.class));
            }
        }
    }

    public void dialogForSelectPayment() {
        final Dialog dialog = new Dialog(MainActivity.this, android.R.style.Theme_Holo_Light_DarkActionBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        dialog.setCancelable(false);
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_for_payment_option);


        ListView lv_payment_option = (ListView) dialog.findViewById(R.id.lv_payment_option);
        lv_payment_option.setAdapter(new PaymentAdapter(MainActivity.this, viewPaymentOption));

        lv_payment_option.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                payment_option_id = viewPaymentOption.getMsg().get(position).getPaymentOptionId();
                String payment_option_name = viewPaymentOption.getMsg().get(position).getPaymentOptionName();
                tv_selected_payment.setText(payment_option_name);

                Logger.e("payment_option_id       " + payment_option_id);

                if (payment_option_id.equals("3")) {
                    customerModule.viewCardApi(user_id, language_id);
                } else {
//                    Ignore This Part
                }


                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void dialogForViewCards() {
        final Dialog dialog = new Dialog(MainActivity.this, android.R.style.Theme_Holo_Light_DarkActionBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        dialog.setCancelable(false);
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_for_viewcards);

        ListView lv_view_cards = (ListView) dialog.findViewById(R.id.lv_view_cards);
        lv_view_cards.setAdapter(new CardsAdapter(MainActivity.this, viewCard));

        lv_view_cards.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                card_id = viewCard.getDetails().get(position).getCardId();

                Logger.e("card id       " + card_id);

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    // GET ROTATION ANGLE OF TWO LatLng
    private double getAngleFromLatLng(LatLng pointA, LatLng pointB) {
        double dLon = (pointB.longitude - pointA.longitude);

        double y = Math.sin(dLon) * Math.cos(pointB.latitude);
        double x = Math.cos(pointA.latitude) * Math.sin(pointB.latitude) - Math.sin(pointA.latitude)
                * Math.cos(pointB.latitude) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;
        brng = 360 - brng; // count degrees counter-clockwise - remove to make clockwise

        return brng;
    }

    // Smoothly Rotate Car
    private void smoothRotateCar(final Marker marker, float rotation) {
        final ValueAnimator animation = ValueAnimator.ofFloat(marker.getRotation(), rotation);
        animation.setDuration(3 * (int) rotation);
        animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float animatedValue = (float) valueAnimator.getAnimatedValue();
                marker.setRotation(animatedValue);
            }
        });
        animation.start();
    }

    // Smooth Change Position
    private void smoothGoTheCar(final Marker marker, LatLng finalLatLng) {
        animationMove = ValueAnimator.ofFloat(0f, 100f);
        final double deltaLatitude = finalLatLng.latitude - marker.getPosition().latitude;
        //Toast.makeText(MainActivity.this, "deltaLatitude" + " " + deltaLatitude, Toast.LENGTH_SHORT).show();
        final double deltaLongitude = finalLatLng.longitude - marker.getPosition().longitude;
        animationMove.setDuration(1500);
        animationMove.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator updatedAnimation) {
                float deltaStep = (float) updatedAnimation.getAnimatedValue() - previousStep;
                previousStep = (float) updatedAnimation.getAnimatedValue();
                marker.setPosition(new LatLng(marker.getPosition().latitude + deltaLatitude * deltaStep * 1 / 100, marker.getPosition().longitude + deltaStep * deltaLongitude * 1 / 100));
            }
        });
        animationMove.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                previousStep = 0;
            }
        });
        animationMove.start();
    }

    // Smoothly Rotate Car
    private void smoothRotateAndGoTheCar(final Marker marker, float rotation, LatLng finalLatLng) {

        animationMove = ValueAnimator.ofFloat(0f, 100f);
        final double deltaLatitude = finalLatLng.latitude - marker.getPosition().latitude;
        //Toast.makeText(MainActivity.this, "deltaLatitude" + " " + deltaLatitude, Toast.LENGTH_SHORT).show();
        final double deltaLongitude = finalLatLng.longitude - marker.getPosition().longitude;
        animationMove.setDuration(1500);
        animationMove.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator updatedAnimation) {
                float deltaStep = (float) updatedAnimation.getAnimatedValue() - previousStep;
                previousStep = (float) updatedAnimation.getAnimatedValue();
                marker.setPosition(new LatLng(marker.getPosition().latitude + deltaLatitude * deltaStep * 1 / 100, marker.getPosition().longitude + deltaStep * deltaLongitude * 1 / 100));
            }
        });
        animationMove.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                previousStep = 0;
                //mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), globalZoom));
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                previousStep = 0;
            }
        });

        final ValueAnimator animationRotate = ValueAnimator.ofFloat(marker.getRotation(), rotation);
        animationRotate.setDuration(3 * (int) rotation);
        animationRotate.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float animatedValue = (float) valueAnimator.getAnimatedValue();
                marker.setRotation(animatedValue);
            }
        });
        animationRotate.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                animationMove.start();
            }
        });
        animationRotate.start();
    }

    // Change Car Size during ZOOM of MAP
//DG    @Override
//    public void onCameraMove() {
//        if (mGoogleMap != null) {
//            if (null != marker) {
//                CameraPosition cameraPosition = mGoogleMap.getCameraPosition();
//                int zoomLavel = (int) cameraPosition.zoom;
//                // globalZoom = zoomLavel;
//                Log.d("ZOOM_DG", zoomLavel + "");
//                BitmapDrawable bitmapCarIcon = (BitmapDrawable) getResources().getDrawable(R.drawable.car_ola);
//                Bitmap b = bitmapCarIcon.getBitmap();
//                Bitmap bitmap = Bitmap.createScaledBitmap(b, zoomLavel * 4, zoomLavel * 8, false);
//                try{
//                    marker.setIcon(BitmapDescriptorFactory.fromBitmap(bitmap));
//                }catch (Exception ex){}
//            }
//        }
//DG    }

    // Draw Route Between Two Routes
    private void drawRoute(LatLng pointA, LatLng pointB, boolean showStartPoint, boolean showEndPoint) {
        String url = "https://maps.googleapis.com/maps/api/directions/json?" + "origin=" + pointA.latitude + "," + pointA.longitude + "&destination=" + pointB.latitude + "," + pointB.longitude + "&sensor=false";
        Log.d(" Direction URL:", url);
        DownloadTask downloadTask = new DownloadTask();
        downloadTask.execute(url);

        // REMOVE PREVIOUS MARKER
        if (null != routeStart) {
            routeStart.remove();
        }
        if (null != routeEnd) {
            routeEnd.remove();
        }

        if (showStartPoint && null != mGoogleMap) {
            routeStart = mGoogleMap.addMarker(new MarkerOptions().position(pointA).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_pin)));
        }
        if (showEndPoint && null != mGoogleMap) {
            routeEnd = mGoogleMap.addMarker(new MarkerOptions().position(pointB).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_pin_end)));
        }
    }

    private void mapDotAnimation() {
        GroundOverlay groundOverlay = mGoogleMap.addGroundOverlay(new GroundOverlayOptions()
                .image(BitmapDescriptorFactory.fromResource(R.drawable.dg_map_pin_animate))
                .position(new LatLng(pickupLat, pickupLong), 100));
        RadiusAnimation groundAnimation = new RadiusAnimation(groundOverlay);
        groundAnimation.setRepeatCount(Animation.INFINITE);
        groundAnimation.setRepeatMode(Animation.RESTART);
        groundAnimation.setDuration(2000);
        mapView.startAnimation(groundAnimation);
    }

    @Override
    public void onCameraMoveStarted(int reason) {
        if (reason == REASON_GESTURE) {
            isMapInteractedWithUser = true;
            YoYo.with(Techniques.SlideOutUp).duration(700).repeat(0).playOn(dgLocationContainer);
            YoYo.with(Techniques.SlideOutDown).duration(700).repeat(0).playOn(dgFooterArea);
            //dgLocationContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCameraIdle() {
        if (isMapInteractedWithUser) {
            YoYo.with(Techniques.SlideInDown).duration(500).repeat(0).playOn(dgLocationContainer);
            YoYo.with(Techniques.SlideInUp).duration(500).repeat(0).playOn(dgFooterArea);
            isMapInteractedWithUser = false;
        }
    }
}