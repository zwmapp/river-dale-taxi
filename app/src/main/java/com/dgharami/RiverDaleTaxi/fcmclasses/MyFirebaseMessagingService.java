package com.dgharami.RiverDaleTaxi.fcmclasses;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.dgharami.RiverDaleTaxi.R;
import com.dgharami.RiverDaleTaxi.logger.Logger;
import com.dgharami.RiverDaleTaxi.others.Contants;
import com.dgharami.RiverDaleTaxi.MainActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    Intent intent;
    public static String pn_message, pn_ride_id, pn_ride_status, app_id;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        String data = remoteMessage.getData().get("message");
        Logger.e("Message       " + data);

        String[] parts = data.split("_");
        pn_message = parts[0];
        pn_ride_id = parts[1];
        pn_ride_status = parts[2];

        checkStatus();
    }

    void checkStatus() {
        if (pn_ride_status.equals("3")) {
            if (Contants.mainActivity == 0) {
                Intent broadcastIntent = new Intent();
                broadcastIntent.putExtra("ride_id", pn_ride_id);
                broadcastIntent.putExtra("ride_status", pn_ride_status);
                broadcastIntent.setAction("GCM_RECEIVED_ACTION_RIVERDALETAXI");
                sendBroadcast(broadcastIntent);
            } else if (Contants.mainActivity == 1) {
                sendNotification(pn_message);
            } else {
                sendNotification(pn_message);
            }
        } else if (pn_ride_status.equals("4")) {
            if (Contants.mainActivity == 0) {
                Intent broadcastIntent = new Intent();
                broadcastIntent.putExtra("ride_id", pn_ride_id);
                broadcastIntent.putExtra("ride_status", pn_ride_status);
                broadcastIntent.setAction("GCM_RECEIVED_ACTION_RIVERDALETAXI");
                sendBroadcast(broadcastIntent);
            } else if (Contants.mainActivity == 1) {
                sendNotification(pn_message);
            } else {
                sendNotification(pn_message);
            }
        } else if (pn_ride_status.equals("5")) {
            if (Contants.openActivity2 == 0) {
                Intent broadcastIntent = new Intent();
                broadcastIntent.putExtra("ride_id", pn_ride_id);
                broadcastIntent.putExtra("ride_status", pn_ride_status);
                broadcastIntent.setAction("GCM_RECEIVED_ACTION_CANCEL_RIVERDALETAXI");
                sendBroadcast(broadcastIntent);
            } else if (Contants.openActivity2 == 1) {
                sendNotification(pn_message);
            } else {
                sendNotification(pn_message);
            }
        } else if (pn_ride_status.equals("6")) {
            if (Contants.openActivity2 == 0) {
                Intent broadcastIntent = new Intent();
                broadcastIntent.putExtra("ride_id", pn_ride_id);
                broadcastIntent.putExtra("ride_status", pn_ride_status);
                broadcastIntent.setAction("GCM_RECEIVED_ACTION_CANCEL_RIVERDALETAXI");
                sendBroadcast(broadcastIntent);
            } else if (Contants.openActivity2 == 1) {
                sendNotification(pn_message);
            } else {
                sendNotification(pn_message);
            }
        } else if (pn_ride_status.equals("7")) {
            if (Contants.openActivity2 == 0) {
                Intent broadcastIntent = new Intent();
                broadcastIntent.putExtra("ride_id", pn_ride_id);
                broadcastIntent.putExtra("ride_status", pn_ride_status);
                broadcastIntent.setAction("GCM_RECEIVED_ACTION_CANCEL_RIVERDALETAXI");
                sendBroadcast(broadcastIntent);
            } else if (Contants.openActivity2 == 1) {
                sendNotification(pn_message);
            } else {
                sendNotification(pn_message);
            }
        } else if (pn_ride_status.equals("9")) {
            if (Contants.openActivity2 == 0) {
                Intent broadcastIntent = new Intent();
                broadcastIntent.putExtra("ride_id", pn_ride_id);
                broadcastIntent.putExtra("ride_status", pn_ride_status);
                broadcastIntent.setAction("GCM_RECEIVED_ACTION_CANCEL_RIVERDALETAXI");
                sendBroadcast(broadcastIntent);
            } else if (Contants.openActivity2 == 1) {
                sendNotification(pn_message);
            } else {
                sendNotification(pn_message);
            }
        }
    }

    void sendNotification(String message) {

        if (pn_ride_status.equals("3")) {
            if (Contants.mainActivity == 1) {
                intent = new Intent(this, MainActivity.class)
                        .putExtra("ride_id", pn_ride_id)
                        .putExtra("ride_status", pn_ride_status);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            } else {
                intent = new Intent(this, MainActivity.class)
                        .putExtra("ride_id", pn_ride_id)
                        .putExtra("ride_status", pn_ride_status);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            }
        } else if (pn_ride_status.equals("4")) {
            if (Contants.mainActivity == 1) {
                intent = new Intent(this, MainActivity.class)
                        .putExtra("ride_id", pn_ride_id)
                        .putExtra("ride_status", pn_ride_status);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            } else {
                intent = new Intent(this, MainActivity.class)
                        .putExtra("ride_id", pn_ride_id)
                        .putExtra("ride_status", pn_ride_status);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            }
        } else if (pn_ride_status.equals("5")) {
            if (Contants.openActivity2 == 1) {
                intent = new Intent(this, MainActivity.class)
                        .putExtra("ride_id", pn_ride_id)
                        .putExtra("ride_status", pn_ride_status);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            } else {
                intent = new Intent(this, MainActivity.class)
                        .putExtra("ride_id", pn_ride_id)
                        .putExtra("ride_status", pn_ride_status);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            }
        } else if (pn_ride_status.equals("6")) {
            if (Contants.openActivity2 == 1) {
                intent = new Intent(this, MainActivity.class)
                        .putExtra("ride_id", pn_ride_id)
                        .putExtra("ride_status", pn_ride_status);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            } else {
                intent = new Intent(this, MainActivity.class)
                        .putExtra("ride_id", pn_ride_id)
                        .putExtra("ride_status", pn_ride_status);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            }
        } else if (pn_ride_status.equals("7")) {
            intent = new Intent(this, MainActivity.class)
                    .putExtra("ride_id", pn_ride_id)
                    .putExtra("ride_status", pn_ride_status);
        } else if (pn_ride_status.equals("9")) {
            intent = new Intent(this, MainActivity.class)
                    .putExtra("ride_id", pn_ride_id)
                    .putExtra("ride_status", pn_ride_status);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        long[] pattern = {500, 500, 500, 500, 500, 500, 500, 500, 500};
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.app_logo_100);

        int color = 0x008000;
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.app_logo_100)
                .setLargeIcon(largeIcon)
                .setColor(color)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(alarmSound)
                .setVibrate(pattern)
                .setContentIntent(pendingIntent);
        notificationManager.notify(0, notificationBuilder.build());
    }
}