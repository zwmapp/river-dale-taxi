package com.dgharami.RiverDaleTaxi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.dgharami.RiverDaleTaxi.adapter.RidesAdapter;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.manager.SessionManager;
import com.dgharami.RiverDaleTaxi.models.ResultCheck;
import com.dgharami.RiverDaleTaxi.models.viewrides.ViewRides;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.parsing.RideModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class RidesActivity extends AppCompatActivity implements ApiFetcher {

    LinearLayout ll_back_rides;
    ListView lv_all_rides;
    TextView tv_all_rides;
    public static Activity ridesActivity;

    SessionManager sessionManager;

    LanguageManager languageManager;

    ProgressDialog pd;

    ViewRides viewRides;
    RideModule rideModule;
    String user_id;

    String language_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rides);
        getSupportActionBar().hide();
        ridesActivity = this;

        pd = new ProgressDialog(this,R.style.MyAlertDialogStyle);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);

        viewRides = new ViewRides();
        rideModule = new RideModule(this);

        languageManager=new LanguageManager(this);
        language_id=languageManager.getLanguageDetail().get(LanguageManager.LANGUAGE_ID);

        sessionManager = new SessionManager(this);
        user_id = sessionManager.getUserDetails().get(SessionManager.USER_ID);

        ll_back_rides = (LinearLayout) findViewById(R.id.ll_back_rides);
        tv_all_rides = (TextView) findViewById(R.id.tv_all_rides);

        lv_all_rides = (ListView) findViewById(R.id.lv_all_rides);
        lv_all_rides.setDivider(null);

        rideModule.myRidesApi(user_id,language_id);

        lv_all_rides.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String ride_id = viewRides.getMsg().get(position).getRideId();
                String ride_status = viewRides.getMsg().get(position).getRideStatus();
                String ride_type = viewRides.getMsg().get(position).getRideType();
                String date_time = viewRides.getMsg().get(position).getRideDate() + ", " + viewRides.getMsg().get(position).getRideTime();
                startActivity(new Intent(RidesActivity.this, RidesSelectedActivity.class)
                        .putExtra("ride_id", ride_id)
                        .putExtra("ride_status", ride_status)
                        .putExtra("date_time", date_time)
                        .putExtra("ride_type", ride_type));
            }
        });

        ll_back_rides.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        rideModule.myRidesApi(user_id,language_id);
    }

    @Override
    public void onAPIRunningState(int a) {

        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            pd.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            pd.dismiss();
        }
    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("My Rides")) {

            ResultCheck resultCheck;
            resultCheck = gson.fromJson(response, ResultCheck.class);

            if (resultCheck.result.equals("1")) {

                viewRides = gson.fromJson(response, ViewRides.class);

                tv_all_rides.setVisibility(View.GONE);
                lv_all_rides.setVisibility(View.VISIBLE);
                lv_all_rides.setAdapter(new RidesAdapter(this, viewRides));
            } else {
                tv_all_rides.setVisibility(View.VISIBLE);
                lv_all_rides.setVisibility(View.GONE);
            }

        }

    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }
}
