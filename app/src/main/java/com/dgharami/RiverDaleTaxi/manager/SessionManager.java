package com.dgharami.RiverDaleTaxi.manager;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class SessionManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "LoginPrefrences";
    private static final String IS_LOGIN = "IsLoggedIn";

    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_PHONE = "user_phone_number";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_IMAGE = "user_image";

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(String user_id, String user_name, String user_email, String user_phone, String user_image) {
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(USER_ID, user_id);
        editor.putString(USER_NAME, user_name);
        editor.putString(USER_EMAIL, user_email);
        editor.putString(USER_PHONE, user_phone);
        editor.putString(USER_IMAGE, user_image);
        editor.commit();
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<>();
        user.put(USER_ID, pref.getString(USER_ID, ""));
        user.put(USER_NAME, pref.getString(USER_NAME, ""));
        user.put(USER_EMAIL, pref.getString(USER_EMAIL, ""));
        user.put(USER_PHONE, pref.getString(USER_PHONE, ""));
        user.put(USER_IMAGE, pref.getString(USER_IMAGE, ""));
        return user;
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public void logoutUser() {
        editor.clear();
        editor.commit();
    }
}
