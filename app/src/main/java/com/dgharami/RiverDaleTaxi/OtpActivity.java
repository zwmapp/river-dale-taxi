package com.dgharami.RiverDaleTaxi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.manager.SessionManager;
import com.dgharami.RiverDaleTaxi.models.register.Register;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.parsing.AccountModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

public class OtpActivity extends AppCompatActivity implements ApiFetcher {

    LinearLayout ll_submit_otp;
    EditText edt_otp1, edt_otp2, edt_otp3, edt_otp4;

    TextView tv_show_mobile;

    ProgressDialog pd;

    String cityLocation, name, email, phone, password, otp, social_id, social_type;
    AccountModule accountModule;

    ArrayList<String> alOtp = new ArrayList<>();

    LanguageManager languageManager;

    String language_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        getSupportActionBar().hide();

        accountModule = new AccountModule(OtpActivity.this);
        pd = new ProgressDialog(this,R.style.MyAlertDialogStyle);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);

        edt_otp1 = (EditText) findViewById(R.id.edt_otp1);
        edt_otp2 = (EditText) findViewById(R.id.edt_otp2);
        edt_otp3 = (EditText) findViewById(R.id.edt_otp3);
        edt_otp4 = (EditText) findViewById(R.id.edt_otp4);

        ll_submit_otp = (LinearLayout) findViewById(R.id.ll_submit_otp);
        tv_show_mobile = (TextView) findViewById(R.id.tv_show_mobile);

        languageManager=new LanguageManager(this);
        language_id=languageManager.getLanguageDetail().get(LanguageManager.LANGUAGE_ID);

        name = super.getIntent().getExtras().getString("name");
        email = super.getIntent().getExtras().getString("email");
        phone = super.getIntent().getExtras().getString("phone");
        password = super.getIntent().getExtras().getString("password");
        otp = super.getIntent().getExtras().getString("otp");
        cityLocation = super.getIntent().getExtras().getString("cityLocation");
        social_id = super.getIntent().getExtras().getString("social_id");
        social_type = super.getIntent().getExtras().getString("social_type");

        tv_show_mobile.setText("We have sent an OTP to " + phone + ". Please enter it below.");

        for (int i = 0; i < otp.length(); i++) {
            if (i == 0) {
                alOtp.add(otp.charAt(0) + "");
            } else if (i == 1) {
                alOtp.add(otp.charAt(1) + "");
            } else if (i == 2) {
                alOtp.add(otp.charAt(2) + "");
            } else if (i == 3) {
                alOtp.add(otp.charAt(3) + "");
            }
        }

        for (int j = 0; j < alOtp.size(); j++) {
            if (j == 0) {
                edt_otp1.setText("" + alOtp.get(0));
            } else if (j == 1) {
                edt_otp2.setText("" + alOtp.get(1));
            } else if (j == 2) {
                edt_otp3.setText("" + alOtp.get(2));
            } else if (j == 3) {
                edt_otp4.setText("" + alOtp.get(3));
            }
        }

        ll_submit_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp_enter = edt_otp1.getText().toString() + "" + edt_otp2.getText().toString() + "" + edt_otp3.getText().toString() + "" + edt_otp4.getText().toString();

                if (otp_enter.equals(otp)) {
                    if (social_id.equals("0")) {
                        accountModule.registrationApi(name, email, phone, password,language_id);
                    } else {
                        accountModule.registrationSocialTokenApi(social_id, name, email, phone, social_type,language_id);
                    }
                } else {
                    Toast.makeText(OtpActivity.this, "Please Enter Correct Otp", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onAPIRunningState(int a) {

        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            pd.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            pd.dismiss();
        }

    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("Register")) {
            Register register;
            register = gson.fromJson(response, Register.class);

            if (register.getResult().toString().equals("1")) {
                String user_id = register.getDetails().getUserId();
                String user_name = register.getDetails().getUserName();
                String user_email = register.getDetails().getUserEmail();
                String user_mobile = register.getDetails().getUserPhone();
                String user_image = register.getDetails().getUserImage();

                Toast.makeText(this, "" + register.getMsg().toString(), Toast.LENGTH_SHORT).show();
                new SessionManager(this).createLoginSession(user_id, user_name, user_email, user_mobile, user_image);
                startActivity(new Intent(this, MainActivity.class).putExtra("cityLocation", cityLocation));
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                finish();
                LoginRegisterActivity.loginRegisterActivity.finish();
//                SplashActivity.splash.finish();
            } else {
                Toast.makeText(this, "" + register.getMsg().toString(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }
}
