package com.dgharami.RiverDaleTaxi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.manager.SessionManager;
import com.dgharami.RiverDaleTaxi.models.otpsent.OtpSent;
import com.dgharami.RiverDaleTaxi.models.register.Register;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.parsing.AccountModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class LoginFragment extends Fragment implements ApiFetcher {

    LinearLayout ll_submit_login;
    EditText edt_phone_login, edt_pass_login;
    TextView tv_forgot_create, tv_signup_link;
    ProgressDialog pd;
    String cityLocation;
    String user_phone;
    LanguageManager languageManager;
    String language_id;

    AccountModule accountModule;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);

        pd = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        accountModule = new AccountModule(LoginFragment.this);

        languageManager = new LanguageManager(getContext());
        language_id = languageManager.getLanguageDetail().get(LanguageManager.LANGUAGE_ID);

        cityLocation = super.getActivity().getIntent().getExtras().getString("cityLocation");

        tv_forgot_create = (TextView) v.findViewById(R.id.tv_forgot_create);
        tv_signup_link = (TextView) v.findViewById(R.id.tv_signup_link);
        ll_submit_login = (LinearLayout) v.findViewById(R.id.ll_submit_login);
        edt_pass_login = (EditText) v.findViewById(R.id.edt_pass_login);
        edt_phone_login = (EditText) v.findViewById(R.id.edt_phone_login);

        edt_pass_login.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "OpenSans_Regular.ttf"));
        edt_phone_login.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "OpenSans_Regular.ttf"));

        cityLocation = super.getActivity().getIntent().getExtras().getString("cityLocation");
     /*//   user_phone = getArguments().getString("user_phone");
        edt_phone_login.setText(user_phone);*/

        ll_submit_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String logiMobileNo = edt_phone_login.getText().toString();
                String password = edt_pass_login.getText().toString().trim();

                if (logiMobileNo.equals("")) {
                    Toast.makeText(getContext(), "Email field can't be Empty", Toast.LENGTH_SHORT).show();
                } else if (password.equals("")) {
                    Toast.makeText(getContext(), "Password field can't be Empty", Toast.LENGTH_SHORT).show();
                } else {
                    //accountModule.loginApi(user_phone, password,language_id);
                    accountModule.loginApi(logiMobileNo, password, language_id);
                }
            }
        });

        tv_forgot_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accountModule.otpSentApi("+918101312954", "1", language_id);
            }
        });
        tv_signup_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterFragment registerFragment = new RegisterFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                LoginRegisterActivity loginRegisterScreen = (LoginRegisterActivity) LoginFragment.super.getActivity();

                transaction.replace(loginRegisterScreen.ll_container.getId(), registerFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        return v;
    }

    @Override
    public void onAPIRunningState(int a) {

        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            pd.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            pd.dismiss();
        }
    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("Login")) {
            Register register;
            register = gson.fromJson(response, Register.class);

            if (register.getResult().toString().equals("1")) {

                String user_id = register.getDetails().getUserId();
                String user_name = register.getDetails().getUserName();
                String user_email = register.getDetails().getUserEmail();
                String user_mobile = register.getDetails().getUserPhone();
                String user_image = register.getDetails().getUserImage();

                Toast.makeText(getContext(), "" + register.getMsg(), Toast.LENGTH_SHORT).show();
                new SessionManager(getContext()).createLoginSession(user_id, user_name, user_email, user_mobile, user_image);
                startActivity(new Intent(getContext(), MainActivity.class).putExtra("cityLocation", cityLocation));
                getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                LoginRegisterActivity.loginRegisterActivity.finish();
//                SplashActivity.splashActivity.finish();
            } else {
                Toast.makeText(getContext(), "" + register.getMsg(), Toast.LENGTH_LONG).show();
            }
        }

        if (apiName.equals("Otp Sent")) {

            OtpSent otpSent;
            otpSent = gson.fromJson(response, OtpSent.class);

            if (otpSent.getResult().toString().equals("1")) {

                String otp = otpSent.getDetails().getOtpSent();
                startActivity(new Intent(getContext(), ResetPasswordActivity.class)
                        .putExtra("phone", user_phone)
                        .putExtra("otp", otp)
                        .putExtra("cityLocation", cityLocation));
            } else {
                Toast.makeText(getContext(), "" + otpSent.getMsg(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }
}
