package com.dgharami.RiverDaleTaxi;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.dgharami.RiverDaleTaxi.logger.Logger;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.manager.SessionManager;
import com.dgharami.RiverDaleTaxi.models.deviceid.DeviceId;
import com.dgharami.RiverDaleTaxi.models.viewinvoice.ViewInvoice;
import com.dgharami.RiverDaleTaxi.models.viewrideinfo.ViewRideInfo;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.others.Contants;
import com.dgharami.RiverDaleTaxi.parsing.CustomerModule;
import com.dgharami.RiverDaleTaxi.parsing.RideModule;
import com.dgharami.RiverDaleTaxi.typeface.TextTypeRegular;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class InvoiceActivity extends AppCompatActivity implements ApiFetcher {

    TextView tv_date_invoice, tv_order_id, tv_payment_id, tv_amount, tv_status, tv_type;
    LinearLayout ll_payment_id, ll_rate;
    public static Activity invoiceactivity;

    LanguageManager languageManager;

    ProgressDialog pd;

    Dialog dialog;

    SessionManager sessionManager;
    String user_id, driverId, language_id;

    RideModule rideModule;
    TextTypeRegular dgPickupAddress, dgDropOffAddress;
    String ride_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice);
        getSupportActionBar().hide();
        invoiceactivity = this;

        sessionManager = new SessionManager(this);

        languageManager = new LanguageManager(this);
        language_id = languageManager.getLanguageDetail().get(LanguageManager.LANGUAGE_ID);

        user_id = sessionManager.getUserDetails().get(SessionManager.USER_ID);
//        driverId = super.getIntent().getExtras().getString("driverId");

        rideModule = new RideModule(this);
        dgPickupAddress = (TextTypeRegular) findViewById(R.id.dgPickupAddress);
        dgDropOffAddress = (TextTypeRegular) findViewById(R.id.dgDropOffAddress);

        pd = new ProgressDialog(this,R.style.MyAlertDialogStyle);
        pd.setMessage("Loading...");

        tv_date_invoice = (TextView) findViewById(R.id.tv_date_invoice);
        tv_order_id = (TextView) findViewById(R.id.tv_order_id);
        tv_payment_id = (TextView) findViewById(R.id.tv_payment_id);
        tv_amount = (TextView) findViewById(R.id.tv_amount);
        tv_status = (TextView) findViewById(R.id.tv_status);
        tv_type = (TextView) findViewById(R.id.tv_type);
        ll_payment_id = (LinearLayout) findViewById(R.id.ll_payment_id);
        ll_rate = (LinearLayout) findViewById(R.id.ll_rate);

        String order_id = super.getIntent().getExtras().getString("order_id");
        ride_id = super.getIntent().getExtras().getString("ride_id");

        CustomerModule customerModule = new CustomerModule(this);
        customerModule.viewInvoiceApi(order_id, language_id);

//        if (paymentid.equals("1")) {
//            ll_payment_id.setVisibility(View.GONE);
//            view.setVisibility(View.GONE);
//            tv_date_invoice.setText(super.getIntent().getExtras().getString("payment_date"));
//            tv_order_id.setText(super.getIntent().getExtras().getString("order_id"));
//            tv_amount.setText("$ " + super.getIntent().getExtras().getString("payment_amount"));
//            tv_status.setText(super.getIntent().getExtras().getString("payment_status"));
//            tv_type.setText(super.getIntent().getExtras().getString("payment_platform"));
//        } else {
//            tv_date_invoice.setText(super.getIntent().getExtras().getString("payment_date"));
//            tv_order_id.setText(super.getIntent().getExtras().getString("order_id"));
//            tv_payment_id.setText(super.getIntent().getExtras().getString("payment_id"));
//            tv_amount.setText("$ " + super.getIntent().getExtras().getString("payment_amount"));
//            tv_status.setText(super.getIntent().getExtras().getString("payment_status"));
//            tv_type.setText(super.getIntent().getExtras().getString("payment_platform"));
//        }

        ll_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogForRating();
            }
        });
        boolean is_first_entry = getIntent().getBooleanExtra("is_first_entry", true);
        if(!is_first_entry){
            ll_rate.setVisibility(View.GONE);
        }
    }

    public void dialogForRating() {
        dialog = new Dialog(InvoiceActivity.this, android.R.style.Theme_Holo_Light_DarkActionBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        dialog.setCancelable(false);
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_for_rating);

        final RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.ratingBar1);
        LinearLayout lldone = (LinearLayout) dialog.findViewById(R.id.lldone);

        lldone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String rating = String.valueOf(ratingBar.getRating());

                Logger.e("rating        " + rating);

                if (rating.equals("0.0")) {
                    Toast.makeText(InvoiceActivity.this, "Please select stars", Toast.LENGTH_SHORT).show();
                } else {
                    CustomerModule customerModule = new CustomerModule(InvoiceActivity.this);
                    customerModule.ratingApi(user_id, driverId, rating, language_id);
                }
            }
        });
        dialog.show();
    }

    @Override
    public void onAPIRunningState(int a) {

        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            pd.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            pd.dismiss();
        }
    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("Rating")) {

            DeviceId deviceId;
            deviceId = gson.fromJson(response, DeviceId.class);
            if (deviceId.getResult().toString().equals("1")) {
                Toast.makeText(this, "" + deviceId.getMsg(), Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                finish();
                if (Contants.openActivity2 == 1) {
                    TrackRideActivity.trackRideActivity.finish();
                } else {

                }
            } else {
                Toast.makeText(this, "" + deviceId.getMsg(), Toast.LENGTH_SHORT).show();
            }
        }

        if (apiName.equals("View Ride Info")) {
            ViewRideInfo viewRideInfo;
            viewRideInfo = gson.fromJson(response, ViewRideInfo.class);

            if (viewRideInfo.getResult().toString().equals("1")) {
                dgPickupAddress.setText(viewRideInfo.getDetails().getBeginLocation());
                dgDropOffAddress.setText(viewRideInfo.getDetails().getEndLocation());
            }
        }

        if (apiName.equals("View Invoice")) {

            ViewInvoice viewInvoice;
            viewInvoice = gson.fromJson(response, ViewInvoice.class);
            if (viewInvoice.getResult().toString().equals("1")) {
                rideModule.viewRideInfoApi(ride_id, language_id);

                tv_date_invoice.setText(viewInvoice.getDetails().getPaymentDateTime());
                tv_order_id.setText(viewInvoice.getDetails().getOrderId());
                tv_payment_id.setText(viewInvoice.getDetails().getPaymentId());
                tv_amount.setText("$ " + viewInvoice.getDetails().getPaymentAmount());
                tv_status.setText("Payment Status: " + viewInvoice.getDetails().getPaymentStatus());
                tv_type.setText(viewInvoice.getDetails().getPaymentPlatform());
                driverId=viewInvoice.getDetails().getDriverId();
            } else {
                Toast.makeText(this, "" + viewInvoice.getMsg(), Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }
}

