package com.dgharami.RiverDaleTaxi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.dgharami.RiverDaleTaxi.logger.Logger;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.others.MessageEventLatLong;
import com.dgharami.RiverDaleTaxi.parsing.GooglePlacesModule;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class DropLocationActivity extends AppCompatActivity {

    LinearLayout ll_back_drop_location;
    public static AutoCompleteTextView edt_drop_location;

    LanguageManager languageManager;

    public static ListView lv_drop_location;
    String description = "", place_id = "";

    GooglePlacesModule googlePlacesModule;

    public static ProgressBar progressBar2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drop_location);
        getSupportActionBar().hide();

        googlePlacesModule = new GooglePlacesModule(DropLocationActivity.this);

        ll_back_drop_location = (LinearLayout) findViewById(R.id.ll_back_drop_location);
        edt_drop_location = (AutoCompleteTextView) findViewById(R.id.edt_drop_location);
        lv_drop_location = (ListView) findViewById(R.id.lv_drop_location);

        progressBar2 = (ProgressBar) findViewById(R.id.progressBar2);

        String name = getIntent().getExtras().getString("name");
        Logger.e("name   " + name);

        edt_drop_location.setThreshold(3);
        edt_drop_location.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = edt_drop_location.getText().toString().trim();
                googlePlacesModule.parsingDrop(text);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        lv_drop_location.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                description = GooglePlacesModule.description.get(position);
                place_id = GooglePlacesModule.placeId.get(position);
                if (place_id.equals("")) {

                } else {
                    googlePlacesModule.parsingLatLong(place_id);
                }
            }
        });

        ll_back_drop_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View views = DropLocationActivity.this.getCurrentFocus();
                if (views != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(views.getWindowToken(), 0);
                }
                finish();
            }
        });
    }

    @Subscribe
    public void onMessageEvent(MessageEventLatLong event) {
        String lati = event.lati;
        String longi = event.longi;
        Intent intent = new Intent();
        intent.putExtra("description", description);
        intent.putExtra("lati", lati);
        intent.putExtra("longi", longi);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
