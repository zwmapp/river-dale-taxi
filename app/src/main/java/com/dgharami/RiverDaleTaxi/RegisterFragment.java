package com.dgharami.RiverDaleTaxi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.dgharami.RiverDaleTaxi.logger.Logger;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.manager.SessionManager;
import com.dgharami.RiverDaleTaxi.models.register.Register;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
import com.dgharami.RiverDaleTaxi.parsing.AccountModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class RegisterFragment extends Fragment implements ApiFetcher {
    EditText edt_username_signup, edt_email_signup, edt_phone_signup, edt_pass_signup;
    LinearLayout ll_submit_register;
    ProgressDialog pd;
    String cityLocation, user_name, user_email, user_phone, user_passwoord;

    LanguageManager languageManager;

    String language_id;

    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_register, container, false);

        pd = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);

        ll_submit_register = (LinearLayout) v.findViewById(R.id.ll_submit_register);
        edt_username_signup = (EditText) v.findViewById(R.id.edt_username_signup);
        edt_email_signup = (EditText) v.findViewById(R.id.edt_email_signup);
        edt_pass_signup = (EditText) v.findViewById(R.id.edt_pass_signup);
        edt_phone_signup = (EditText) v.findViewById(R.id.edt_phone_signup);

        languageManager = new LanguageManager(getContext());
        language_id = languageManager.getLanguageDetail().get(LanguageManager.LANGUAGE_ID);

        edt_username_signup.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "OpenSans_Regular.ttf"));
        edt_email_signup.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "OpenSans_Regular.ttf"));
        edt_pass_signup.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "OpenSans_Regular.ttf"));
        edt_phone_signup.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "OpenSans_Regular.ttf"));

        cityLocation = super.getActivity().getIntent().getExtras().getString("cityLocation");
       /* user_phone = getArguments().getString("user_phone");
        edt_phone_signup.setText(user_phone);*/

        Logger.e("city from Phone Fragment      " + cityLocation);

        ll_submit_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user_name = edt_username_signup.getText().toString().trim();
                user_email = edt_email_signup.getText().toString().trim();
                user_phone = edt_phone_signup.getText().toString().trim();
                user_passwoord = edt_pass_signup.getText().toString().trim();

                //String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+.[a-z]+.[a-z]";
                String emailPattern = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
                if (user_name.equals("")) {
                    Toast.makeText(getContext(), "Please Enter Name", Toast.LENGTH_SHORT).show();
                } else if (user_email.equals("")) {
                    Toast.makeText(getContext(), "Please Enter Email", Toast.LENGTH_SHORT).show();
                } else if (user_phone.equals("")) {
                    Toast.makeText(getContext(), "Please Enter Phone", Toast.LENGTH_SHORT).show();
                } else if (user_passwoord.equals("")) {
                    Toast.makeText(getContext(), "Please Enter Password", Toast.LENGTH_SHORT).show();
                } else if (!(user_email.matches(emailPattern))) {
                    Toast.makeText(getContext(), "Please Enter Correct Email", Toast.LENGTH_SHORT).show();
                } else if (user_passwoord.length() < 6) {
                    Toast.makeText(getContext(), "Password should be of min. 6 character", Toast.LENGTH_SHORT).show();
                } else {

                    AccountModule accountModule = new AccountModule(RegisterFragment.this);
                    accountModule.registrationApi(user_name, user_email, user_phone, user_passwoord, language_id);

//                    accountModule.otpSentApi(user_phone, "1",language_id);
                }
            }
        });
        return v;
    }

    @Override
    public void onAPIRunningState(int a) {

        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            pd.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            pd.dismiss();
        }
    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("Register")) {
            Register register;
            register = gson.fromJson(response, Register.class);

            if (register.getResult().toString().equals("1")) {
                String user_id = register.getDetails().getUserId();
                String user_name = register.getDetails().getUserName();
                String user_email = register.getDetails().getUserEmail();
                String user_mobile = register.getDetails().getUserPhone();
                String user_image = register.getDetails().getUserImage();

                Toast.makeText(getContext(), register.getMsg().toString(), Toast.LENGTH_SHORT).show();
                new SessionManager(getContext()).createLoginSession(user_id, user_name, user_email, user_mobile, user_image);
                startActivity(new Intent(getContext(), MainActivity.class).putExtra("cityLocation", cityLocation));
//                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
//                finish();
                LoginRegisterActivity.loginRegisterActivity.finish();
                SplashActivity.splashActivity.finish();
            } else {
                Toast.makeText(getContext(), "" + register.getMsg().toString(), Toast.LENGTH_SHORT).show();
            }
        }

//        if (apiName.equals("Otp Sent")) {
//
//            OtpSent otpSent;
//            otpSent = gson.fromJson(response, OtpSent.class);
//
//            if (otpSent.getResult().toString().equals("1")) {
//                String otp = otpSent.getDetails().getOtpSent();
//                startActivity(new Intent(getContext(), OtpActivity.class)
//                        .putExtra("name", user_name)
//                        .putExtra("email", user_email)
//                        .putExtra("phone", user_phone)
//                        .putExtra("password", user_passwoord)
//                        .putExtra("otp", otp)
//                        .putExtra("cityLocation", cityLocation)
//                        .putExtra("social_id", "0")
//                        .putExtra("social_type", "0"));
//            } else {
//                Toast.makeText(getContext(), "" + otpSent.getMsg(), Toast.LENGTH_SHORT).show();
//            }
//        }
    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }
}
