package com.dgharami.RiverDaleTaxi;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.dgharami.RiverDaleTaxi.manager.LanguageManager;
import com.dgharami.RiverDaleTaxi.models.checkphone.CheckPhone;
import com.dgharami.RiverDaleTaxi.others.ApiFetcher;
//import com.digits.sdk.android.AuthCallback;
//import com.digits.sdk.android.Digits;
//import com.digits.sdk.android.DigitsAuthButton;
//import com.digits.sdk.android.DigitsException;
//import com.digits.sdk.android.DigitsSession;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class PhoneEmailFragment extends Fragment implements ApiFetcher {

    //    EditText edt_phone_1;
//    LinearLayout ll_submit;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

//    private static final String TWITTER_KEY = "nslQ4LkKg1yKbiFyU6FtBt9IC";
//    private static final String TWITTER_SECRET = "jSIVCesm8Om4zdnLxLx2S9Aj44WDy3A6N3zJT0wGWpDYNnZPKV";

    LanguageManager languageManager;

    String language_id;
    ProgressDialog pd;

    public PhoneEmailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_phone, container, false);

//        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
//        Fabric.with(getContext(), new Crashlytics(), new Twitter(authConfig), new Digits.Builder().build());

//        edt_phone_1 = (EditText) v.findViewById(R.id.edt_phone_1);
//        ll_submit = (LinearLayout) v.findViewById(R.id.ll_submit);

        pd = new ProgressDialog(getContext(),R.style.MyAlertDialogStyle);
        pd.setMessage("Loading");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);

        languageManager = new LanguageManager(getContext());
        language_id = languageManager.getLanguageDetail().get(LanguageManager.LANGUAGE_ID);

        fragmentTransaction = getFragmentManager().beginTransaction();

//        ll_submit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////                String phone = edt_phone_1.getText().toString();
////                AccountModule accountModule = new AccountModule(PhoneEmailFragment.this);
////                accountModule.checkPhoneApi(phone, language_id);
//            }
//        });

        //Digits.clearActiveSession();

//        DigitsAuthButton digitsButton = (DigitsAuthButton) v.findViewById(R.id.auth_button);
//        digitsButton.setText("Click Here For Phone Number");
//        digitsButton.setBackgroundColor(getResources().getColor(R.color.colorFooter));
//        digitsButton.setTextColor(getResources().getColor(R.color.colorTextFooter));
//        digitsButton.setTextSize(15);

//        Digits.getSessionManager().clearActiveSession();

//        digitsButton.setCallback(new AuthCallback() {
//            @Override
//            public void success(DigitsSession session, String phoneNumber) {
//                Log.e("success        ", phoneNumber.toString());
//
//                AccountModule accountModule = new AccountModule(PhoneEmailFragment.this);
//                accountModule.checkPhoneApi(phoneNumber, language_id);
//            }
//
//            @Override
//            public void failure(DigitsException exception) {
//                Log.e("Exception        ", exception.toString());
//            }
//        });
        return v;
    }

    @Override
    public void onAPIRunningState(int a) {

        if (a == ApiFetcher.KEY_API_IS_RUNNING) {
            pd.show();
        }
        if (a == ApiFetcher.KEY_API_IS_STOPPED) {
            pd.dismiss();
        }

    }

    @Override
    public void onFetchProgress(int progress) {

    }

    @Override
    public void onFetchComplete(String response, String apiName) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (apiName.equals("Check Phone")) {
            CheckPhone checkPhone;
            checkPhone = gson.fromJson(response, CheckPhone.class);

            if (checkPhone.getResult().toString().equals("1")) {

                String user_phone = checkPhone.getDetails().getUserPhone();
                String phone_verified = checkPhone.getDetails().getPhoneVerified();

                if (phone_verified.equals("0")) {
                    Bundle bundle = new Bundle();
                    bundle.putString("user_phone", user_phone);
                    RegisterFragment registerFragment = new RegisterFragment();
                    registerFragment.setArguments(bundle);
                    fragmentTransaction.replace(R.id.ll_container, registerFragment);
                    fragmentTransaction.commit();
                    getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString("user_phone", user_phone);
                    LoginFragment loginFragment = new LoginFragment();
                    loginFragment.setArguments(bundle);
                    fragmentTransaction.replace(R.id.ll_container, loginFragment);
                    fragmentTransaction.commit();
                    getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            } else {
                Toast.makeText(getContext(), "" + checkPhone.getMsg(), Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public void onFetchFailed(ANError error) {

    }

    @Override
    public void WhichApi(String apiName) {

    }
}
