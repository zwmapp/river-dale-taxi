
package com.dgharami.RiverDaleTaxi.models.otpsent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details {

    @SerializedName("otp_sent")
    @Expose
    private String otpSent;

    public String getOtpSent() {
        return otpSent;
    }

    public void setOtpSent(String otpSent) {
        this.otpSent = otpSent;
    }

}
