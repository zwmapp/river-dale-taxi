
package com.dgharami.RiverDaleTaxi.models.donerideinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DoneRideInfo {

    @SerializedName("result")
    @Expose
    private Integer result;
    @SerializedName("msg")
    @Expose
    private Msg msg;

    /**
     * 
     * @return
     *     The result
     */
    public Integer getResult() {
        return result;
    }

    /**
     * 
     * @param result
     *     The result
     */
    public void setResult(Integer result) {
        this.result = result;
    }

    /**
     * 
     * @return
     *     The msg
     */
    public Msg getMsg() {
        return msg;
    }

    /**
     * 
     * @param msg
     *     The msg
     */
    public void setMsg(Msg msg) {
        this.msg = msg;
    }

}
