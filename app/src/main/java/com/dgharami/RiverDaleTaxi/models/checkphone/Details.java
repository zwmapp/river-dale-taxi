
package com.dgharami.RiverDaleTaxi.models.checkphone;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details {

    @SerializedName("user_phone")
    @Expose
    private String userPhone;
    @SerializedName("phone_verified")
    @Expose
    private String phoneVerified;

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getPhoneVerified() {
        return phoneVerified;
    }

    public void setPhoneVerified(String phoneVerified) {
        this.phoneVerified = phoneVerified;
    }

}
