
package com.dgharami.RiverDaleTaxi.models.viewcard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Detail {

    @SerializedName("card_id")
    @Expose
    private String cardId;

    @SerializedName("card_number")
    @Expose
    private String cardNumber;

    @SerializedName("card_type")
    @Expose
    private String cardType;

    @SerializedName("card_cvv")
    @Expose
    private String cardCvv;

    @SerializedName("card_exp_month")
    @Expose
    private String cardExpMonth;

    @SerializedName("card_exp_year")
    @Expose
    private String cardExpYear;

    @SerializedName("card_name")
    @Expose
    private String cardName;

    @SerializedName("user_id")
    @Expose
    private String cardUserID;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardCvv() {
        return cardCvv;
    }

    public void setCardCvv(String cardCvv) {
        this.cardCvv = cardCvv;
    }

    public String getCardExpMonth() {
        return cardExpMonth;
    }

    public void setCardExpMonth(String cardExpMonth) {
        this.cardExpMonth = cardExpMonth;
    }

    public String getCardExpYear() {
        return cardExpYear;
    }

    public void setCardExpYear(String cardExpYear) {
        this.cardExpYear = cardExpYear;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardUserID() {
        return cardUserID;
    }

    public void setCardUserID(String cardUserID) {
        this.cardUserID = cardUserID;
    }
}
