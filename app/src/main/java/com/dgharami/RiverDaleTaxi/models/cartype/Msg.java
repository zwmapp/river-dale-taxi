
package com.dgharami.RiverDaleTaxi.models.cartype;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Msg {

    @SerializedName("car_type_id")
    @Expose
    private String carTypeId;
    @SerializedName("car_type_name")
    @Expose
    private String carTypeName;
    @SerializedName("car_type_image")
    @Expose
    private String carTypeImage;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("distance")
    @Expose
    private String distance;

    public String getCarTypeId() {
        return carTypeId;
    }

    public void setCarTypeId(String carTypeId) {
        this.carTypeId = carTypeId;
    }

    public String getCarTypeName() {
        return carTypeName;
    }

    public void setCarTypeName(String carTypeName) {
        this.carTypeName = carTypeName;
    }

    public String getCarTypeImage() {
        return carTypeImage;
    }

    public void setCarTypeImage(String carTypeImage) {
        this.carTypeImage = carTypeImage;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

}
