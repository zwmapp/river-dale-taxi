
package com.dgharami.RiverDaleTaxi.models.donerideinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Msg {

    @SerializedName("done_ride_id")
    @Expose
    private String doneRideId;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("tot_time")
    @Expose
    private String totTime;
    @SerializedName("driver_id")
    @Expose
    private String driverId;
    @SerializedName("driver_name")
    @Expose
    private String driverName;
    @SerializedName("driver_image")
    @Expose
    private String driverImage;

    @SerializedName("payment_option_id")
    @Expose
    private String paymentOptionId;

    @SerializedName("ride_id")
    @Expose
    private String rideId;


    /**
     * 
     * @return
     *     The doneRideId
     */
    public String getDoneRideId() {
        return doneRideId;
    }

    /**
     * 
     * @param doneRideId
     *     The done_ride_id
     */
    public void setDoneRideId(String doneRideId) {
        this.doneRideId = doneRideId;
    }

    /**
     * 
     * @return
     *     The distance
     */
    public String getDistance() {
        return distance;
    }

    /**
     * 
     * @param distance
     *     The distance
     */
    public void setDistance(String distance) {
        this.distance = distance;
    }

    /**
     * 
     * @return
     *     The amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * 
     * @param amount
     *     The amount
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * 
     * @return
     *     The totTime
     */
    public String getTotTime() {
        return totTime;
    }

    /**
     * 
     * @param totTime
     *     The tot_time
     */
    public void setTotTime(String totTime) {
        this.totTime = totTime;
    }

    /**
     * 
     * @return
     *     The driverId
     */
    public String getDriverId() {
        return driverId;
    }

    /**
     * 
     * @param driverId
     *     The driver_id
     */
    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    /**
     * 
     * @return
     *     The driverName
     */
    public String getDriverName() {
        return driverName;
    }

    /**
     * 
     * @param driverName
     *     The driver_name
     */
    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    /**
     * 
     * @return
     *     The driverImage
     */
    public String getDriverImage() {
        return driverImage;
    }

    /**
     * 
     * @param driverImage
     *     The driver_image
     */
    public void setDriverImage(String driverImage) {
        this.driverImage = driverImage;
    }


    public String getPaymentOptionId() {
        return paymentOptionId;
    }

    public void setPaymentOptionId(String paymentOptionId) {
        this.paymentOptionId = paymentOptionId;
    }

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }
}
