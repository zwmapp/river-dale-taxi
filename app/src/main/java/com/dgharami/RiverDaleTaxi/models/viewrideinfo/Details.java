
package com.dgharami.RiverDaleTaxi.models.viewrideinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details {

    @SerializedName("ride_id")
    @Expose
    private String rideId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("pickup_lat")
    @Expose
    private String pickupLat;
    @SerializedName("pickup_long")
    @Expose
    private String pickupLong;
    @SerializedName("pickup_location")
    @Expose
    private String pickupLocation;

    @SerializedName("drop_lat")
    @Expose
    private String dropLat;
    @SerializedName("drop_long")
    @Expose
    private String dropLong;

    @SerializedName("drop_location")
    @Expose
    private String dropLocation;
    @SerializedName("ride_date")
    @Expose
    private String rideDate;
    @SerializedName("ride_time")
    @Expose
    private String rideTime;
    @SerializedName("ride_status")
    @Expose
    private String rideStatus;
    @SerializedName("ride_type")
    @Expose
    private String rideType;
    @SerializedName("driver_name")
    @Expose
    private String driverName;

    @SerializedName("driver_id")
    @Expose
    private String driverID;

    @SerializedName("driver_image")
    @Expose
    private String driverImage;
    @SerializedName("driver_rating")
    @Expose
    private String driverRating;
    @SerializedName("driver_phone")
    @Expose
    private String driverPhone;
    @SerializedName("driver_lat")
    @Expose
    private String driverLat;
    @SerializedName("driver_long")
    @Expose
    private String driverLong;
    @SerializedName("car_number")
    @Expose
    private String carNumber;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("car_type_name")
    @Expose
    private String carTypeName;
    @SerializedName("car_model_name")
    @Expose
    private String carModelName;
    @SerializedName("begin_lat")
    @Expose
    private String beginLat;
    @SerializedName("begin_long")
    @Expose
    private String beginLong;
    @SerializedName("arrived_time")
    @Expose
    private String arrivedTime;
    @SerializedName("begin_time")
    @Expose
    private String beginTime;
    @SerializedName("begin_location")
    @Expose
    private String beginLocation;
    @SerializedName("end_location")
    @Expose
    private String endLocation;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("time")
    @Expose
    private String time;
    @Expose
    @SerializedName("car_type_image")
    private String carTypeImage;

    @Expose
    @SerializedName("done_ride_id")
    private String doneRideId;

    @Expose
    @SerializedName("ride_image")
    private String rideImage;

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPickupLat() {
        return pickupLat;
    }

    public void setPickupLat(String pickupLat) {
        this.pickupLat = pickupLat;
    }

    public String getPickupLong() {
        return pickupLong;
    }

    public void setPickupLong(String pickupLong) {
        this.pickupLong = pickupLong;
    }

    public String getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public String getDropLocation() {
        return dropLocation;
    }

    public void setDropLocation(String dropLocation) {
        this.dropLocation = dropLocation;
    }

    public String getRideDate() {
        return rideDate;
    }

    public void setRideDate(String rideDate) {
        this.rideDate = rideDate;
    }

    public String getRideTime() {
        return rideTime;
    }

    public void setRideTime(String rideTime) {
        this.rideTime = rideTime;
    }

    public String getRideStatus() {
        return rideStatus;
    }

    public void setRideStatus(String rideStatus) {
        this.rideStatus = rideStatus;
    }

    public String getRideType() {
        return rideType;
    }

    public void setRideType(String rideType) {
        this.rideType = rideType;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverImage() {
        return driverImage;
    }

    public void setDriverImage(String driverImage) {
        this.driverImage = driverImage;
    }

    public String getDriverRating() {
        return driverRating;
    }

    public void setDriverRating(String driverRating) {
        this.driverRating = driverRating;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getDriverLat() {
        return driverLat;
    }

    public void setDriverLat(String driverLat) {
        this.driverLat = driverLat;
    }

    public String getDriverLong() {
        return driverLong;
    }

    public void setDriverLong(String driverLong) {
        this.driverLong = driverLong;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getCarTypeName() {
        return carTypeName;
    }

    public void setCarTypeName(String carTypeName) {
        this.carTypeName = carTypeName;
    }

    public String getCarModelName() {
        return carModelName;
    }

    public void setCarModelName(String carModelName) {
        this.carModelName = carModelName;
    }

    public String getBeginLat() {
        return beginLat;
    }

    public void setBeginLat(String beginLat) {
        this.beginLat = beginLat;
    }

    public String getBeginLong() {
        return beginLong;
    }

    public void setBeginLong(String beginLong) {
        this.beginLong = beginLong;
    }

    public String getArrivedTime() {
        return arrivedTime;
    }

    public void setArrivedTime(String arrivedTime) {
        this.arrivedTime = arrivedTime;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getBeginLocation() {
        return beginLocation;
    }

    public void setBeginLocation(String beginLocation) {
        this.beginLocation = beginLocation;
    }

    public String getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(String endLocation) {
        this.endLocation = endLocation;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCarTypeImage() {
        return carTypeImage;
    }

    public void setCarTypeImage(String carTypeImage) {
        this.carTypeImage = carTypeImage;
    }

    public String getDropLat() {
        return dropLat;
    }

    public void setDropLat(String dropLat) {
        this.dropLat = dropLat;
    }

    public String getDropLong() {
        return dropLong;
    }

    public void setDropLong(String dropLong) {
        this.dropLong = dropLong;
    }

    public String getDoneRideId() {
        return doneRideId;
    }

    public void setDoneRideId(String doneRideId) {
        this.doneRideId = doneRideId;
    }

    public String getRideImage() {
        return rideImage;
    }

    public void setRideImage(String rideImage) {
        this.rideImage = rideImage;
    }

    public String getDriverID() {
        return driverID;
    }

    public void setDriverID(String driverID) {
        this.driverID = driverID;
    }
}
