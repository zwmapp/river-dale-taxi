
package com.dgharami.RiverDaleTaxi.models.trackrideafterstart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details {

    @SerializedName("latlong_id")
    @Expose
    private String latlongId;
    @SerializedName("ride_id")
    @Expose
    private String rideId;
    @SerializedName("driver_id")
    @Expose
    private String driverId;
    @SerializedName("driver_lat")
    @Expose
    private String driverLat;
    @SerializedName("driver_long")
    @Expose
    private String driverLong;

    public String getLatlongId() {
        return latlongId;
    }

    public void setLatlongId(String latlongId) {
        this.latlongId = latlongId;
    }

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getDriverLat() {
        return driverLat;
    }

    public void setDriverLat(String driverLat) {
        this.driverLat = driverLat;
    }

    public String getDriverLong() {
        return driverLong;
    }

    public void setDriverLong(String driverLong) {
        this.driverLong = driverLong;
    }

}
