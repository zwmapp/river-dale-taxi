
package com.dgharami.RiverDaleTaxi.models.rate_card;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details {

    @SerializedName("rate_card_id")
    @Expose
    private String rateCardId;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("car_type_id")
    @Expose
    private String carTypeId;
    @SerializedName("base_miles")
    @Expose
    private String baseMiles;
    @SerializedName("base_minutes")
    @Expose
    private String baseMinutes;
    @SerializedName("base_price_miles")
    @Expose
    private String basePriceMiles;
    @SerializedName("base_price_minute")
    @Expose
    private String basePriceMinute;
    @SerializedName("price_per_mile")
    @Expose
    private String pricePerMile;
    @SerializedName("price_per_minute")
    @Expose
    private String pricePerMinute;
    @SerializedName("base_waiting_minutes")
    @Expose
    private String baseWaitingMinutes;
    @SerializedName("base_price_minute_waiting")
    @Expose
    private String basePriceMinuteWaiting;
    @SerializedName("price_per_minute_waiting")
    @Expose
    private String pricePerMinuteWaiting;
    @SerializedName("extra_charges")
    @Expose
    private String extraCharges;

    public String getRateCardId() {
        return rateCardId;
    }

    public void setRateCardId(String rateCardId) {
        this.rateCardId = rateCardId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCarTypeId() {
        return carTypeId;
    }

    public void setCarTypeId(String carTypeId) {
        this.carTypeId = carTypeId;
    }

    public String getBaseMiles() {
        return baseMiles;
    }

    public void setBaseMiles(String baseMiles) {
        this.baseMiles = baseMiles;
    }

    public String getBaseMinutes() {
        return baseMinutes;
    }

    public void setBaseMinutes(String baseMinutes) {
        this.baseMinutes = baseMinutes;
    }

    public String getBasePriceMiles() {
        return basePriceMiles;
    }

    public void setBasePriceMiles(String basePriceMiles) {
        this.basePriceMiles = basePriceMiles;
    }

    public String getBasePriceMinute() {
        return basePriceMinute;
    }

    public void setBasePriceMinute(String basePriceMinute) {
        this.basePriceMinute = basePriceMinute;
    }

    public String getPricePerMile() {
        return pricePerMile;
    }

    public void setPricePerMile(String pricePerMile) {
        this.pricePerMile = pricePerMile;
    }

    public String getPricePerMinute() {
        return pricePerMinute;
    }

    public void setPricePerMinute(String pricePerMinute) {
        this.pricePerMinute = pricePerMinute;
    }

    public String getBaseWaitingMinutes() {
        return baseWaitingMinutes;
    }

    public void setBaseWaitingMinutes(String baseWaitingMinutes) {
        this.baseWaitingMinutes = baseWaitingMinutes;
    }

    public String getBasePriceMinuteWaiting() {
        return basePriceMinuteWaiting;
    }

    public void setBasePriceMinuteWaiting(String basePriceMinuteWaiting) {
        this.basePriceMinuteWaiting = basePriceMinuteWaiting;
    }

    public String getPricePerMinuteWaiting() {
        return pricePerMinuteWaiting;
    }

    public void setPricePerMinuteWaiting(String pricePerMinuteWaiting) {
        this.pricePerMinuteWaiting = pricePerMinuteWaiting;
    }

    public String getExtraCharges() {
        return extraCharges;
    }

    public void setExtraCharges(String extraCharges) {
        this.extraCharges = extraCharges;
    }

}
